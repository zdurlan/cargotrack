//
//  TextPopupViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 28/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class TextPopupViewController: UIViewController {
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var textView: UITextView!
    var textToShow: String?
    var date: String?
    var attributedTextToShow: NSAttributedString?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let attributedString = attributedTextToShow {
            textView.attributedText = attributedString
        } else {
            textView.text = textToShow
        }
        dateLabel.text = date
    }
}
