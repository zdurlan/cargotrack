//
//  CustomCarMarkerInfoWindow.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 03/09/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

enum CarStatus {
    case driving
    case parking
    case neutral
    
    var description: String {
        switch self {
        case .driving:
            return "D"
        case .parking:
            return "P"
        case .neutral:
            return "N"
        }
    }
}

class CustomCarMarkerInfoWindow: UIView {
    @IBOutlet private weak var carNameLabel: UILabel!
    @IBOutlet private weak var carStatusLabel: UILabel!
    @IBOutlet private weak var carStatusBackground: UIView!
    @IBOutlet private weak var triangleView: TriangleView!
    
    func configure(with name: String?, status: CarStatus) {
        carNameLabel.text = name
        carStatusLabel.text = status.description
        switch status {
        case .driving:
            carStatusBackground.backgroundColor = UIColor(red: 26/255, green: 192/255, blue: 105/255, alpha: 1)
        case .parking:
            carStatusBackground.backgroundColor = UIColor(red: 203/255, green: 11/255, blue: 28/255, alpha: 1)
        case .neutral:
            carStatusBackground.backgroundColor = UIColor(red: 240/255, green: 109/255, blue: 26/255, alpha: 1)
        }
    }
}

extension CustomCarMarkerInfoWindow {
    static func annotationImage(name: String?, carStatus: CarStatus, pinIcon icon: UIImage, course: String?) -> UIImage? {
        let infoWindow = CustomCarMarkerInfoWindow.loadFromNib() as! CustomCarMarkerInfoWindow
        infoWindow.configure(with: name, status: carStatus)
        
        // End image context before the methods returns
        defer {
            UIGraphicsEndImageContext()
        }
        
        // Create container view
        let annotationImage = UIView(frame: CGRect(x: 0, y: 0, width: infoWindow.frame.size.width, height: infoWindow.frame.size.height + icon.size.height))
        annotationImage.addSubview(infoWindow)
        
        let iconImageView = UIImageView(image: icon)
        iconImageView.backgroundColor = UIColor.clear
        iconImageView.contentMode = .scaleAspectFit
        let position = carStatus == .driving ? infoWindow.frame.size.height - 20 : infoWindow.frame.size.height
        iconImageView.frame = CGRect(x: (infoWindow.frame.size.width - icon.size.width)/2, y: position, width: icon.size.width, height: icon.size.height)
        if let floatCourse = Float(course ?? "0"), carStatus == .driving {
            iconImageView.transform = CGAffineTransform(rotationAngle: CGFloat(floatCourse * .pi / 180))
        }
        annotationImage.addSubview(iconImageView)
        
        // Render image
        UIGraphicsBeginImageContextWithOptions(annotationImage.frame.size, false, UIScreen.main.scale)
        if let ctx = UIGraphicsGetCurrentContext() {
            annotationImage.layer.render(in: ctx)
            return UIGraphicsGetImageFromCurrentImageContext()
        }
        
        return nil
    }
}
