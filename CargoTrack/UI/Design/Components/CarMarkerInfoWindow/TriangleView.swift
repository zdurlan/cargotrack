//
//  TriangleView.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 03/09/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class TriangleView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.minY))
        context.addLine(to: CGPoint(x: (rect.maxX / 2.0), y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        context.closePath()
        
        context.setFillColor(red: 29/255, green: 47/255, blue: 133/255, alpha: 1)
        context.fillPath()
    }
}
