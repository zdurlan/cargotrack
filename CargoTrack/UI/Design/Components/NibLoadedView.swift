//
//  NibLoadedView.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 20/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class NibLoadedView: UIView {
    
    private func loadFromNib() {
        let ViewSubclass = type(of: self)
        let customView: UIView = ViewSubclass.loadFromNib(owner: self)
        customView.frame = bounds
        customView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(customView)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadFromNib()
    }
    
}
