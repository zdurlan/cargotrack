//
//  ProfileHeaderView.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 30/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class ProfileHeaderView: UIView {
    @IBOutlet private weak var textLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!

    func configure() {
        textLabel.text = "contact_details".localized
        imageView.image = UIImage(named: "humanProfileDetailsIcon")
    }
}
