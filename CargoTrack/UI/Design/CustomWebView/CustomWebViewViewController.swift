//
//  CustomWebViewViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 27/05/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit
import WebKit

class CustomWebViewViewController: BaseLightStatusBarViewController {
    private var webView: WKWebView!
    private var urlObservation: NSKeyValueObservation?
    
    var date: String?
    var textToShow: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.cornerRadius = 4
        webView = WKWebView()
        view.addSubview(webView)
        webView.pin(to: view, insets: UIEdgeInsets(top: 24, left: 8, bottom: 16, right: -8))
        webView.loadHTMLString(textToShow, baseURL: nil)
        
        let label = UILabel()
        label.text = date
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .black
        label.sizeToFit()
        view.addSubview(label)
        label.setAnchorCenterHorizontallyTo(view: view, topAnchor: (view.topAnchor, 8))
    }
}
