//
//  BaseLightStatusBarViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 25/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Presentr
import SwiftMessages
import UIKit

public protocol ActivityIndicatorPresenter {
    /// The activity indicator
    var activityIndicator: UIActivityIndicatorView { get }
    
    /// Show the activity indicator in the view
    func showActivityIndicator(withBackgroundAlpha: CGFloat)
    
    /// Hide the activity indicator in the view
    func hideActivityIndicator()
}

class BaseLightStatusBarViewController: UIViewController, ActivityIndicatorPresenter, Deinitcallable {
    var activityIndicator = UIActivityIndicatorView()
    var onDeinit: (() -> Void)?
    var timerRights: Timer!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard (_:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        NotificationCenter.default.addObserver(self, selector: #selector(showNotification(_:)), name: .didReceiveNotification, object: nil)
        timerRights = Timer.scheduledTimer(withTimeInterval: 3600, repeats: true) { _ in
            self.periodicallyCheckUserRights()
        }
    }
    
    func periodicallyCheckUserRights() {
        showActivityIndicator()
        APIClient().perform(UserService.getUserSuspended(userId: StateController.currentUser?.userId ?? 0)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success(let response):
                let suspended = String(data: response.body!, encoding: String.Encoding.utf8)
                if suspended == "true" {
                    strongSelf.hideActivityIndicator()
                    NotificationCenter.default.post(name: .didReceiveLogout, object: nil)
                } else {
                    strongSelf.getDeviceList()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func getDeviceList() {
        if !(StateController.currentUser?.mobileAppAsWrapper ?? false) {
            APIClient().perform(DeviceService.createDeviceListRequest()) { result in
                self.hideActivityIndicator()
                switch result {
                case .success(let response):
                    if let vehicles = try? response.decode(to: [Vehicle].self) {
                        if vehicles.body.count != DBManager.sharedInstance.getVehiclesFromDB().count {
                            NotificationCenter.default.post(name: .didReceiveLogout, object: nil)
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            hideActivityIndicator()
        }
    }
    
    @objc func showNotification(_ notification: Notification) {
        guard let notificationStruct = try? JSONDecoder().decode(NotificationStruct.self, from: notification.userInfo?["data"] as! Data) else {
            return
        }
        let message = notificationStruct.payload.summary
    
        let view = MessageView.viewFromNib(layout: .cardView)
        var color = UIColor(red: 14/255, green: 98/255, blue: 213/255, alpha: 1)
        switch notificationStruct.payload.priority {
        case 1:
            color = UIColor(red: 0/255, green: 200/255, blue: 125/255, alpha: 1)
        case 2:
            color = UIColor(red: 255/255, green: 175/255, blue: 50/255, alpha: 1)
        case 3:
            color = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
        case 4:
            color = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
        default:
            break
        }
        view.configureTheme(backgroundColor: color, foregroundColor: .white, iconImage: nil, iconText: nil)
        view.configureDropShadow()
        view.configureContent(title: "", body: notificationStruct.payload.summary)
        view.button?.isHidden = false
        view.button?.setTitle("view".localized, for: .normal)
        view.tapHandler = { _ in
            SwiftMessages.hide()
            let presenter = Presentr(presentationType: .popup)
            let controller = TextPopupViewController()
            controller.textToShow = message
            self.customPresentViewController(presenter, viewController: controller, animated: true, completion: nil)
        }
        view.buttonTapHandler = { _ in
            SwiftMessages.hide()
            let presenter = Presentr(presentationType: .popup)
            let controller = TextPopupViewController()
            controller.textToShow = message
            self.customPresentViewController(presenter, viewController: controller, animated: true, completion: nil)
        }
        view.layoutMarginAdditions.top = -40
        
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: .statusBar)
        config.duration = .seconds(seconds: 5)
        config.dimMode = .gray(interactive: true)
        config.preferredStatusBarStyle = .lightContent
        
        SwiftMessages.show(config: config, view: view)
    }
  
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    deinit {
        timerRights = nil
        onDeinit?()
    }
}

public extension ActivityIndicatorPresenter where Self: UIViewController {
    
    func showActivityIndicator(withBackgroundAlpha: CGFloat = 1) {
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = false
            if #available(iOS 13.0, *) {
                self.activityIndicator.style = .large
            } else {
                self.activityIndicator.style = .gray
            }
            self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            self.activityIndicator.center = CGPoint(x: self.view.bounds.size.width / 2, y: self.view.bounds.height / 2)
            self.view.alpha = withBackgroundAlpha
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            self.view.alpha = 1
            self.view.isUserInteractionEnabled = true
            self.activityIndicator.stopAnimating()
            self.activityIndicator.removeFromSuperview()
        }
    }
}
