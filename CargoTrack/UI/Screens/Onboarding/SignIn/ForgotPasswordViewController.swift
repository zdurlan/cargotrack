//
//  ForgotPasswordViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 28/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var passwordLabel: UILabel! {
        didSet {
            passwordLabel.text = "password".localized
        }
    }
    @IBOutlet private weak var resetLabel: UILabel! {
        didSet {
            resetLabel.text = "reset".localized
        }
    }
    @IBOutlet private weak var submitButton: UIButton! {
        didSet {
            submitButton.setTitle("submit_button".localized, for: .normal)
        }
    }
    @IBOutlet private weak var emailTxt: UITextField! {
        didSet {
            emailTxt.addTarget(self, action: #selector(textFieldDidChange(_:)),
                               for: UIControl.Event.editingChanged)
            emailTxt.delegate = self
            emailTxt.returnKeyType = .done
            emailTxt.attributedPlaceholder = NSAttributedString(string: "email_address".localized,
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
            emailTxt.addTarget(self, action: #selector(emailTxtChanged), for: .editingChanged)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc func emailTxtChanged(textField: UITextField) {
        if !String.isValidEmail(textField.text!) {
            emailTxt.textColor = UIColor(red: 214/255, green: 10/255, blue: 44/255, alpha: 1)
        } else {
            emailTxt.textColor = UIColor.white
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapSubmit(_ sender: Any) {
        showActivityIndicator()
        APIClient().perform(UserService.forgotPasswordRequest(email: emailTxt.text!)) { result in
            self.hideActivityIndicator()
            switch result {
            case .success(let response):
                if response.statusCode == 200 {
                    self.presentAlertController(title: "success_title".localized, message: "we_have_sent_an_email_reset_password".localized)
                } else if (try? response.decode(to: ErrorJson.self)) != nil {
                    self.presentAlertController(title: "", message: "no_email_username_found".localized)
                } else {
                    self.presentAlertController(title: "", message: "error_message".localized)
                }
            case .failure(let error):
                print(error)
                self.presentAlertController(title: "", message: "error_message".localized)
            }
        }
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if !emailTxt.text!.isEmpty && String.isValidEmail(emailTxt.text!) {
            submitButton.isUserInteractionEnabled = true
            submitButton.backgroundColor = .white
            submitButton.setTitleColor(view.tintColor, for: .normal)
        } else {
            submitButton.isUserInteractionEnabled = false
            submitButton.backgroundColor = .clear
            submitButton.setTitleColor(.white, for: .normal)
        }
    }
}
