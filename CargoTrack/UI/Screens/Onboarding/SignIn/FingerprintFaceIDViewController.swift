//
//  FingerprintFaceIDViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 04/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

enum AuthMethod {
    case fingerprint
    case faceID
}

class FingerprintFaceIDViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var authMethodIcon: UIImageView!
    @IBOutlet private weak var firstTextLabel: UILabel!
    @IBOutlet private weak var secondTextLabel: UILabel!
    @IBOutlet private weak var youCanChangeLabel: UILabel! {
        didSet {
            youCanChangeLabel.text = "fingerprint_change_in_settings".localized
        }
    }
    @IBOutlet private weak var yesButton: UIButton! {
        didSet {
            yesButton.setTitle("yes".localized, for: .normal)
        }
    }
    @IBOutlet private weak var maybeLaterButton: UIButton! {
        didSet {
            maybeLaterButton.setTitle("later".localized, for: .normal)
        }
    }
    
    var stateController: StateController!
    var authMethod: AuthMethod!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureWith(authenticationMethod: authMethod)
    }
    
    private func configureWith(authenticationMethod: AuthMethod) {
        switch authenticationMethod {
        case .faceID:
            authMethodIcon.image = UIImage(named: "faceIDIcon")
            firstTextLabel.text = "enable_faceid_authentication".localized
            secondTextLabel.text = "use_faceid_to_authenticate".localized
        case .fingerprint:
            authMethodIcon.image = UIImage(named: "fingerprintIcon")
            firstTextLabel.text = "enable_fingerprint_authentication".localized
            secondTextLabel.text = "use_fingerprint_to_authenticate".localized
        }
    }
    
    @IBAction func didTapYes(_ sender: Any) {
        AuthenticationManager.checkAuthMethod { (success, authenticationError) in
            if success {
                DispatchQueue.main.async {
                    UserDefaults.standard.set(true, forKey: "isAuthEnabled")
                    UserDefaults.standard.set(StateController.currentUser?.username ?? "", forKey: "username")
                    UserDefaults.standard.synchronize()
                    self.navigateForward()
                }
            } else {
                DispatchQueue.main.async {
                    switch authenticationError {
                    case .some(let error):
                        switch error {
                        case .noBiometrics:
                            self.presentAlertController(title: "error_title".localized, message: "device_not_configured_fingerprint".localized)
                        case .failure:
                            self.presentAlertController(title: "error_title".localized, message: "fingerprint_failed".localized)
                        }
                    case .none:
                        break
                    }
                }
            }
        }
    }
    
    @IBAction func didTapLater(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isAuthEnabled")
        UserDefaults.standard.synchronize()
        navigateForward()
    }
    
    private func navigateForward() {
        if (StateController.currentUser?.isSuspended ?? 0) != 0 {
            let financialCoordinator = FinancialCoordinator(presenter: navigationController ?? UINavigationController())
            financialCoordinator.isFromFingerprint = true
            financialCoordinator.start()
        } else {
            if StateController.currentUser?.mobileAppAsWrapper ?? false {
                self.goToMobileWrapper()
            } else if StateController.currentUser?.userRights?.contains(where: { $0.id == 6 }) ?? false {
                self.goToFleetScreen()
            } else if StateController.currentUser?.userRights?.contains(where: { $0.id == 26 }) ?? false {
                self.goToRoadTax()
            }
        }
    }
    
    private func goToRoadTax() {
        let roadTaxCoordinator = RoadTaxCoordinator(presenter: self.navigationController ?? UINavigationController())
        roadTaxCoordinator.isFromFingerprint = true
        roadTaxCoordinator.start()
    }
    
    private func goToFleetScreen() {
        let fleetCoordinator = FleetCoordinator(presenter: navigationController ?? UINavigationController())
        fleetCoordinator.start()
    }
    
    private func goToMobileWrapper() {
        let fleetWebViewCoordinator = FleetWebviewCoordinator(presenter: navigationController ?? UINavigationController())
        fleetWebViewCoordinator.start()
    }
}
