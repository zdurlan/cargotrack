//
//  SignInViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 26/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import KeychainSwift
import UIKit

class SignInViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var forgotPasswordButton: UIButton! {
        didSet {
            forgotPasswordButton.setTitle("forgot_password".localized, for: .normal)
        }
    }
    @IBOutlet private weak var enterDetailsLabel: UILabel! {
        didSet {
            enterDetailsLabel.text = "enter_details_below".localized
        }
    }
    @IBOutlet private weak var signInTitleLabel: UILabel! {
        didSet {
            signInTitleLabel.text = "sign_in_button".localized
        }
    }
    @IBOutlet private weak var signInButton: UIButton! {
        didSet {
            signInButton.setTitle("sign_in_button".localized, for: .normal)
        }
    }
    @IBOutlet private weak var userTxt: UITextField! {
        didSet {
            userTxt.addTarget(self, action: #selector(textFieldDidChange(_:)),
                              for: UIControl.Event.editingChanged)
            userTxt.delegate = self
            userTxt.returnKeyType = .next
            userTxt.attributedPlaceholder = NSAttributedString(string: "username".localized,
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        }
    }
    
    @IBOutlet private weak var passTxt: UITextField! {
        didSet {
            passTxt.addTarget(self, action: #selector(textFieldDidChange(_:)),
                              for: UIControl.Event.editingChanged)
            passTxt.delegate = self
            passTxt.returnKeyType = .done
            passTxt.attributedPlaceholder = NSAttributedString(string: "password".localized,
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let newToText = "new_to_cargo_track".localized
        let normalAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5), NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
        
        let attributedString = NSMutableAttributedString(string: newToText, attributes: normalAttributes)
        
        let boldAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5), NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 12)!]
        let cargoTrackString = NSAttributedString(string: " CargoTrack?", attributes: boldAttributes)
        attributedString.append(cargoTrackString)
    }
    
    private func goToFleetScreen() {
        showActivityIndicator()
        APIClient().perform(UserService.getUserRightsRequest()) { result in
            self.hideActivityIndicator()
            switch result {
            case .success(let x):
                if let response = try? x.decode(to: [String?].self) {
                    StateController.currentUser?.userOptions = response.body
                    if let encoded = try? JSONEncoder().encode(StateController.currentUser) {
                        UserDefaults.standard.set(encoded, forKey: "user")
                        UserDefaults.standard.synchronize()
                    }
                    DispatchQueue.main.async {
                        let fleetCoordinator = FleetCoordinator(presenter: self.navigationController ?? UINavigationController())
                        fleetCoordinator.start()
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.presentAlertController(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }
    
    private func goToMobileWrapper() {
        showActivityIndicator()
        APIClient().perform(UserService.getUserRightsRequest()) { result in
            self.hideActivityIndicator()
            switch result {
            case .success(let x):
                if let response = try? x.decode(to: [String?].self) {
                    StateController.currentUser?.userOptions = response.body
                    if let encoded = try? JSONEncoder().encode(StateController.currentUser) {
                        UserDefaults.standard.set(encoded, forKey: "user")
                        UserDefaults.standard.synchronize()
                    }
                    DispatchQueue.main.async {
                        let fleetWebViewCoordinator = FleetWebviewCoordinator(presenter: self.navigationController ?? UINavigationController())
                        fleetWebViewCoordinator.start()
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.presentAlertController(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }
    
    private func goToFingerprint() {
        showActivityIndicator()
        APIClient().perform(UserService.getUserRightsRequest()) { result in
            self.hideActivityIndicator()
            switch result {
            case .success(let x):
                if let response = try? x.decode(to: [String?].self) {
                    StateController.currentUser?.userOptions = response.body
                    if let encoded = try? JSONEncoder().encode(StateController.currentUser) {
                        UserDefaults.standard.set(encoded, forKey: "user")
                        UserDefaults.standard.synchronize()
                    }
                    DispatchQueue.main.async {
                        let fingerprintCoordinator = FingerprintFaceIDCoordinator(presenter: self.navigationController ?? UINavigationController())
                        fingerprintCoordinator.start()
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.presentAlertController(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }
    
    @IBAction func didTapForgotPassword(_ sender: Any) {
        let forgotPassCoordinator = ForgotPassViewCoordinator(presenter: navigationController ?? UINavigationController())
        forgotPassCoordinator.start()
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapSignIn(_ sender: Any) {
        signIn()
    }
    
    @IBAction func didtapSignInWithFingerprintFaceID(_ sender: Any) {
        AuthenticationManager.checkAuthMethod { (success, authenticationError) in
            if success {
                if let storedUsername = UserDefaults.standard.value(forKey: "username") as? String {
                    let keychain = KeychainSwift()
                    let pass = keychain.get("password")
                    if !storedUsername.isEmpty && !(pass?.isEmpty ?? true) {
                        self.signIn(userKeychain: storedUsername, pass: pass)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.presentAlertController(title: "error_title".localized, message: "You did not login using your fingerprint/FaceID yet")
                    }
                }
            } else {
                DispatchQueue.main.async {
                    switch authenticationError {
                    case .some(let error):
                        switch error {
                        case .noBiometrics:
                            self.presentAlertController(title: "error_title".localized, message: "device_not_configured_fingerprint".localized)
                        case .failure:
                            self.presentAlertController(title: "error_title".localized, message: "fingerprint_failed".localized)
                        }
                    case .none:
                        break
                    }
                }
            }
        }
    }
    
    private func goToRoadTax() {
        DispatchQueue.main.async {
            let roadTaxCoordinator = RoadTaxCoordinator(presenter: self.navigationController ?? UINavigationController())
            roadTaxCoordinator.isFromFingerprint = true
            roadTaxCoordinator.start()
        }
    }
    
    @IBAction func didSwitchRememberMe(_ sender: UISwitch) {
//        UserDefaults.standard.set(sender.isOn, forKey: "rememberMe")
//        UserDefaults.standard.synchronize()
    }
    
    @IBAction func didTapSeePassword(_ sender: Any) {
        passTxt.isSecureTextEntry = !passTxt.isSecureTextEntry
    }
    
    private func signIn(userKeychain: String? = nil, pass: String? = nil) {
        showActivityIndicator()
        let password = pass != nil ? pass! : passTxt.text!
        APIClient().perform(UserService.createLoginRequest(username: userKeychain != nil ? userKeychain! : userTxt.text!, password: password, rememberMe: true)) { result in
            switch result {
            case .success(let x):
                if let response = try? x.decode(to: User.self) {
                    var isFirstLogin = true
                    var user = response.body
                    UserService.shared.accessToken = "Bearer \(user.token)"
                    let userDefaults = UserDefaults.standard
                    if userDefaults.data(forKey: "user") != nil {
                        isFirstLogin = false
                    }
                    if let frontendPreferences = user.frontendPreferences,
                        let frontEndPrefData = frontendPreferences.data(using: .utf8),
                        let frontendPreferencesDict = try? JSONSerialization.jsonObject(with: frontEndPrefData, options: .mutableContainers) as? [String: Any],
                        let stopDuration = frontendPreferencesDict["stopDuration"] as? String {
                        user.stopDuration = Int(stopDuration)
                    } else {
                        user.stopDuration = 0
                    }
                    if let encoded = try? JSONEncoder().encode(user) {
                        userDefaults.set(encoded, forKey: "user")
                    }
                    userDefaults.set(100, forKey: "historyPrecision")
                    let keychain = KeychainSwift()
                    keychain.set(password, forKey: "password")
                    keychain.set(user.token, forKey: "accessToken")
                    userDefaults.synchronize()
                    StateController.currentUser = user
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                        if !isFirstLogin {
                            if StateController.currentUser?.mobileAppAsWrapper ?? false {
                                self.goToMobileWrapper()
                            } else if user.userRights?.contains(where: { $0.id == 6 }) ?? false {
                                self.goToFleetScreen()
                            } else if user.userRights?.contains(where: { $0.id == 26 }) ?? false {
                                self.goToRoadTax()
                            }
                        } else if userKeychain != nil {
                            if StateController.currentUser?.mobileAppAsWrapper ?? false {
                                self.goToMobileWrapper()
                            } else if user.userRights?.contains(where: { $0.id == 6}) ?? false {
                                self.goToFleetScreen()
                            } else if user.userRights?.contains(where: { $0.id == 26 }) ?? false {
                                self.goToRoadTax()
                            }
                        } else {
                            self.goToFingerprint()
                        }
                    }
                } else if (try? x.decode(to: ErrorJson.self)) != nil {
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                        self.presentAlertController(title: "error_title".localized, message: "username_and_password_incorrect".localized)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                        self.presentAlertController(title: "error_title".localized, message: "username_and_password_incorrect".localized)
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.presentAlertController(title: "error_title".localized, message: error.localizedDescription)
                }
            }
        }
    }
}

extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !textField.text!.isEmpty && !userTxt.text!.isEmpty && textField == passTxt {
            didTapSignIn(self)
        } else {
            if textField == userTxt {
                passTxt.becomeFirstResponder()
            }
            signInButton.isUserInteractionEnabled = false
            signInButton.backgroundColor = .clear
            signInButton.setTitleColor(.white, for: .normal)
        }
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == userTxt {
            if !textField.text!.isEmpty && !passTxt.text!.isEmpty {
                signInButton.isUserInteractionEnabled = true
                signInButton.backgroundColor = .white
                signInButton.setTitleColor(view.tintColor, for: .normal)
            } else {
                signInButton.isUserInteractionEnabled = false
                signInButton.backgroundColor = .clear
                signInButton.setTitleColor(.white, for: .normal)
            }
        } else {
            if !textField.text!.isEmpty && !userTxt.text!.isEmpty {
                signInButton.isUserInteractionEnabled = true
                signInButton.backgroundColor = .white
                signInButton.setTitleColor(view.tintColor, for: .normal)
            } else {
                signInButton.isUserInteractionEnabled = false
                signInButton.backgroundColor = .clear
                signInButton.setTitleColor(.white, for: .normal)
            }
        }
    }
}
