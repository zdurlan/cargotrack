//
//  UpdateViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 02/05/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class UpdateViewController: BaseLightStatusBarViewController {
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "title_update".localized
        }
    }
    @IBOutlet private var textLabel: UILabel! {
        didSet {
            textLabel.text = "text_update".localized
        }
    }
    @IBOutlet private var updateButton: UIButton! {
        didSet {
            updateButton.setTitle("update".localized, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didTapUpdate(_ sender: Any) {
        guard let url = URL(string: "http://apps.apple.com/app/id/1471118536") else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
