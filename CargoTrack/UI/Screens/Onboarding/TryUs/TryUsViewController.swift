//
//  TryUsViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 25/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class TryUsViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var tryUsTitleLabel: UILabel! {
        didSet {
            tryUsTitleLabel.text = "try_us_out_button".localized
        }
    }
    @IBOutlet private weak var enterDetailsLabel: UILabel! {
        didSet {
            enterDetailsLabel.text = "enter_details_below_contact_24h".localized
        }
    }
    @IBOutlet private weak var submitButton: UIButton! {
        didSet {
            submitButton.setTitle("submit_button".localized, for: .normal)
        }
    }
    
    @IBOutlet private weak var nameTxt: UITextField! {
        didSet {
            nameTxt.addTarget(self, action: #selector(textFieldDidChange(_:)),
                              for: UIControl.Event.editingChanged)
            nameTxt.delegate = self
            nameTxt.returnKeyType = .next
            nameTxt.attributedPlaceholder = NSAttributedString(string: "name".localized,
                                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        }
    }
    @IBOutlet private weak var emailTxt: UITextField! {
        didSet {
            emailTxt.addTarget(self, action: #selector(textFieldDidChange(_:)),
                              for: UIControl.Event.editingChanged)
            emailTxt.delegate = self
            emailTxt.returnKeyType = .next
            emailTxt.attributedPlaceholder = NSAttributedString(string: "email_address".localized,
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
            emailTxt.addTarget(self, action: #selector(emailTxtChanged), for: .editingChanged)
        }
    }
    @IBOutlet private weak var phoneTxt: UITextField! {
        didSet {
            phoneTxt.addTarget(self, action: #selector(textFieldDidChange(_:)),
                              for: UIControl.Event.editingChanged)
            phoneTxt.delegate = self
            phoneTxt.returnKeyType = .next
            phoneTxt.attributedPlaceholder = NSAttributedString(string: "phone".localized,
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        }
    }
    @IBOutlet private weak var companyTxt: UITextField! {
        didSet {
            companyTxt.addTarget(self, action: #selector(textFieldDidChange(_:)),
                              for: UIControl.Event.editingChanged)
            companyTxt.delegate = self
            companyTxt.returnKeyType = .next
            companyTxt.attributedPlaceholder = NSAttributedString(string: "company".localized,
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        }
    }
    @IBOutlet private weak var messageTxt: UITextView! {
        didSet {
            messageTxt.text = "message".localized
            messageTxt.textColor = UIColor.white.withAlphaComponent(0.5)
            messageTxt.delegate = self
            messageTxt.returnKeyType = .done
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func didTapSubmit(_ sender: Any) {
        showActivityIndicator()
        APIClient().perform(UserService.createTryUsRequest(name: nameTxt.text!, email: emailTxt.text!, phone: phoneTxt.text!, company: companyTxt.text!, message: messageTxt.text!)) { result in
            self.hideActivityIndicator()
            switch result {
            case .success(let response):
                if response.statusCode == 200 {
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                        self.presentAlertController(title: "", message: "email_received_register".localized)
                    }
                } else {
                    self.presentAlertController(title: "error_title".localized, message: "register_error".localized)
                }
            case .failure(let error):
                self.presentAlertController(title: "error_title".localized, message: error.localizedDescription)
            }
        }
    }
    
    @objc func emailTxtChanged(textField: UITextField) {
        if !String.isValidEmail(textField.text!) {
            emailTxt.textColor = UIColor(red: 214/255, green: 10/255, blue: 44/255, alpha: 1)
        } else {
            emailTxt.textColor = UIColor.white
        }
    }
    
    private func checkFieldsCompletion() {
        if !nameTxt.text!.isEmpty && !emailTxt.text!.isEmpty && !phoneTxt.text!.isEmpty && !companyTxt.text!.isEmpty && !messageTxt.text!.isEmpty && String.isValidEmail(emailTxt.text!) {
            submitButton.isUserInteractionEnabled = true
            submitButton.backgroundColor = .white
            submitButton.setTitleColor(view.tintColor, for: .normal)
        } else {
            submitButton.isUserInteractionEnabled = false
            submitButton.backgroundColor = .clear
            submitButton.setTitleColor(.white, for: .normal)
        }
    }
}

extension TryUsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == nameTxt {
            emailTxt.becomeFirstResponder()
        } else if textField == emailTxt {
            phoneTxt.becomeFirstResponder()
        } else if textField == phoneTxt {
            companyTxt.becomeFirstResponder()
        } else if textField == companyTxt {
            messageTxt.becomeFirstResponder()
        }
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        checkFieldsCompletion()
    }
}

extension TryUsViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            view.endEditing(true)
            return false
        }
        checkFieldsCompletion()
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor != UIColor.white {
            textView.text = nil
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "message".localized
            textView.textColor = UIColor.white.withAlphaComponent(0.5)
        }
    }
}
