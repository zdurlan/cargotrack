//
//  ViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 20/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class OnboardingViewController: BaseLightStatusBarViewController {
    //MARK: - Outlets
    @IBOutlet private weak var buildLabel: UILabel!
    @IBOutlet private weak var connectLabel: UILabel!
    @IBOutlet private weak var signInButton: UIButton!
    @IBOutlet private weak var newToCargoTrackLabel: UILabel!
    @IBOutlet private weak var tryUsOutButton: UIButton!
    @IBOutlet private weak var languageLabel: UILabel!
    @IBOutlet private weak var languageButton: UIButton!
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    private func setupUI() {
        connectLabel.text = "connect_to_your_account".localized
        languageLabel.text = "language".localized
        let newToText = "new_to_cargo_track".localized
        let normalAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5), NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
        
        let attributedString = NSMutableAttributedString(string: newToText, attributes: normalAttributes)
        
        let boldAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5), NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 12)!]
        let cargoTrackString = NSAttributedString(string: " CargoTrack?", attributes: boldAttributes)
        attributedString.append(cargoTrackString)
        
        newToCargoTrackLabel.attributedText = attributedString
        signInButton.setTitle("sign_in_button".localized, for: .normal)
        tryUsOutButton.setTitle("try_us_out_button".localized, for: .normal)
        languageButton.setTitle(UserDefaults.standard.string(forKey: "language")?.localized ?? "english".localized, for: .normal)
        
        buildLabel.text = getCurrentAppVersion()
    }

    @IBAction func didTapEnglish(_ sender: Any) {
        let englishAction = UIAlertAction(title: "English", style: .default) {  [weak self] (action) in
            guard let strongSelf = self else {
                return
            }
            Bundle.setLanguage(lang: "en")
            UserDefaults.standard.set("english", forKey: "language")
            strongSelf.setupUI()
        }
        let romanianAction = UIAlertAction(title: "Romanian", style: .default) { [weak self] (action) in
            guard let strongSelf = self else {
                return
            }
            Bundle.setLanguage(lang: "ro")
            UserDefaults.standard.set("romanian", forKey: "language")
            strongSelf.setupUI()
        }
        let germanAction = UIAlertAction(title: "German", style: .default) {  [weak self] (action) in
            guard let strongSelf = self else {
                return
            }
            Bundle.setLanguage(lang: "de")
            UserDefaults.standard.set("german", forKey: "language")
            strongSelf.setupUI()
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        presentAlertController(title: nil, message: nil, actions: [englishAction, romanianAction, germanAction, cancelAction], style: .actionSheet)
    }
    
    @IBAction private func didTapSignIn(_ sender: Any) {
        let signInCoordinator = SignInCoordinator(presenter: navigationController ?? UINavigationController())
        signInCoordinator.start()
    }
    
    @IBAction private func didTapTryUsOut(_ sender: Any) {
        let tryUsCoordinator = TryUsCoordinator(presenter: navigationController ?? UINavigationController())
        tryUsCoordinator.start()
    }
    
    private func getCurrentAppVersion() -> String {
        guard let info = Bundle.main.infoDictionary,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                return ""
        }
        
        showActivityIndicator()
        guard let data = try? Data(contentsOf: url) else {
            return ""
        }
        guard let json = try? JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            hideActivityIndicator()
            return ""
        }
        hideActivityIndicator()
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            return "app_version".localized + " " + version
        }
        return ""
    }
}

