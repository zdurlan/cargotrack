//
//  AllVehiclesMapViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 02/09/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import GoogleMaps
import RealmSwift
import UIKit

class AllVehiclesMapViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "all_vehicles_title".localized
        }
    }
    @IBOutlet private weak var mapView: GMSMapView!
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    
    var vehicles: [Vehicle]!
    private var bounds = GMSCoordinateBounds()
    private var allMarkers = [GMSMarker]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: .didUpdateVehicles, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadLatestPositions), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
        loadMarkers()
        mapView.setMinZoom(1, maxZoom: 15)
        
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        mapView.animate(with: update)
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
        mapView.setMinZoom(1, maxZoom: 20)
        mapView.delegate = self
    }
    
    private func loadMarkers() {
        mapView.clear()
        vehicles.forEach {
            let carMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: Double($0.usedValues?.latitude ?? "") ?? 0, longitude: Double($0.usedValues?.longitude ?? "") ?? 0))
            carMarker.map = mapView
            bounds = bounds.includingCoordinate(carMarker.position)
            if let carStatus = $0.usedValues?.status {
                if let status = Int(carStatus) {
                    switch status {
                    case 0:
                        let image = self.imageWithImage(image: UIImage(named: "carParkedPin")!, scaledToSize: CGSize(width: 30.0, height: 30.0))
                        carMarker.icon = CustomCarMarkerInfoWindow.annotationImage(name: $0.label != nil && !($0.label?.isEmpty ?? true) ? $0.label : $0.name, carStatus: .parking, pinIcon: image, course: $0.usedValues?.course)
                    case 1:
                        let image = self.imageWithImage(image: UIImage(named: "carNeutralPin")!, scaledToSize: CGSize(width: 30.0, height: 30.0))
                        carMarker.icon = CustomCarMarkerInfoWindow.annotationImage(name: $0.label != nil && !($0.label?.isEmpty ?? true) ? $0.label : $0.name, carStatus: .neutral, pinIcon: image, course: $0.usedValues?.course)
                    case 2:
                        carMarker.icon = CustomCarMarkerInfoWindow.annotationImage(name: $0.label != nil && !($0.label?.isEmpty ?? true) ? $0.label : $0.name, carStatus: .driving, pinIcon: UIImage(named: "carDrivingPin")!, course: $0.usedValues?.course)
                    default:
                        break
                    }
                } else {
                    let image = self.imageWithImage(image: UIImage(named: "carParkedPin")!, scaledToSize: CGSize(width: 30.0, height: 30.0))
                    carMarker.icon = CustomCarMarkerInfoWindow.annotationImage(name: $0.label != nil ? $0.label : $0.name, carStatus: .parking, pinIcon: image, course: $0.usedValues?.course)
                }
            } else {
                let image = self.imageWithImage(image: UIImage(named: "carParkedPin")!, scaledToSize: CGSize(width: 30.0, height: 30.0))
                carMarker.icon = CustomCarMarkerInfoWindow.annotationImage(name: $0.label != nil ? $0.label : $0.name, carStatus: .parking, pinIcon: image, course: $0.usedValues?.course)
            }
            carMarker.groundAnchor = CGPoint(x: 0.5, y: 0.6)
            carMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.8)
            carMarker.userData = $0.id
            carMarker.tracksViewChanges = false
            carMarker.tracksInfoWindowChanges = false
            allMarkers.append(carMarker)
        }
    }
    
    private func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    private func updateMarkers() {
        self.vehicles.forEach { vehicle in
            let marker = self.allMarkers.first(where: {
                $0.userData as! Int == vehicle.id
            })
            marker?.position = CLLocationCoordinate2D(latitude: Double(vehicle.usedValues?.latitude ?? "") ?? 0, longitude: Double(vehicle.usedValues?.longitude ?? "") ?? 0)
        }
    }
    
    @objc func reloadLatestPositions() {
        for vehicle in self.vehicles {
            let vehicleRef = ThreadSafeReference(to: vehicle)
            DispatchQueue.global(qos: .background).async {
                let realm = try! Realm()
                guard let vehicle = realm.resolve(vehicleRef) else {
                    return // object was deleted
                }
                if let storedVehicle = DBManager.sharedInstance.getVehicle(id: vehicle.id),
                    let deviceTime = storedVehicle.deviceTime {
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                    dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                    let deviceDate = dateFormatter.date(from: deviceTime)!
                    let components = Calendar.current.dateComponents([.hour, .minute], from: deviceDate, to: Date())
                    let minutesDifference = components.minute ?? 0
                    let vehicleName = storedVehicle.name
                    if minutesDifference > 1 {
                        let dateTime = dateFormatter.string(from: Date())
                        var newPositions: [[String: Any]]?
                        let newPositionsSemaphore = DispatchSemaphore(value: 0)
                        APIClient().perform(DeviceService.createDevicePositionsRequest(deviceId: [vehicle.secondaryId], dsServer: vehicle.hostServer, dateTime: dateTime)) { result in
                            switch result {
                            case .success(let response):
                                guard let json = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: [.allowFragments]) as? [[String: Any]], json.count > 0 else {
                                    APIClient().perform(SlackService.sendMessage("Could not refresh \(vehicleName) on user: \(StateController.currentUser?.name ?? "") positions from API")) { _ in }
                                    debugPrint("error decoding JSON")
                                    newPositionsSemaphore.signal()
                                    return
                                }
                                newPositions = json
                                newPositionsSemaphore.signal()
                            case .failure:
                                APIClient().perform(SlackService.sendMessage("Error decoding JSON \(vehicle.name) on user: \(StateController.currentUser?.name ?? "") positions from API")) { _ in }
                            }
                        }
                        newPositionsSemaphore.wait()
                        if newPositions != nil {
                            let newPos = newPositions!.first(where: {
                                $0["deviceid"] as! Int == vehicle.secondaryId
                            })!
                            DBManager.sharedInstance.updateVehicleFromBackground(vehicle, newPosition: newPos)
                        }
                    }
                }
            }
        }
        WSSClient.sharedInstance.webSockets.forEach {
            $0.connect()
        }//update to a better method maybe when deleting realm
    }
    
    @objc func reloadData() {
        vehicles = Array(DBManager.sharedInstance.getVehiclesFromDB())
        updateMarkers()
    }
    
    @IBAction func didTapTraffic(_ sender: Any) {
        mapView.isTrafficEnabled = !mapView.isTrafficEnabled
    }
    
    @IBAction func didTapMapType(_ sender: Any) {
        let satelliteAction = UIAlertAction(title: "satellite".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .satellite
        }
        let hybridAction = UIAlertAction(title: "hybrid".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .hybrid
        }
        let normalAction = UIAlertAction(title: "normal".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .normal
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        presentAlertController(title: nil, message: nil, actions: [satelliteAction, hybridAction, normalAction, cancelAction], style: .actionSheet)
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension AllVehiclesMapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let vehicle = vehicles.first(where: { $0.id == marker.userData as! Int })!
        let vehicleDetailsCoordinator = VehicleDetailsCoordinator(presenter: navigationController ?? UINavigationController(), vehicle: vehicle)
        vehicleDetailsCoordinator.start()
        return false
    }
}
