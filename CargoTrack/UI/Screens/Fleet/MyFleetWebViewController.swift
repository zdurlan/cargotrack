//
//  MyFleetWebViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 26/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class MyFleetWebViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var navBar: UIView!
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var webViewContainer: UIView!
    @IBOutlet private weak var webActivityIndicator: UIActivityIndicatorView!
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private weak var myFleetTitleLabel: UILabel! {
        didSet {
            myFleetTitleLabel.text = "my_fleet_title".localized
        }
    }
    
    private var webView: WKWebView!
    private var urlObservation: NSKeyValueObservation?
    private var fromBackground = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWebView), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
        setupLayout()
    }
    
    private func setupLayout() {
        setupWebView()
        loadPage()
    }
    
    private func setupWebView() {
        webView = WKWebView()
        webView.scrollView.delegate = self
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webViewContainer.addSubview(webView)
        webView.pin(to: webViewContainer)
    }
    
    private func setupWebViewRetry() {
        webActivityIndicator.startAnimating()
        webView.reload()
    }
    
    @objc private func reloadWebView() {
        fromBackground = true
        loadPage()
    }
    
    private func getMobileWrapperURL(onComplete: @escaping (URL?) -> Void) {
        APIClient().perform(UserService.createMobileAsWrapperRequest()) { result in
            DispatchQueue.main.async {
                self.webActivityIndicator.stopAnimating()
            }
            switch result {
            case .success(let response):
                if let response = try? response.decode(to: URLJson.self) {
                    onComplete(URL(string: response.body.url))
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.presentAlertController(title: "error_title".localized, message: error.localizedDescription)
                }
            }
        }
    }
    
    private func loadPage() {
        webActivityIndicator.startAnimating()
        getMobileWrapperURL { url in
            DispatchQueue.main.async {
                self.webView.load(URLRequest(url: url!))
                if self.fromBackground {
                    self.webView.reload()
                }
            }
        }
    }
    
    private func makeRetryAlertActions() -> [UIAlertAction] {
        let retryAction = UIAlertAction(title: "retry_button".localized, style: .default) { [weak self] (alert) in
            self?.setupWebViewRetry()
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel) { [weak self] (alert) in
            self?.navigationController?.popViewController(animated: true)
        }
        return [retryAction, cancelAction]
    }
    
    @IBAction func didTapMenu(_ sender: Any) {
        var menuAlreadyPresent = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MainMenuViewController.self) {
                menuAlreadyPresent = true
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        if !menuAlreadyPresent {
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
            view.window!.layer.add(transition, forKey: kCATransition)
            let mainMenuCoordinator = MainMenuCoordinator(presenter: navigationController ?? UINavigationController())
            mainMenuCoordinator.start()
        }
    }
    
    deinit {
        urlObservation?.invalidate()
    }
    
}

extension MyFleetWebViewController: WKUIDelegate, WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        webActivityIndicator.stopAnimating()
        presentAlertController(title: "error_title".localized, message: "page_not_load".localized, actions: makeRetryAlertActions())
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webActivityIndicator.stopAnimating()
    }
}

extension MyFleetWebViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.maximumZoomScale = 1.0
    }
}
