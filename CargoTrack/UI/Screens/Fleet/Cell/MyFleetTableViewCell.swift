//
//  MyFleetTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 05/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class MyFleetTableViewCell: UITableViewCell {
    @IBOutlet private weak var vehicleNameLabel: UILabel!
    @IBOutlet private weak var carStatusImage: UIImageView!
    @IBOutlet private weak var carSpeedLabel: UILabel!
    @IBOutlet private weak var carLocationLabel: UILabel!
    @IBOutlet private weak var carBatteryView: UIView!
    @IBOutlet private weak var carKmView: UIView!
    @IBOutlet private weak var carFuelView: UIView!
    @IBOutlet private weak var carOilView: UIView!
    @IBOutlet private weak var carBatteryLabel: UILabel!
    @IBOutlet private weak var carKmLabel: UILabel!
    @IBOutlet private weak var carFuelLabel: UILabel!
    @IBOutlet private weak var carOilLabel: UILabel!
    @IBOutlet private weak var flagImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with vehicle: Vehicle) {
        vehicleNameLabel.text = vehicle.label?.isEmpty ?? true ? vehicle.name : vehicle.label
        if let carStatus = vehicle.usedValues?.status {
            checkVehicleStatus(carStatus)
        } else if let oldStatus = vehicle.oldValues?.status {
            checkVehicleStatus(oldStatus)
        } else {
            carStatusImage.isHidden = true
        }
        
        if let mileage = vehicle.usedValues?.mileage {
            checkMileageValue(mileage, vehicle: vehicle)
        } else if let oldMileage = vehicle.oldValues?.mileage {
            checkMileageValue(oldMileage, vehicle: vehicle)
        } else{
            carKmView.isHidden = true
        }
        
        if let speed = vehicle.usedValues?.speed {
            checkSpeedValue(speed)
        } else if let oldSpeed = vehicle.oldValues?.speed {
            checkSpeedValue(oldSpeed)
        } else {
            carSpeedLabel.isHidden = true
        }
        
        if let countryCode = vehicle.usedValues?.countryCode {
            flagImage.image = UIImage(named: countryCode.uppercased())
        } else {
            flagImage.image = nil
        }
       
        if StateController.getShowCoordinatesOnFleetScreenValue() {
            let latitude = vehicle.usedValues?.latitude ?? "loading".localized
            let longitude = vehicle.usedValues?.longitude ?? "loading".localized
            carLocationLabel.text = latitude + ", " + longitude
        } else {
            carLocationLabel.text = vehicle.usedValues?.location ?? "loading".localized
        }
        
        if let powerSupply = vehicle.usedValues?.powerSupply {
            checkPowerSupply(powerSupply)
        } else if let oldPowerSupply = vehicle.oldValues?.powerSupply {
            checkPowerSupply(oldPowerSupply)
        } else {
            carBatteryView.isHidden = false
        }
        
        if let fuelLevel = vehicle.usedValues?.fuelLevel {
            checkFuelLevel(fuelLevel)
        } else if let oldFuelLevel = vehicle.oldValues?.fuelLevel {
            checkFuelLevel(oldFuelLevel)
        } else {
            carFuelView.isHidden = true
        }
        
        if let fuelConsumption = vehicle.usedValues?.fuelConsumption {
            checkFuelConsumption(fuelConsumption)
        } else if let oldFuelConsumption = vehicle.oldValues?.fuelConsumption {
            checkFuelConsumption(oldFuelConsumption)
        } else {
            carOilView.isHidden = true
        }
    }
    
    private func checkVehicleStatus(_ carStatus: String) {
        if let status = Int(carStatus) {
            carStatusImage.isHidden = false
            switch status {
            case 0:
                carStatusImage.image = UIImage(named: "parkedIcon")
            case 1:
                carStatusImage.image = UIImage(named: "neutralIcon")
            case 2:
                carStatusImage.image = UIImage(named: "drivingIcon")
            default:
                carStatusImage.isHidden = true
                break
            }
        } else {
            carStatusImage.isHidden = true
        }
    }
    
    private func checkMileageValue(_ mileage: String, vehicle: Vehicle) {
        carKmView.isHidden = false
        if let mileageDouble = Double(mileage) {
            carKmLabel.text = String(format: "%.0f", mileageDouble + Double(vehicle.attributeCorrections?.attrMileageCorrectVal ?? 0)) + "km"
        } else {
            carKmView.isHidden = true
        }
    }
    
    private func checkSpeedValue(_ speed: String) {
        carSpeedLabel.isHidden = false
        carSpeedLabel.text = "\(speed) km/h"
    }
    
    private func checkPowerSupply(_ powerSupply: String) {
        carBatteryView.isHidden = false
        if let doublePowerSupply = Double(powerSupply) {
            carBatteryLabel.text = String(format: "%.1f", doublePowerSupply/1000.0).replacingOccurrences(of: ",", with: ".") + "v"
        } else {
            carBatteryView.isHidden = true
        }
    }
    
    private func checkFuelLevel(_ fuelLevel: String) {
        carFuelView.isHidden = false
        if let doubleFuelLevel = Double(fuelLevel), doubleFuelLevel > 0 {
            carFuelLabel.text = String(format: "%.0f", doubleFuelLevel) + "L"
        } else {
            carFuelView.isHidden = true
        }
    }
    
    private func checkFuelConsumption(_ fuelConsumption: String) {
        carOilView.isHidden = false
        if let doubleFuelConsumption = Double(fuelConsumption) {
            carOilLabel.text = String(format: "%.1f", doubleFuelConsumption).replacingOccurrences(of: ",", with: ".") + "%"
        } else {
            carOilView.isHidden = true
        }
    }
}

