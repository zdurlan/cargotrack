//
//  FuelConsumptionViewController.swift
//  CargoTrack
//
//  Created by Andrei Stefanescu on 10/01/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import RealmSwift
import UIKit

class FuelConsumptionViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var dateTextField: UITextField! {
        didSet {
            dateTextField.borderStyle = .none
        }
    }
    @IBOutlet private weak var timeTextField: UITextField! {
        didSet {
            timeTextField.borderStyle = .none
        }
    }
    @IBOutlet private weak var consumptionFromLabel: UILabel! {
        didSet {
            consumptionFromLabel.text = "consumption_from".localized
        }
    }
    @IBOutlet private weak var consumptionFromDateLabel: UILabel! {
        didSet {
            consumptionFromDateLabel.text = vehicle.fuelConsumption?.datetime
        }
    }
    @IBOutlet private weak var saveButton: UIButton! {
        didSet {
            saveButton.setTitle("save_button".localized, for: .normal)
        }
    }
    @IBOutlet private weak var modifyTheDateLabel: UILabel! {
        didSet {
            modifyTheDateLabel.text = "modify_the_date".localized
        }
    }
    @IBOutlet private weak var fuelConsumptionLabel: UILabel! {
        didSet {
            if let fuelConsumption = vehicle.usedValues?.fuelConsumption {
                if let doubleFuelConsumption = Double(fuelConsumption) {
                    fuelConsumptionLabel.text = String(format: "%.2f", doubleFuelConsumption).replacingOccurrences(of: ",", with: ".") + "%"
                } else {
                    fuelConsumptionLabel.text = "-"
                }
            } else  if let fuelConsumption = vehicle.oldValues?.fuelConsumption {
                if let doubleFuelConsumption = Double(fuelConsumption) {
                    fuelConsumptionLabel.text = String(format: "%.2f", doubleFuelConsumption).replacingOccurrences(of: ",", with: ".") + "%"
                } else {
                    fuelConsumptionLabel.text = "-"
                }
            } else {
                fuelConsumptionLabel.text = "-"
            }
        }
    }
    @IBOutlet private weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "fuel_consumption".localized
        }
    }
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var dateLabel: UILabel! {
        didSet {
            dateLabel.text = "date".localized
        }
    }
    @IBOutlet private weak var calculateButton: UIButton! {
        didSet {
            calculateButton.setTitle("calculate".localized, for: .normal)
        }
    }
    
    var vehicle: Vehicle!
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
        
        let toolBarDate = UIToolbar().ToolbarPicker(mySelect: #selector(dismissDate))
        let toolBarTime = UIToolbar().ToolbarPicker(mySelect: #selector(dismissTime))
        dateTextField.inputView = datePicker
        dateTextField.inputAccessoryView = toolBarDate
        datePicker.datePickerMode = .date
        datePicker.locale = Locale.current
        datePicker.maximumDate = Date()
        
        timeTextField.inputView = timePicker
        timeTextField.inputAccessoryView = toolBarTime
        timePicker.datePickerMode = .time
        timePicker.locale = Locale.current
        timePicker.maximumDate = Date()
        
        timePicker.addTarget(self, action: #selector(handlePickerTime), for: .valueChanged)
        datePicker.addTarget(self, action: #selector(handlePickerDate), for: .valueChanged)
        
        dateFormatter.locale = Locale.current
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let deviceDate = dateFormatter.date(from: vehicle.fuelConsumption?.datetime ?? "")
        dateFormatter.dateFormat = StateController.getDateFormat().replacingOccurrences(of: "HH:mm:ss", with: "")
        dateTextField.text = dateFormatter.string(from: deviceDate ?? Date())
        datePicker.date = dateFormatter.date(from: dateTextField.text ?? "") ?? Date()
        dateFormatter.dateFormat = "HH:mm"
        timeTextField.text = dateFormatter.string(from: Date())
        timePicker.date = dateFormatter.date(from: timeTextField.text ?? "") ?? Date()
    }
    
    @objc override func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc private func handlePickerDate() {
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = StateController.getDateFormat().replacingOccurrences(of: "HH:mm:ss", with: "")
        dateTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    @objc private func handlePickerTime() {
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "HH:mm"
        timeTextField.text = dateFormatter.string(from: timePicker.date)
    }
    
    @objc func dismissDate() {
        dateFormatter.dateFormat = StateController.getDateFormat().replacingOccurrences(of: "HH:mm:ss", with: "")
        dateTextField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    @objc func dismissTime() {
        dateFormatter.dateFormat = "HH:mm"
        timeTextField.text = dateFormatter.string(from: timePicker.date)
        view.endEditing(true)
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    private func calculateFuelConsumption(shouldSave: Bool) {
        let pickerDate = dateTextField.text! + timeTextField.text!
        dateFormatter.dateFormat = StateController.getDateFormat().replacingOccurrences(of: ":ss", with: "")
        let dateToString = dateFormatter.date(from: pickerDate)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var dateToSendString = dateFormatter.string(from: dateToString ?? Date())
        let vehicleSecondaryId = vehicle.secondaryId
        let vehicleMileage = vehicle.attributes?.mileage
        let vehicleHostServer = vehicle.hostServer
        let vehicleFuelConsumption = vehicle.attributes?.fuelConsumption
        let definitionsDict = try! JSONSerialization.jsonObject(with: vehicle.attributeDefinitionsData!, options: .mutableContainers) as! [String: Any]
        showActivityIndicator()
        APIClient().perform(DeviceService.createDevicePositionsRequest(deviceId: [vehicle.secondaryId], dsServer: vehicle.hostServer, dateTime: dateToSendString)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success(let response):
                guard let json = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: [.allowFragments]) as? [[String: Any]], json.count > 0 else {
                    print("error decoding JSON")
                    return
                }
                let newDeviceAttributeLogic = DeviceAttributeLogicDictionary(rawValues: json.first(where: {
                    $0["deviceid"] as! Int == vehicleSecondaryId
                })!, definitions: definitionsDict)
                let oldMileage = newDeviceAttributeLogic.getValue(attributeKey: vehicleMileage ?? "")
                let oldFuelConsumption = newDeviceAttributeLogic.getValue(attributeKey: vehicleFuelConsumption ?? "")
                if oldFuelConsumption != "LOGIC_ERROR" && oldMileage != "LOGIC_ERROR" {
                    dateToSendString = strongSelf.dateFormatter.string(from: Date())
                    strongSelf.showActivityIndicator()
                    APIClient().perform(DeviceService.createDevicePositionsRequest(deviceId: [vehicleSecondaryId], dsServer: vehicleHostServer, dateTime: dateToSendString)) { [weak self] result in
                        guard let strongSelf = self else {
                            return
                        }
                        strongSelf.hideActivityIndicator()
                        switch result {
                        case .success(let response):
                            guard let json = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: [.allowFragments]) as? [[String: Any]], json.count > 0 else {
                                print("error decoding JSON")
                                return
                            }
                            let newDeviceAttributeLogic = DeviceAttributeLogicDictionary(rawValues: json.first(where: {
                                $0["deviceid"] as! Int == vehicleSecondaryId
                            })!, definitions: definitionsDict)
                            let newMileage = newDeviceAttributeLogic.getValue(attributeKey: vehicleMileage ?? "")
                            let newFuelConsumption = newDeviceAttributeLogic.getValue(attributeKey: vehicleFuelConsumption ?? "")
                            if newFuelConsumption != "LOGIC_ERROR" && newMileage != "LOGIC_ERROR" {
                                let mileageKm = Double(newMileage)! - Double(oldMileage)!
                                if mileageKm > 0 {
                                    let consumption = Double(newFuelConsumption)! - Double(oldFuelConsumption)! 
                                    let lastConsumption = (100 * consumption) / mileageKm
                                    DispatchQueue.main.async {
                                        strongSelf.fuelConsumptionLabel.text = String(format: "%.2f", lastConsumption).replacingOccurrences(of: ",", with: ".") + "%"
                                        strongSelf.consumptionFromDateLabel.text = pickerDate
                                        if shouldSave {
                                            let realm = try! Realm()
                                            try! realm.write {
                                                strongSelf.vehicle.fuelConsumption?.datetime = pickerDate
                                                strongSelf.vehicle.fuelConsumption?.mileage.value = Double(newMileage)!
                                                strongSelf.vehicle.fuelConsumption?.sensor.value = Double(newFuelConsumption)!
                                                strongSelf.vehicle.usedValues?.fuelConsumption = String(lastConsumption)
                                                strongSelf.vehicle.oldValues?.fuelConsumption = String(lastConsumption)
                                            }
                                            strongSelf.saveNewFuelConsumption()
                                        }
                                    }
                                }
                            }
                        case .failure(let error):
                            strongSelf.presentAlertController(title: "", message: error.localizedDescription)
                        }
                    }
                }
            case .failure(let error):
                strongSelf.presentAlertController(title: "", message: error.localizedDescription)
            }
        }
    }
    
    private func saveNewFuelConsumption() {
        let pickerDate = dateTextField.text! + timeTextField.text!
        let dateFormatterX = DateFormatter()
        dateFormatterX.dateFormat = StateController.getDateFormat().replacingOccurrences(of: ":ss", with: "")
        dateFormatter.dateFormat = StateController.getDateFormat()
        let dateToSendDate = dateFormatterX.date(from: pickerDate)
        dateFormatterX.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dateToSendString = dateFormatter.string(from: dateToSendDate ?? Date()).appending(":00")
        showActivityIndicator()
        APIClient().perform(DeviceService.updateFuelConsumptionRequest(vehicleId: vehicle.id, dsServer: vehicle.hostServer, dateTime: dateToSendString, mileage: Double(vehicle.fuelConsumption?.mileage.value ?? 0), sensor: Double(vehicle.fuelConsumption?.sensor.value ?? 0))) { result in
            self.hideActivityIndicator()
            switch result {
            case .success(let response):
                if response.statusCode == 200 {
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.presentAlertController(title: "", message: "error_message".localized)
                    }
                }
            case .failure:
                DispatchQueue.main.async {
                    self.presentAlertController(title: "", message: "error_message".localized)
                }
            }
        }
    }
    
    @IBAction func didTapCalculate(_ sender: Any) {
        calculateFuelConsumption(shouldSave: false)
    }
    
    @IBAction func didTapSave(_ sender: Any) {
        calculateFuelConsumption(shouldSave: true)
    }
}
