//
//  VehicleHistoryStopTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 25/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class VehicleHistoryStopTableViewCell: UITableViewCell {
    @IBOutlet private weak var stopTimeLabel: UILabel!
    @IBOutlet private weak var stopNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(stop: DeviceHistory) {
        stopNameLabel.text = "stop".localized + " \(stop.stopNumber ?? 0)"
        let stopDuration = stop.duration.stopped + stop.duration.idling
        hmsFrom(seconds: stopDuration) { (hours, minutes, seconds) in
            self.stopTimeLabel.text = "\(hours)h \(minutes)m"
        }
    }
    
    func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        
    }
}

