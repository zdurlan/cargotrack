//
//  VehicleHistoryTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 07/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class VehicleHistoryTableViewCell: UITableViewCell {
    @IBOutlet private weak var startFlagImage: UIImageView!
    @IBOutlet private weak var endFlagImage: UIImageView!
    @IBOutlet private weak var startLocationLabel: UILabel!
    @IBOutlet private weak var endLocationLabel: UILabel!
    @IBOutlet private weak var startDateLabel: UILabel!
    @IBOutlet private weak var endDateLabel: UILabel!
    @IBOutlet private weak var speedLabel: UILabel!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var mileageLabel: UILabel!
    @IBOutlet private weak var fuelConsumptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(trip: DeviceHistory) {
        guard let countryCodeStart = trip.address.begin.addressElements["country_code"] as? String,
            let countryCodeEnd = trip.address.end.addressElements["country_code"] as? String else {
                return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let deviceDateStart = dateFormatter.date(from: trip.period.begin)!
        let deviceDateEnd = dateFormatter.date(from: trip.period.end) ?? Date()
        dateFormatter.dateFormat = StateController.getDateFormat()
        dateFormatter.timeZone = TimeZone.current
        let deviceDateEndRepresented = dateFormatter.string(from: deviceDateEnd)
        let deviceDateStartRepresented = dateFormatter.string(from: deviceDateStart)
        startFlagImage.image = UIImage(named: countryCodeStart.uppercased())
        startLocationLabel.text = trip.address.begin.addressFormatted
        startDateLabel.text = deviceDateStartRepresented
        
        endFlagImage.image = UIImage(named: countryCodeEnd.uppercased())
        endLocationLabel.text = trip.address.end.addressFormatted
        endDateLabel.text = deviceDateEndRepresented
        
        var movingTime = trip.duration.moving
        let (hoursDriving, minutesDriving) = movingTime.secondsToHoursMinutesSeconds()
        timeLabel.text = "\(hoursDriving)h \(minutesDriving)m"
        let tripDistance = round(trip.distance*1000)/1000
        if trip.distance < 1 {
            mileageLabel.text = String(format: "%.0f", trip.distance*1000) + "m"
        } else {
            mileageLabel.text = String(format: "%.0f", round(trip.distance)) + "km"
        }
        if let lastConsumptionValue = trip.positions.last?["fcs"]?.value as? Double,
            let firstConsumptionValue = trip.positions.first?["fcs"]?.value as? Double {
            let result = (lastConsumptionValue - firstConsumptionValue) * 100 / tripDistance
            fuelConsumptionLabel.text = result > 0 ? "\(result.truncate(places: 2))L" : "-"
        } else {
            fuelConsumptionLabel.text = "-"
        }
        if movingTime == 0 {
            movingTime = 1
        }
        let currentSpeed = tripDistance / (Double(movingTime)/3600)
        speedLabel.text = String(format: "%.0f", currentSpeed) + "km/h"
    }
}
