//
//  VehicleHistoryViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 07/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import GoogleMaps
import UIKit

enum HistoryPeriod {
    case today
    case yesterday
    case currentWeek
    case customPeriod
}

class VehicleHistoryViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var bottomFuelConsumptionLabel: UILabel!
    @IBOutlet private weak var todayLabel: UILabel! {
        didSet {
            todayLabel.text = "today".localized
        }
    }
    @IBOutlet private weak var bottomView: UIView!
    @IBOutlet private weak var bottomDateLabel: UILabel!
    @IBOutlet private weak var bottomBatteryLabel: UILabel!
    @IBOutlet private weak var bottomSpeedLabel: UILabel!
    @IBOutlet private weak var bottomMileageLabel: UILabel!
    @IBOutlet private weak var mapView: GMSMapView!
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var stackView: UIStackView!
    @IBOutlet private weak var customPeriodView: UIView!
    
    // Stop bottom view outlets
    @IBOutlet private weak var parkingView: UIView!
    @IBOutlet private weak var parkingTimeLabel: UILabel!
    @IBOutlet private weak var parkedFrom: UILabel!
    @IBOutlet private weak var parkedTo: UILabel!
    
    @IBOutlet private weak var startPeriodTxt: UITextField! {
        didSet {
            dateFormatter.locale = Locale.current
            dateFormatter.dateFormat = StateController.getDateFormat()
            startPeriodTxt.text = dateFormatter.string(from: Date())
        }
    }
    @IBOutlet private weak var endPeriodTxt: UITextField! {
        didSet {
            dateFormatter.locale = Locale.current
            dateFormatter.dateFormat = StateController.getDateFormat()
            endPeriodTxt.text = dateFormatter.string(from: Date())
        }
    }
    @IBOutlet private weak var distanceLabel: UILabel!
    @IBOutlet private weak var drivingTimeLabel: UILabel!
    @IBOutlet private weak var parkedTimeLabel: UILabel!
    @IBOutlet private weak var neutralTimeLabel: UILabel!
    @IBOutlet private weak var oilConsumptionLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var coordinatesLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var vehicleHistoryTitleLabel: UILabel! {
        didSet {
            vehicleHistoryTitleLabel.text = "vehicle_history".localized
        }
    }
    @IBOutlet private weak var timeIntervalLabel: UILabel! {
        didSet {
            timeIntervalLabel.text = "time_interval".localized
        }
    }
    @IBOutlet private weak var distanceTranslatedLabel: UILabel! {
        didSet {
            distanceTranslatedLabel.text = "distance".localized
        }
    }
    @IBOutlet private weak var drivingTimeTranslatedLabel: UILabel! {
        didSet {
            drivingTimeTranslatedLabel.text = "driving_time".localized
        }
    }
    @IBOutlet private weak var neutralTimeTranslatedLabel: UILabel! {
        didSet {
            neutralTimeTranslatedLabel.text = "stationed".localized
        }
    }
    @IBOutlet private weak var stationedTimeTranslatedLabel: UILabel! {
        didSet {
            stationedTimeTranslatedLabel.text = "stopped".localized
        }
    }
    @IBOutlet private weak var fuelConsumptionTranslatedLabel: UILabel! {
        didSet {
            fuelConsumptionTranslatedLabel.text = "consumption".localized
        }
    }
    @IBOutlet private weak var startDateLabel: UILabel! {
        didSet {
            startDateLabel.text = "start_date".localized
        }
    }
    @IBOutlet private weak var endDateLabel: UILabel! {
        didSet {
            endDateLabel.text = "end_date".localized
        }
    }
    @IBOutlet private weak var mapTypeButton: UIButton!
    @IBOutlet private var applyButton: UIButton! {
        didSet {
            applyButton.setTitle("apply_button".localized, for: .normal)
        }
    }
    
    private let dateFormatter = DateFormatter()
    private let datePickerStartDate = UIDatePicker()
    private let datePickerEndDate = UIDatePicker()
    private var directionMarker: GMSMarker!
    private var polyline: GMSPolyline!
    private var lastTappedIsStop = false
    
    private var chartData: [Position] = []
    var vehicle: Vehicle!
    var deviceHistory: [DeviceHistory] = []
    private var entireDeviceHistory: [DeviceHistory] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
        addSwipeDownGesture()
        addSwipeUpGesture()
        setupUI()
        getDeviceHistory(for: .today)
    }
    
    private func setupUI() {
        tableView.register(UINib(nibName: "VehicleHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "VehicleHistoryTableViewCell")
        tableView.register(UINib(nibName: "VehicleHistoryStopTableViewCell", bundle: nil), forCellReuseIdentifier: "VehicleHistoryStopTableViewCell")
        mapView.mapType = .normal
        mapView.delegate = self
        mapView.isMyLocationEnabled = false
        if let latitude = vehicle.usedValues?.latitude,
            let longitude = vehicle.usedValues?.longitude {
            let location = GMSCameraPosition.camera(withLatitude: Double(latitude)!,
                                                    longitude: Double(longitude)!,
                                                    zoom: 10)
            mapView.animate(to: location)
        }
        
        datePickerStartDate.datePickerMode = .dateAndTime
        datePickerStartDate.maximumDate = Date()
        datePickerStartDate.locale = Locale.current
        datePickerStartDate.addTarget(self, action: #selector(handlePickerStartDate), for: .valueChanged)
        
        datePickerEndDate.datePickerMode = .dateAndTime
        datePickerEndDate.maximumDate = Date()
        datePickerEndDate.locale = Locale.current
        datePickerEndDate.addTarget(self, action: #selector(handlePickerEndDate), for: .valueChanged)
        
        let toolbarPickerDate = UIToolbar().ToolbarPicker(mySelect: #selector(dismissPickerCustomPeriodDate))
        startPeriodTxt.inputView = datePickerStartDate
        startPeriodTxt.inputAccessoryView = toolbarPickerDate
        
        endPeriodTxt.inputView = datePickerEndDate
        endPeriodTxt.inputAccessoryView = toolbarPickerDate
    }
    
    private func getDeviceHistory(for period: HistoryPeriod) {
        let vehicleId = vehicle.id
        let hostServer = vehicle.hostServer
        showActivityIndicator()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let calendar = Calendar.current
        var startOfDay = calendar.startOfDay(for: Date())
        var components = DateComponents()
        components.day = 1
        components.second = -1
        var dateAtEnd = calendar.date(byAdding: components, to: startOfDay)!
        switch period {
        case .yesterday:
            components.day = -1
            components.second = 0
            startOfDay = calendar.startOfDay(for: calendar.date(byAdding: components, to: startOfDay)!)
            components.day = 1
            components.second = -1
            dateAtEnd = Calendar.current.date(byAdding: components, to: startOfDay)!
        case .currentWeek:
            startOfDay = startOfDay.startOfWeek!
            dateAtEnd = startOfDay.endOfWeek!
        case .customPeriod:
            startOfDay = datePickerStartDate.date
            dateAtEnd = datePickerEndDate.date
        default:
            break
        }
        let startDate = dateFormatter.string(from: startOfDay)
        let endDate = dateFormatter.string(from: dateAtEnd)
        APIClient().perform(DeviceService.createDeviceHistoryRequest(deviceId: vehicle.id, startDate: startDate, endDate: endDate, dsServer: vehicle.hostServer)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success(let response):
                if let deviceHistory = try? response.decode(to: [DeviceHistory].self) {
                    strongSelf.entireDeviceHistory = deviceHistory.body
                    var historyItems: [DeviceHistory] = []
                    var historyItemsBuffer: [DeviceHistory] = []
                    for i in 0..<deviceHistory.body.count {
                        let currentHistoryItem = deviceHistory.body[i]
                        if currentHistoryItem.getType() == "stoptrip" {
                            strongSelf.dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let periodBeginDate = dateFormatter.date(from: currentHistoryItem.period.begin) ?? Date()
                            let stopEndDate = Calendar.current.date(byAdding: .second, value: currentHistoryItem.duration.stopped + currentHistoryItem.duration.idling, to: periodBeginDate) ?? Date()
                            let stopEndDateString = dateFormatter.string(from: stopEndDate)
                            let stop = DeviceHistory(duration: VehicleMovingDuration(moving: 0, stopped: currentHistoryItem.duration.stopped, idling: currentHistoryItem.duration.idling), distance: 0, address: HistoryAddress(begin: currentHistoryItem.address.begin, end: currentHistoryItem.address.begin), period: Period(begin: currentHistoryItem.period.begin, end: stopEndDateString), positions: [currentHistoryItem.positions[0]])
                            let positionsWithoutStop = Array(currentHistoryItem.positions.dropFirst())
                            let periodEndDate = dateFormatter.date(from: currentHistoryItem.period.end) ?? Date()
                            let periodEndDateString = dateFormatter.string(from: periodEndDate)
                            let trip = DeviceHistory(duration: VehicleMovingDuration(moving: currentHistoryItem.duration.moving, stopped: 0, idling: 0), distance: currentHistoryItem.distance, address: HistoryAddress(begin: currentHistoryItem.address.begin, end: currentHistoryItem.address.end), period: Period(begin: stopEndDateString, end: periodEndDateString), positions: positionsWithoutStop)
                            historyItemsBuffer.append(stop)
                            historyItemsBuffer.append(trip)
                        } else {
                            historyItemsBuffer.append(currentHistoryItem)
                        }
                    }
                    historyItemsBuffer.forEach { item in
                        let currentHistoryItem = item
                        guard let nextHistoryItem = historyItemsBuffer.after(item) else {
                            guard let previousHistoryItem = historyItems.last else {
                                assertionFailure("Not possible")
                                return
                            }
                            if previousHistoryItem.getType() != currentHistoryItem.getType() {
                                historyItems.append(currentHistoryItem)
                            }
                            return
                        }
                        if (currentHistoryItem.getType() == "stop" && nextHistoryItem.getType() == "stop") {
                            historyItems.append(DeviceHistory(duration: VehicleMovingDuration(moving: 0, stopped: currentHistoryItem.duration.stopped + nextHistoryItem.duration.stopped, idling: currentHistoryItem.duration.idling + nextHistoryItem.duration.idling), distance: currentHistoryItem.distance + nextHistoryItem.distance, address: HistoryAddress(begin: currentHistoryItem.address.begin, end: nextHistoryItem.address.end), period: Period(begin: currentHistoryItem.period.begin, end: nextHistoryItem.period.end), positions: nextHistoryItem.positions + currentHistoryItem.positions))
                        } else if (currentHistoryItem.getType() == "trip" && nextHistoryItem.getType() == "trip")  {
                            historyItems.append(DeviceHistory(duration: VehicleMovingDuration(moving: currentHistoryItem.duration.moving + nextHistoryItem.duration.moving, stopped: 0, idling: 0), distance: currentHistoryItem.distance + nextHistoryItem.distance, address: HistoryAddress(begin: currentHistoryItem.address.begin, end: nextHistoryItem.address.end), period: Period(begin: currentHistoryItem.period.begin, end: nextHistoryItem.period.end), positions: nextHistoryItem.positions + currentHistoryItem.positions))
                        } else {
                            guard let previousHistoryItem = historyItems.last else {
                                historyItems.append(currentHistoryItem)
                                return
                            }
                            if previousHistoryItem.getType() != currentHistoryItem.getType() {
                                historyItems.append(currentHistoryItem)
                            }
                        }
                    }
                    var historyItemsComplete: [DeviceHistory] = []
                    for i in 0..<historyItems.count {
                        let currentHistoryItem = historyItems[i]
                        if currentHistoryItem.getType() == "stop" && ((currentHistoryItem.duration.stopped + currentHistoryItem.duration.idling) <= StateController.getStopDuration()*60) {
                            if i == 0 {
                                historyItemsComplete.append(currentHistoryItem)
                            } else {
                                guard let previousTrip = historyItems.last else {
                                    continue
                                }
                                historyItemsComplete.append(DeviceHistory(duration: VehicleMovingDuration(moving: currentHistoryItem.duration.moving + previousTrip.duration.moving, stopped: currentHistoryItem.duration.stopped + previousTrip.duration.stopped, idling: currentHistoryItem.duration.idling + previousTrip.duration.idling), distance: previousTrip.distance, address: previousTrip.address, period: previousTrip.period, positions: previousTrip.positions + currentHistoryItem.positions))
                                historyItemsComplete.remove(at: historyItemsComplete.firstIndex(of: previousTrip) ?? 0)
                            }
                        } else if currentHistoryItem.getType() == "trip" {
                            guard let previousTrip = historyItemsComplete.last else {
                                continue
                            }
                            if previousTrip.getType() != "stop" {
                                historyItemsComplete.append(DeviceHistory(duration: VehicleMovingDuration(moving: currentHistoryItem.duration.moving + previousTrip.duration.moving, stopped: currentHistoryItem.duration.stopped + previousTrip.duration.stopped, idling: currentHistoryItem.duration.idling + previousTrip.duration.idling), distance: currentHistoryItem.distance + previousTrip.distance, address: HistoryAddress(begin: previousTrip.address.begin, end: currentHistoryItem.address.end), period: Period(begin: previousTrip.period.begin, end: currentHistoryItem.period.end), positions: previousTrip.positions + currentHistoryItem.positions))
                                historyItemsComplete.remove(at: historyItemsComplete.firstIndex(of: previousTrip) ?? 0)
                            } else {
                                historyItemsComplete.append(currentHistoryItem)
                            }
                        } else {
                            guard let previousTrip = historyItemsComplete.last else {
                                historyItemsComplete.append(currentHistoryItem)
                                continue
                            }
                            if currentHistoryItem.getType() == "stop" && previousTrip.getType() == "stop" {
                                historyItemsComplete.append(DeviceHistory(duration: VehicleMovingDuration(moving: currentHistoryItem.duration.moving + previousTrip.duration.moving, stopped: currentHistoryItem.duration.stopped + previousTrip.duration.stopped, idling: currentHistoryItem.duration.idling + previousTrip.duration.idling), distance: previousTrip.distance, address: previousTrip.address, period: previousTrip.period, positions: previousTrip.positions + currentHistoryItem.positions))
                                historyItemsComplete.remove(at: historyItemsComplete.firstIndex(of: previousTrip) ?? 0)
                            } else {
                                historyItemsComplete.append(currentHistoryItem)
                            }
                        }
                    }
                    var stop = 0
                    let finalDeviceHistory = historyItemsComplete.map({ historyItem -> DeviceHistory in
                        if historyItem.getType() == "stop" {
                            stop += 1
                            var modified = historyItem
                            modified.stopNumber = stop
                            return modified
                        } else {
                            return historyItem
                        }
                    })
                    strongSelf.deviceHistory = finalDeviceHistory
                    strongSelf.getDeviceFuelLevel(vehicleId: vehicleId, hostServer: hostServer, startDate: startDate, endDate: endDate)
                } else {
                    DispatchQueue.main.async {
                        strongSelf.presentAlertController(title: "error_title".localized, message: "error_message".localized)
                        strongSelf.tableView.reloadData()
                        strongSelf.hideActivityIndicator()
                    }
                    print("error")
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    strongSelf.presentAlertController(title: "error_title".localized, message: "error_message".localized)
                    strongSelf.tableView.reloadData()
                    strongSelf.hideActivityIndicator()
                }
                print(error.localizedDescription)
            }
        }
    }
    
    private func getDeviceFuelLevel(vehicleId: Int, hostServer: String, startDate: String, endDate: String) {
        APIClient().perform(DeviceService.getRefuelingsPath(startDate: startDate, endDate: endDate, deviceId: vehicleId, dsServer: hostServer)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success(let response):
                if let dataReport = try? response.decode(to: DataReport.self) {
                    strongSelf.chartData = dataReport.body.chartData
                    DispatchQueue.main.async {
                        strongSelf.updateUIWithDevice(strongSelf.deviceHistory)
                    }
                } else {
                    DispatchQueue.main.async {
                        strongSelf.hideActivityIndicator()
                        strongSelf.presentAlertController(title: "error_title".localized, message: "error_message".localized)
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    strongSelf.hideActivityIndicator()
                    strongSelf.presentAlertController(title: "error_title".localized, message: "error_message".localized)
                }
                print(error.localizedDescription)
            }
        }
    }
    
    private func updateUIWithDevice(_ deviceHistory: [DeviceHistory]) {
        var distance = 0.0
        var drivingTime = 0
        var stationedTime = 0
        var idleTime = 0
                
        entireDeviceHistory.forEach {
            distance += $0.distance
            drivingTime += $0.duration.moving
            stationedTime += $0.duration.stopped
            idleTime += $0.duration.idling
        }
        distanceLabel.text = String(format: "%.2f", distance) + "km"
        let (hoursDriving, minutesDriving) = drivingTime.secondsToHoursMinutesSeconds()
        let (hoursStationed, minutesStationed) = stationedTime.secondsToHoursMinutesSeconds()
        let (hoursIdling, minutesIdling) = idleTime.secondsToHoursMinutesSeconds()
        drivingTimeLabel.text = "\(Int(hoursDriving))h \(minutesDriving)m"
        parkedTimeLabel.text = "\(Int(hoursStationed))h \(minutesStationed)m"
        neutralTimeLabel.text = "\(Int(hoursIdling))h \(minutesIdling)m"
        var result = 0.0
        if let lastItem = entireDeviceHistory.last,
            let firstItem = entireDeviceHistory.first,
            let lastConsumptionValue = lastItem.positions.last?["fcs"]?.value as? Double,
            let firstConsumptionValue = firstItem.positions.first?["fcs"]?.value as? Double {
            result = (lastConsumptionValue - firstConsumptionValue) * 100 / distance
        }
        oilConsumptionLabel.text = result > 0 ? "\(result.truncate(places: 2))L" : "-"
        tableView.reloadData()
        drawRoute(with: nil, stopPosition: 0)
        hideActivityIndicator()
    }
    
    private func drawRoute(with trip: DeviceHistory?, stopPosition: Int) {
        if deviceHistory.count > 0 {
            let path = GMSMutablePath()
            mapView.clear()
            if let trip = trip { // one trip
                trip.positions.forEach {
                    if let latitude = $0["lat"]?.value as? Double,
                        let longitude = $0["lng"]?.value as? Double {
                        path.addLatitude(latitude, longitude: longitude)
                    }
                }
                if let startPosition = trip.positions.first,
                    let latitude = startPosition["lat"]?.value as? Double,
                    let longitude = startPosition["lng"]?.value as? Double {
                    let startPoint = GMSMarker()
                    startPoint.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    startPoint.groundAnchor = CGPoint(x: 0.5, y: 0.8)
                    let image = UIImage.textToImage(drawText: stopPosition < 10 ? "0\(stopPosition)" : "\(stopPosition)", inImage: UIImage(named:"stopMarkerLabel")!, atPoint: CGPoint(x: 10, y: 10))
                    startPoint.icon = image
                    startPoint.map = mapView
                }
                if let endPosition = trip.positions.last,
                    let latitude = endPosition["lat"]?.value as? Double,
                    let longitude = endPosition["lng"]?.value as? Double {
                    let endPoint = GMSMarker()
                    endPoint.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    endPoint.groundAnchor = CGPoint(x: 0.5, y: 0.8)
                    let image = UIImage.textToImage(drawText: stopPosition < 10 ? "0\(stopPosition+1)" : "\(stopPosition+1)", inImage: UIImage(named:"stopMarkerLabel")!, atPoint: CGPoint(x: 10, y: 10))
                    endPoint.icon = image
                    endPoint.userData = "stopMarker\(stopPosition < 10 ? "0\(stopPosition)" : "\(stopPosition)")"
                    endPoint.map = mapView
                }
            } else { // entire route
                deviceHistory.forEach { history in
                    history.positions.forEach { position in
                        if let latitude = position["lat"]?.value as? Double,
                            let longitude = position["lng"]?.value as? Double {
                            path.addLatitude(latitude, longitude: longitude)
                        }
                    }
                }
                if let startPosition = deviceHistory.first,
                    let latitude = startPosition.positions.first?["lat"]?.value as? Double,
                    let longitude = startPosition.positions.first?["lng"]?.value as? Double {
                    let startPoint = GMSMarker()
                    startPoint.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    startPoint.groundAnchor = CGPoint(x: 0.5, y: 0.8)
                    startPoint.icon = UIImage(named: "startPointMarker")
                    startPoint.map = mapView
                }
                if let endPosition = deviceHistory.last,
                    let latitude = endPosition.positions.last?["lat"]?.value as? Double,
                    let longitude = endPosition.positions.last?["lng"]?.value as? Double {
                    let endPoint = GMSMarker()
                    endPoint.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    endPoint.groundAnchor = CGPoint(x: 0.5, y: 0.8)
                    endPoint.icon = UIImage(named: "endPointMarker")
                    endPoint.map = mapView
                }
                var count = 1
                deviceHistory.filter {
                    $0.getType() == "stop"
                }.forEach {
                    if let firstPosition = deviceHistory.first,
                        let lastPosition = deviceHistory.last {
                        if $0 != firstPosition && $0 != lastPosition {
                            if let latitude = $0.positions.first?["lat"]?.value as? Double,
                                let longitude = $0.positions.first?["lng"]?.value as? Double {
                                let startPoint = GMSMarker()
                                startPoint.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                                startPoint.groundAnchor = CGPoint(x: 0.5, y: 0.8)
                                let image = UIImage.textToImage(drawText: count < 10 ? "0\(count)" : "\(count)", inImage: UIImage(named:"stopMarkerLabel")!, atPoint: CGPoint(x: 10, y: 10))
                                startPoint.icon = image
                                startPoint.userData = "stopMarker\(count < 10 ? "0\(count)" : "\(count)")"
                                startPoint.map = mapView
                                count += 1
                            }
                        }
                    }
                }
            }
            
            let polyline2 = GMSPolyline(path: path)
            polyline2.strokeColor = .white
            polyline2.strokeWidth = 5.0
            polyline2.geodesic = false
            polyline2.isTappable = false
            polyline2.map = mapView
            self.polyline = polyline2
            
            let polyline = GMSPolyline(path: path)
            polyline.strokeColor = UIColor(red: 0/255, green: 100/255, blue: 255/255, alpha: 1)
            polyline.strokeWidth = 2.0
            polyline.geodesic = false
            polyline.isTappable = false
            polyline.map = mapView
            
            var bounds = GMSCoordinateBounds()
            
            for index in 1...path.count() {
                bounds = bounds.includingCoordinate(path.coordinate(at: index))
            }
        
            mapView.animate(with: GMSCameraUpdate.fit(bounds))
        }
    }
    
    private func addSwipeDownGesture() {
        let slideDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissPanelView(gesture:)))
        slideDown.direction = .down
        bottomView.addGestureRecognizer(slideDown)
    }
    
    private func addSwipeUpGesture() {
        let slideUp = UISwipeGestureRecognizer(target: self, action: #selector(showPanelView(gesture:)))
        slideUp.direction = .up
        bottomView.addGestureRecognizer(slideUp)
    }
    
    @objc private func handlePickerStartDate() {
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = StateController.getDateFormat()
        startPeriodTxt.text = dateFormatter.string(from: datePickerStartDate.date)
    }
    
    @objc private func handlePickerEndDate() {
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = StateController.getDateFormat()
        endPeriodTxt.text = dateFormatter.string(from: datePickerEndDate.date)
    }
    
    @objc private func dismissPickerCustomPeriodDate() {
        view.endEditing(true)
    }
    
    @objc func dismissPanelView(gesture: UISwipeGestureRecognizer) {
        bottomViewHeightConstraint.constant = 35
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
            self.bottomDateLabel.isHidden = false
            self.stackView.isHidden = true
            self.parkingView.isHidden = true
        })
    }
    
    @objc func showPanelView(gesture: UISwipeGestureRecognizer) {
        bottomViewHeightConstraint.constant = 176
        UIView.animate(withDuration: 0.25, animations: {
            self.stackView.isHidden = self.lastTappedIsStop
            self.parkingView.isHidden = !self.lastTappedIsStop
            self.bottomDateLabel.isHidden = self.lastTappedIsStop
        })
    }
    
    @IBAction func didTapCopyAddress(_ sender: Any) {
        UIPasteboard.general.string = addressLabel.text! + " " + coordinatesLabel.text!
        ToastUtil.toastMessageInTheMiddle(message: "address_copied".localized)
    }
    
    @IBAction func didTapOutsideCustomPeriodView(_ sender: Any) {
        UIView.animate(withDuration: 0.25, animations: {
            self.customPeriodView.alpha = 0
        })
    }
    
    @IBAction func didTapTimeInterval(_ sender: Any) {
        let todayAction = UIAlertAction(title: "today".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.todayLabel.text = "today".localized
            strongSelf.getDeviceHistory(for: .today)
            strongSelf.tableView.reloadData()
        }
        let yesterdayAction = UIAlertAction(title: "yesterday".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.todayLabel.text = "yesterday".localized
            strongSelf.getDeviceHistory(for: .yesterday)
            strongSelf.tableView.reloadData()
        }
        let currentWeekAction = UIAlertAction(title: "current_week".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.todayLabel.text = "current_week".localized
            strongSelf.getDeviceHistory(for: .currentWeek)
            strongSelf.tableView.reloadData()
        }
        let customPeriodAction = UIAlertAction(title: "custom_period".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.showCustomPeriodView()
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        presentAlertController(title: nil, message: nil, actions: [todayAction, yesterdayAction, currentWeekAction, customPeriodAction, cancelAction], style: .actionSheet)
    }
    
    @IBAction func didTapApplyCustomPeriod(_ sender: Any) {
        let calendar = Calendar.current
        let date1 = calendar.startOfDay(for: datePickerStartDate.date)
        let date2 = calendar.startOfDay(for: datePickerEndDate.date)
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        if components.day ?? 0 < 90 {
            hideCustomPeriodView()
        } else {
            presentAlertController(title: "", message: "maximum_interval".localized)
        }
    }
    
    @IBAction func didTapMapType(_ sender: Any) {
        let satelliteAction = UIAlertAction(title: "satellite".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .satellite
        }
        let hybridAction = UIAlertAction(title: "hybrid".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .hybrid
        }
        let normalAction = UIAlertAction(title: "normal".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .normal
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        presentAlertController(title: nil, message: nil, actions: [satelliteAction, hybridAction, normalAction, cancelAction], style: .actionSheet)
    }
    
    private func showCustomPeriodView() {
        UIView.animate(withDuration: 0.25, animations: {
            self.customPeriodView.alpha = 1
        })
    }
    
    private func hideCustomPeriodView() {
        getDeviceHistory(for: .customPeriod)
        todayLabel.text = "\(startPeriodTxt.text!) - \(endPeriodTxt.text!)"
        UIView.animate(withDuration: 0.25, animations: {
            self.customPeriodView.alpha = 0
        })
    }
    
    private func showMap() {
        UIView.transition(with: mapView, duration: 0.55, options: [.transitionFlipFromLeft], animations: {
            self.mapView.isHidden = false
            self.tableView.isHidden = true
            self.mapTypeButton.isHidden = false
        })
    }
    
    private func hideMap() {
        UIView.transition(with: tableView, duration: 0.55, options: [.transitionFlipFromRight], animations: {
            self.bottomView.isHidden = true
            self.bottomViewHeightConstraint.constant = 0
            self.tableView.isHidden = false
            self.mapView.isHidden = true
            self.mapTypeButton.isHidden = true
        })
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapMap(_ sender: Any) {
        mapView.isHidden ? showMap() : hideMap()
    }
}

extension VehicleHistoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let currentDeviceHistory = deviceHistory[indexPath.row]
        if currentDeviceHistory.getType() == "stop" {
            return 36
        } else {
            return 120
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentDeviceHistory = deviceHistory[indexPath.row]
        if let firstDeviceHistory = deviceHistory.first {
            if currentDeviceHistory != firstDeviceHistory {
                let previousDeviceHistory = deviceHistory[indexPath.row-1]
                drawRoute(with: currentDeviceHistory, stopPosition: previousDeviceHistory.stopNumber ?? 0)
            } else {
                drawRoute(with: currentDeviceHistory, stopPosition: 0)
            }
        }
        
        showMap()
    }
}

extension VehicleHistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentDeviceHistory = deviceHistory[indexPath.row]
        switch currentDeviceHistory.getType() {
        case "trip":
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleHistoryTableViewCell", for: indexPath) as! VehicleHistoryTableViewCell
            cell.configure(trip: currentDeviceHistory)
            cell.isUserInteractionEnabled = true
            cell.selectionStyle = .none
            return cell
        case "stop":
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleHistoryStopTableViewCell", for: indexPath) as! VehicleHistoryStopTableViewCell
            cell.configure(stop: currentDeviceHistory)
            cell.isUserInteractionEnabled = false
            return cell
        default:
            break
        }
        assertionFailure("should not happen")
        let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleHistoryTableViewCell", for: indexPath) as! VehicleHistoryTableViewCell
        return cell
    }
}

extension VehicleHistoryViewController: GMSMapViewDelegate {
    func closestLocation(locations: [CLLocation], closestToLocation location: CLLocation) -> CLLocation? {
        if let closestLocation = locations.min(by: { location.distance(from: $0) < location.distance(from: $1) }) {
            print("closest location: \(closestLocation), distance: \(location.distance(from: closestLocation))")
            return closestLocation
        } else {
            print("coordinates is empty")
            return nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if self.directionMarker != nil {
            self.directionMarker.map = nil
        }
        
        guard let polylinePath = polyline.path else {
            return
        }
        
        if GMSGeometryIsLocationOnPathTolerance(coordinate, polylinePath, false, 10000) {
            var locations: [CLLocation] = []
            deviceHistory.forEach { historyItem in
                if historyItem.getType() == "trip" {
                    historyItem.positions.forEach { position in
                        if let latitude = position["lat"]?.value as? Double,
                            let longitude = position["lng"]?.value as? Double {
                            locations.append(CLLocation(latitude: latitude, longitude: longitude))
                        }
                    }
                }
            }
            let closestPosition = closestLocation(locations: locations, closestToLocation: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
            for historyItem in deviceHistory {
                if historyItem.getType() == "trip" {
                    for position in historyItem.positions {
                        if let latitude = position["lat"]?.value as? Double,
                            let longitude = position["lng"]?.value as? Double {
                            if latitude == closestPosition?.coordinate.latitude && longitude == closestPosition?.coordinate.longitude {
                                self.directionMarker = GMSMarker()
                                self.parkingView.isHidden = true
                                self.lastTappedIsStop = false
                                self.directionMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                                self.directionMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                                self.directionMarker.icon = UIImage(named: "directionMarkerHistory")
                                self.directionMarker.rotation = (position["dir"]?.value as! NSNumber).doubleValue
                                self.directionMarker.map = mapView
                                self.bottomView.isHidden = false
                                self.stackView.isHidden = false
                                self.bottomViewHeightConstraint.constant = 176
                                if let deviceTime = position["dtu"]?.value as? String {
                                    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                                    dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                                    let deviceDate = dateFormatter.date(from: deviceTime)!
                                    dateFormatter.timeZone = TimeZone.current
                                    dateFormatter.dateFormat = StateController.getDateFormat()
                                    let deviceDateRepresented = dateFormatter.string(from: deviceDate)
                                    self.bottomDateLabel.isHidden = false
                                    self.bottomDateLabel.text = deviceDateRepresented
                                }
                                self.addressLabel.text = "loading".localized
                                APIClient().perform(DeviceService.createReverseGeocodingSingleRequest(latitude: latitude, longitude: longitude, dsServer: vehicle.hostServer)) { result in
                                    switch result {
                                    case .success(let response):
                                        if let deviceAddress = try? response.decode(to: DeviceAddress.self) {
                                            DispatchQueue.main.async {
                                                self.addressLabel.text = deviceAddress.body.addressFormatted
                                            }
                                        } else {
                                            DispatchQueue.main.async {
                                                self.addressLabel.text = "no_location_found".localized
                                            }
                                        }
                                    case .failure(let error):
                                        print(error.localizedDescription)
                                        DispatchQueue.main.async {
                                            self.addressLabel.text = "no_location_found".localized
                                        }
                                    }
                                }
                                self.coordinatesLabel.text = "\(latitude), \(longitude)"
                                if let mileagePosition = position["mil"]?.value as? Double {
                                    self.bottomMileageLabel.text = String(format: "%.2f", mileagePosition + Double(vehicle.attributeCorrections?.attrMileageCorrectVal ?? 0)) + "km"
                                } else {
                                    self.bottomMileageLabel.text = "-"
                                }
                                if let batteryLevel = position["pwr"]?.value as? Int {
                                    self.bottomBatteryLabel.text = String(format: "%.1f", Double(batteryLevel)/1000.0).replacingOccurrences(of: ",", with: ".") + "v"
                                } else {
                                    self.bottomBatteryLabel.text = "-"
                                }
                                if let speed = position["spd"]?.value as? Int {
                                    self.bottomSpeedLabel.text = "\(speed)km/h"
                                } else {
                                    self.bottomSpeedLabel.text = "-"
                                }
                                let fuelLevelPosition = chartData.first(where: { $0.dt == position["dtu"]?.value as! String })
                                if let fuelLevel = fuelLevelPosition?.fuel {
                                    self.bottomFuelConsumptionLabel.text = fuelLevel > 0 ? "\(fuelLevel.truncate(places: 2))L" : "-"
                                } else {
                                    self.bottomFuelConsumptionLabel.text = "-"
                                }
                                break
                            }
                        }
                    }
                }
            }
        } else {
            self.bottomViewHeightConstraint.constant = 0
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let description = marker.userData as? String else {
            return false
        }
        if description.contains("stopMarker") {
            for historyItem in deviceHistory {
                if historyItem.getType() == "stop" && historyItem.stopNumber == Int(description.replacingOccurrences(of: "stopMarker", with: ""))! {
                    bottomDateLabel.isHidden = true
                    bottomView.isHidden = false
                    lastTappedIsStop = true
                    if self.directionMarker != nil {
                        self.directionMarker.map = nil
                    }
                    bottomViewHeightConstraint.constant = 164
                    parkingView.isHidden = false
                    stackView.isHidden = true
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let startTime = dateFormatter.date(from: historyItem.period.begin)
                    let endTime = dateFormatter.date(from: historyItem.period.end)
                    let totalTime = Double(endTime?.timeIntervalSince1970 ?? 0) - Double(startTime?.timeIntervalSince1970 ?? 0)
                    dateFormatter.dateFormat = "HH:mm"
                    let hours = Int(totalTime/3600)
                    let minutes = Int(Int(totalTime - Double(hours*3600))/60)
                    parkingTimeLabel.text = "\(hours < 10 && hours > 0 ? "0\(hours)" : "\(hours)")h \(minutes < 10 && minutes > 0 ? "0\(minutes)" : "\(minutes)")m"
                    parkedFrom.text = "from".localized + ":\n \(historyItem.period.begin)"
                    parkedTo.text = "to".localized + ":\n \(historyItem.period.end)"
                    addressLabel.text = "loading".localized
                    if let latitude = historyItem.positions.first?["lat"]?.value as? Double,
                        let longitude = historyItem.positions.first?["lng"]?.value as? Double {
                        APIClient().perform(DeviceService.createReverseGeocodingSingleRequest(latitude: latitude, longitude: longitude, dsServer: vehicle.hostServer)) { result in
                            switch result {
                            case .success(let response):
                                if let deviceAddress = try? response.decode(to: DeviceAddress.self) {
                                    DispatchQueue.main.async {
                                        self.addressLabel.text = deviceAddress.body.addressFormatted
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        self.addressLabel.text = "no_location_found".localized
                                    }
                                }
                            case .failure(let error):
                                print(error.localizedDescription)
                                DispatchQueue.main.async {
                                    self.addressLabel.text = "no_location_found".localized
                                }
                            }
                        }
                        self.coordinatesLabel.text = "\(latitude), \(longitude)"
                    }
                }
            }
        } else {
            bottomViewHeightConstraint.constant = 0
        }
        return true
    }
}
