//
//  AlimentareTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 25/04/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class AlimentareTableViewCell: UITableViewCell {
    @IBOutlet private var typeIcon: UIImageView!
    @IBOutlet private var countryIcon: UIImageView!
    @IBOutlet private var addressLabel: UILabel!
    @IBOutlet private var fuelLevelLabel: UILabel!
    @IBOutlet private var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configure(with alimentare: Alimentare) {
        typeIcon.image = alimentare.isPositive ? UIImage(named: "alimentareIcon") : UIImage(named: "pierdereIcon")
        let countryCode = alimentare.address.addressElements["country_code"] ?? ""
        countryIcon.image = UIImage(named: countryCode?.uppercased() ?? "")
        addressLabel.text = alimentare.address.addressFormatted
        fuelLevelLabel.text = String(format: "%.2f", alimentare.fuel) + "L"
        dateLabel.text = alimentare.date
    }
}
