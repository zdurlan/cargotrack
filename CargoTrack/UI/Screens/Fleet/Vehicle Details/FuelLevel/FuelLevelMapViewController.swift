//
//  FuelLevelMapViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 03/05/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import GoogleMaps
import UIKit

class FuelLevelMapViewController: BaseLightStatusBarViewController {
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.text = alimentare.isPositive ? "alimentare".localized : "pierdere".localized
        }
    }
    @IBOutlet private var bottomView: UIView!
    @IBOutlet private var bottomContainerView: UIView!
    @IBOutlet private var mapView: GMSMapView!
    @IBOutlet private var addressLabel: UILabel! {
        didSet {
            addressLabel.text = alimentare.address.addressFormatted
        }
    }
    @IBOutlet private var coordinatesLabel: UILabel! {
        didSet {
            coordinatesLabel.text = "\(alimentare.latitude)" + ", " + "\(alimentare.longitude)"
        }
    }
    @IBOutlet private var alimentareIcon: UIImageView! {
        didSet {
            alimentareIcon.image = alimentare.isPositive ? UIImage(named: "alimentareIcon") : UIImage(named: "pierdereIcon")
        }
    }
    @IBOutlet private var quantityLabel: UILabel! {
        didSet {
            quantityLabel.text = "quantity".localized + ": \n" + String(format: "%.2f", alimentare.fuel) + " L"
        }
    }
    @IBOutlet private var dateAndTimeLabel: UILabel! {
        didSet {
            dateAndTimeLabel.text = "date_and_time".localized + ": \n" + alimentare.date
        }
    }
    
    var alimentare: Alimentare!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
        addSwipeDownGesture()
        addSwipeUpGesture()
        setupMap()
    }
    
    private func setupMap() {
        mapView.mapType = .normal
        mapView.delegate = self
        mapView.isMyLocationEnabled = false
        let location = GMSCameraPosition.camera(withLatitude: alimentare.latitude,
                                                longitude: alimentare.longitude,
                                                zoom: 10)
        mapView.animate(to: location)
        

        let startPoint = GMSMarker()
        startPoint.position = CLLocationCoordinate2D(latitude: alimentare.latitude, longitude: alimentare.longitude)
        startPoint.groundAnchor = CGPoint(x: 0.5, y: 0.8)
        startPoint.icon = alimentare.isPositive ? UIImage(named:"alimentareIcon") : UIImage(named:"pierdereIcon")
        startPoint.map = mapView
    }
    
    private func addSwipeDownGesture() {
        let slideDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissPanelView(gesture:)))
        slideDown.direction = .down
        bottomContainerView.addGestureRecognizer(slideDown)
    }
    
    private func addSwipeUpGesture() {
        let slideUp = UISwipeGestureRecognizer(target: self, action: #selector(showPanelView(gesture:)))
        slideUp.direction = .up
        bottomContainerView.addGestureRecognizer(slideUp)
    }
    
    @objc func dismissPanelView(gesture: UISwipeGestureRecognizer) {
        bottomViewHeightConstraint.constant = 35
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
            self.bottomView.isHidden = false
        })
    }
    
    @objc func showPanelView(gesture: UISwipeGestureRecognizer) {
        bottomViewHeightConstraint.constant = 160
        UIView.animate(withDuration: 0.25, animations: {
            self.bottomView.isHidden = false
        })
    }
    
    @IBAction func didTapCopyAddress(_ sender: Any) {
        UIPasteboard.general.string = addressLabel.text! + " " + coordinatesLabel.text!
        ToastUtil.toastMessageInTheMiddle(message: "address_copied".localized)
    }
    
    @IBAction func didTapMaps(_ sender: Any) {
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!) {
            UIApplication.shared.open(URL(string: "comgooglemaps://?saddr=&daddr=\(alimentare.latitude),\(alimentare.longitude)&directionsmode=driving")!)
        } else {
            UIApplication.shared.open(URL(string: "https://maps.google.com/?q=@\(alimentare.latitude),\(alimentare.longitude)")!)
        }
    }
    
    @IBAction func didTapMapType(_ sender: Any) {
        let satelliteAction = UIAlertAction(title: "satellite".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .satellite
        }
        let hybridAction = UIAlertAction(title: "hybrid".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .hybrid
        }
        let normalAction = UIAlertAction(title: "normal".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .normal
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        presentAlertController(title: nil, message: nil, actions: [satelliteAction, hybridAction, normalAction, cancelAction], style: .actionSheet)
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension FuelLevelMapViewController: GMSMapViewDelegate {
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapView.isMyLocationEnabled = !mapView.isMyLocationEnabled
        return true
    }
}
