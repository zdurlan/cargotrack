//
//  FuelLevelViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 23/04/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class FuelLevelViewController: BaseLightStatusBarViewController {
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.text = vehicle.label?.isEmpty ?? true ? vehicle.name : vehicle.label
        }
    }
    @IBOutlet private var todayLabel: UILabel! {
        didSet {
            todayLabel.text = "today".localized
        }
    }
    @IBOutlet private var timeIntervalLabel: UILabel! {
        didSet {
            timeIntervalLabel.text = "time_interval".localized
        }
    }
    @IBOutlet private var customPeriodView: UIView!
    @IBOutlet private var startDateLabel: UILabel! {
        didSet {
            startDateLabel.text = "start_date".localized
        }
    }
    @IBOutlet private var startPeriodTxt: UITextField! {
        didSet {
            dateFormatter.locale = Locale.current
            dateFormatter.dateFormat = StateController.getDateFormat()
            startPeriodTxt.text = dateFormatter.string(from: Date())
        }
    }
    @IBOutlet private var endDateLabel: UILabel! {
        didSet {
            endDateLabel.text = "end_date".localized
        }
    }
    @IBOutlet private var endPeriodTxt: UITextField! {
        didSet {
            dateFormatter.locale = Locale.current
            dateFormatter.dateFormat = StateController.getDateFormat()
            endPeriodTxt.text = dateFormatter.string(from: Date())
        }
    }
    @IBOutlet private var applyButton: UIButton! {
        didSet {
            applyButton.setTitle("apply_button".localized, for: .normal)
        }
    }
    @IBOutlet private var kmLabel: UILabel!
    @IBOutlet private var consumptionLabel: UILabel!
    @IBOutlet private var alimentareLabel: UILabel!
    @IBOutlet private var pierdereLabel: UILabel!
    @IBOutlet private var tableView: UITableView!
    
    var vehicle: Vehicle!
    private let dateFormatter = DateFormatter()
    private let datePickerStartDate = UIDatePicker()
    private let datePickerEndDate = UIDatePicker()
    
    private var alimentari: [Alimentare] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
        setupUI()
        getDeviceFuelLevel(for: .today)
    }
    
    private func setupUI() {
        tableView.register(UINib(nibName: "AlimentareTableViewCell", bundle: nil), forCellReuseIdentifier: "AlimentareTableViewCell")
        
        datePickerStartDate.datePickerMode = .dateAndTime
        datePickerStartDate.maximumDate = Date()
        datePickerStartDate.locale = Locale.current
        datePickerStartDate.addTarget(self, action: #selector(handlePickerStartDate), for: .valueChanged)
        
        datePickerEndDate.datePickerMode = .dateAndTime
        datePickerEndDate.maximumDate = Date()
        datePickerEndDate.locale = Locale.current
        datePickerEndDate.addTarget(self, action: #selector(handlePickerEndDate), for: .valueChanged)
        
        let toolbarPickerDate = UIToolbar().ToolbarPicker(mySelect: #selector(dismissPickerCustomPeriodDate))
        startPeriodTxt.inputView = datePickerStartDate
        startPeriodTxt.inputAccessoryView = toolbarPickerDate
        
        endPeriodTxt.inputView = datePickerEndDate
        endPeriodTxt.inputAccessoryView = toolbarPickerDate
    }
    
    private func getDeviceFuelLevel(for period: HistoryPeriod) {
        showActivityIndicator()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let calendar = Calendar.current
        var startOfDay = calendar.startOfDay(for: Date())
        var components = DateComponents()
        components.day = 1
        components.second = -1
        var dateAtEnd = calendar.date(byAdding: components, to: startOfDay)!
        switch period {
        case .yesterday:
            components.day = -1
            components.second = 0
            startOfDay = calendar.startOfDay(for: calendar.date(byAdding: components, to: startOfDay)!)
            components.day = 1
            components.second = -1
            dateAtEnd = Calendar.current.date(byAdding: components, to: startOfDay)!
        case .currentWeek:
            startOfDay = startOfDay.startOfWeek!
            dateAtEnd = startOfDay.endOfWeek!
        case .customPeriod:
            startOfDay = datePickerStartDate.date
            dateAtEnd = datePickerEndDate.date
        default:
            break
        }
        let startDate = dateFormatter.string(from: startOfDay)
        let endDate = dateFormatter.string(from: dateAtEnd)
        let refuelTreshold = vehicle.refuelingTreshold == 0 ? 10 : vehicle.refuelingTreshold
        let leakTreshold = vehicle.leakTreshold == 0 ? 10 : vehicle.leakTreshold
        APIClient().perform(DeviceService.getRefuelingsPath(startDate: startDate, endDate: endDate, deviceId: vehicle.id, dsServer: vehicle.hostServer)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success(let response):
                if let dataReport = try? response.decode(to: DataReport.self) {
                    strongSelf.alimentari = dataReport.body.refuelings.filter( {
                        if $0.isPositive {
                            return $0.fuel >= Double(refuelTreshold ?? 10)
                        } else {
                            return abs($0.fuel) >= Double(leakTreshold ?? 10)
                        }
                    })
                    DispatchQueue.main.async {
                        strongSelf.tableView.reloadData()
                        let mileage = dataReport.body.distance
                        strongSelf.kmLabel.text = String(format: "%.2f", mileage) + " km"
                        var consumption: Double = 0
                        dataReport.body.consumptions.forEach {
                            consumption += $0.con
                        }
                        if mileage > 0 {
                            let actualConsumption = (consumption / mileage) * 100
                            strongSelf.consumptionLabel.text = String(format: "%.2f", actualConsumption) + "%"
                        } else {
                            strongSelf.consumptionLabel.text = "0.0%"
                        }
                        var alimentare: Double = 0
                        var pierdere: Double = 0
                        strongSelf.alimentari.forEach {
                            if $0.isPositive {
                                alimentare += $0.fuel
                            } else {
                                pierdere += $0.fuel
                            }
                        }
                        strongSelf.alimentareLabel.text = String(format: "%.2f", alimentare) + " L"
                        strongSelf.pierdereLabel.text = String(format: "%.2f", pierdere) + " L"
                    }
                } else {
                    DispatchQueue.main.async {
                        strongSelf.presentAlertController(title: "error_title".localized, message: "error_message".localized)
                        strongSelf.tableView.reloadData()
                        strongSelf.hideActivityIndicator()
                    }
                    print("error")
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    strongSelf.presentAlertController(title: "error_title".localized, message: "error_message".localized)
                    strongSelf.tableView.reloadData()
                    strongSelf.hideActivityIndicator()
                }
                print(error.localizedDescription)
            }
        }
    }
    
    @IBAction func didTapTimeInterval(_ sender: Any) {
        let todayAction = UIAlertAction(title: "today".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.todayLabel.text = "today".localized
            strongSelf.getDeviceFuelLevel(for: .today)
            strongSelf.tableView.reloadData()
        }
        let yesterdayAction = UIAlertAction(title: "yesterday".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.todayLabel.text = "yesterday".localized
            strongSelf.getDeviceFuelLevel(for: .yesterday)
            strongSelf.tableView.reloadData()
        }
        let currentWeekAction = UIAlertAction(title: "current_week".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.todayLabel.text = "current_week".localized
            strongSelf.getDeviceFuelLevel(for: .currentWeek)
            strongSelf.tableView.reloadData()
        }
        let customPeriodAction = UIAlertAction(title: "custom_period".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.showCustomPeriodView()
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        presentAlertController(title: nil, message: nil, actions: [todayAction, yesterdayAction, currentWeekAction, customPeriodAction, cancelAction], style: .actionSheet)
    }
    
    @IBAction func didTapOutsideCustomPeriod(_ sender: Any) {
        UIView.animate(withDuration: 0.25, animations: {
            self.customPeriodView.alpha = 0
        })
    }
    
    @IBAction func didTapApplyCustomPeriod(_ sender: Any) {
        let calendar = Calendar.current
        let date1 = calendar.startOfDay(for: datePickerStartDate.date)
        let date2 = calendar.startOfDay(for: datePickerEndDate.date)
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        if components.day ?? 0 < 90 {
            hideCustomPeriodView()
        } else {
            presentAlertController(title: "", message: "maximum_interval".localized)
        }
    }
    
    private func showCustomPeriodView() {
        UIView.animate(withDuration: 0.25, animations: {
            self.customPeriodView.alpha = 1
        })
    }
    
    private func hideCustomPeriodView() {
        getDeviceFuelLevel(for: .customPeriod)
        todayLabel.text = "\(startPeriodTxt.text!) - \(endPeriodTxt.text!)"
        UIView.animate(withDuration: 0.25, animations: {
            self.customPeriodView.alpha = 0
        })
    }
    
    @objc private func dismissPickerCustomPeriodDate() {
        view.endEditing(true)
    }
    
    @objc private func handlePickerStartDate() {
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = StateController.getDateFormat()
        startPeriodTxt.text = dateFormatter.string(from: datePickerStartDate.date)
    }
    
    @objc private func handlePickerEndDate() {
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = StateController.getDateFormat()
        endPeriodTxt.text = dateFormatter.string(from: datePickerEndDate.date)
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension FuelLevelViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return alimentari.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlimentareTableViewCell") as! AlimentareTableViewCell
        cell.configure(with: alimentari[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fuelLevelMapCoordinator = FuelLevelMapCoordinator(presenter: navigationController ?? UINavigationController(), alimentare: alimentari[indexPath.row])
        fuelLevelMapCoordinator.start()
    }
}
