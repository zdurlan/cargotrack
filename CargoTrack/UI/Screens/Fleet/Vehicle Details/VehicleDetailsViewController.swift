//
//  VehicleDetailsViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 06/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import GoogleMaps
import RealmSwift
import UIKit

class VehicleDetailsViewController: BaseLightStatusBarViewController, GMSMapViewDelegate {
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var mapView: GMSMapView!
    @IBOutlet private weak var panelView: UIView!
    @IBOutlet private weak var topStackView: UIStackView!
    @IBOutlet private var panelViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var flagImage: UIImageView!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var coordinatesLabel: UILabel!
    @IBOutlet private weak var lastRequestedFromServerDateLabel: UILabel!
    @IBOutlet private weak var controlsButton: UIButton!
    @IBOutlet private weak var controlsTrailingConstraint: NSLayoutConstraint!
    
    // MARK: - CAR outlets
    @IBOutlet private weak var carStatusImage: UIImageView!
    @IBOutlet private weak var carStatusLabel: UILabel!
    @IBOutlet private weak var carKmLabel: UILabel!
    @IBOutlet private weak var carSpeedLabel: UILabel!
    @IBOutlet private weak var carBatteryLabel: UILabel!
    @IBOutlet private weak var carBatteryImage: UIImageView!
    @IBOutlet private weak var fuelLabel: UILabel!
    @IBOutlet private weak var oilLabel: UILabel!
    @IBOutlet private weak var lastVehicleUpdateDateLabel: UILabel!
    @IBOutlet private weak var lastRequestedFromServerLabel: UILabel! {
        didSet {
            lastRequestedFromServerLabel.text = "last_requested_from_server".localized
        }
    }
    @IBOutlet private weak var lastVehicleUpdateLabel: UILabel! {
        didSet {
            lastVehicleUpdateLabel.text = "last_vehicle_update".localized
        }
    }
    @IBOutlet var notificationsButton: UIButton!
    @IBOutlet private var fuelLevelIcon: UIImageView!
    @IBOutlet private var fuelConsumptionIcon: UIImageView!
    
    var vehicle: Vehicle!
    private var carMarker: GMSMarker!
    var firstTime: Bool = true
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
        if let label = vehicle.label, !label.isEmpty {
            titleLabel.text = label
        } else {
            titleLabel.text = vehicle.name
        }
        mapView.mapType = .normal
        addSwipeDownGesture()
        addSwipeUpGesture()
        NotificationCenter.default.addObserver(self, selector: #selector(updateMap(_:)), name: .didUpdateVehicles, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkDeviceLatestPositionBackground), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkDeviceLatestPositionFirstTime()
        WSSClient.sharedInstance.webSockets.forEach {
            $0.connect()
        }
        timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { _ in
            if WSSClient.sharedInstance.areWSSNotConnected() {
                self.checkDeviceLatestPosition()
            }
        }
        checkUnreadNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    private func checkUnreadNotifications() {
        showActivityIndicator()
        APIClient().perform(NotificationsService.getUnreadNotificationsCount()) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success(let response):
                if let notificationsResponse = try? response.decode(to: CountResponse.self) {
                    DispatchQueue.main.async {
                        strongSelf.notificationsButton.isHidden = notificationsResponse.body.total == 0
                    }
                } else {
                    DispatchQueue.main.async {
                        APIClient().perform(SlackService.sendMessage("Notifications count on user: \(StateController.currentUser?.name ?? "")")) { _ in }
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    strongSelf.presentAlertController(title: "error_title".localized, message: error.localizedDescription)
                }
            }
        }
    }
    
    @objc private func checkDeviceLatestPositionBackground() {
        StateController.fromBackground = true
        if !vehicle.isInvalidated {
            if let storedVehicle = DBManager.sharedInstance.getVehicle(id: vehicle.id) {
                self.showActivityIndicator(withBackgroundAlpha: 0.6)
                let dateFormatter = DateFormatter()
                let vehicleName = storedVehicle.name
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                let dateTime = dateFormatter.string(from: Date())
                var newPositions: [[String: Any]]?
                DispatchQueue.main.async {
                    let dateFormatterX = DateFormatter()
                    dateFormatterX.dateFormat = StateController.getDateFormat()
                    dateFormatterX.timeZone = TimeZone.current
                    let deviceDateRepresented = dateFormatterX.string(from: Date())
                    self.lastRequestedFromServerDateLabel.text = deviceDateRepresented
                }
                let newPositionsSemaphore = DispatchSemaphore(value: 0)
                APIClient().perform(DeviceService.createDevicePositionsRequest(deviceId: [vehicle.secondaryId], dsServer: vehicle.hostServer, dateTime: dateTime)) { result in
                    switch result {
                    case .success(let response):
                        guard let json = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: [.allowFragments]) as? [[String: Any]], json.count > 0 else {
                            APIClient().perform(SlackService.sendMessage("Could not refresh \(vehicleName) on user: \(StateController.currentUser?.name ?? "") positions from API")) { _ in }
                            debugPrint("error decoding JSON")
                            newPositionsSemaphore.signal()
                            return
                        }
                        newPositions = json
                        newPositionsSemaphore.signal()
                    case .failure:
                        APIClient().perform(SlackService.sendMessage("Error decoding JSON \(vehicleName) on user: \(StateController.currentUser?.name ?? "") positions from API")) { _ in }
                        newPositionsSemaphore.signal()
                    }
                }
                newPositionsSemaphore.wait()
                if newPositions != nil {
                    let newPos = newPositions!.first(where: {
                        $0["deviceid"] as! Int == vehicle.secondaryId
                    })!
                    DBManager.sharedInstance.updateVehicle(vehicle.secondaryId, newPosition: newPos)
                }
                DispatchQueue.main.async {
                    self.setupUI()
                    self.getDeviceStatusSince()
                    self.getVehicleLocation()
                }
            }
        }
    }
    
    @objc private func checkDeviceLatestPosition() {
        if let storedVehicle = DBManager.sharedInstance.getVehicle(id: vehicle.id) {
            let dateFormatter = DateFormatter()
            let vehicleName = storedVehicle.name
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            let dateTime = dateFormatter.string(from: Date())
            var newPositions: [[String: Any]]?
            DispatchQueue.main.async {
                let dateFormatterX = DateFormatter()
                dateFormatterX.dateFormat = StateController.getDateFormat()
                dateFormatterX.timeZone = TimeZone.current
                let deviceDateRepresented = dateFormatterX.string(from: Date())
                self.lastRequestedFromServerDateLabel.text = deviceDateRepresented
            }
            let newPositionsSemaphore = DispatchSemaphore(value: 0)
            APIClient().perform(DeviceService.createDevicePositionsRequest(deviceId: [vehicle.secondaryId], dsServer: vehicle.hostServer, dateTime: dateTime)) { result in
                switch result {
                case .success(let response):
                    guard let json = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: [.allowFragments]) as? [[String: Any]], json.count > 0 else {
                        APIClient().perform(SlackService.sendMessage("Could not refresh \(vehicleName) on user: \(StateController.currentUser?.name ?? "") positions from API")) { _ in }
                        debugPrint("error decoding JSON")
                        newPositionsSemaphore.signal()
                        return
                    }
                    newPositions = json
                    newPositionsSemaphore.signal()
                case .failure:
                    APIClient().perform(SlackService.sendMessage("Error decoding JSON \(vehicleName) on user: \(StateController.currentUser?.name ?? "") positions from API")) { _ in }
                    newPositionsSemaphore.signal()
                }
            }
            newPositionsSemaphore.wait()
            if newPositions != nil {
                let newPos = newPositions!.first(where: {
                    $0["deviceid"] as! Int == vehicle.secondaryId
                })!
                DBManager.sharedInstance.updateVehicle(vehicle.secondaryId, newPosition: newPos)
            }
            DispatchQueue.main.async {
                self.setupUI()
                self.getDeviceStatusSince()
                self.getVehicleLocation()
            }
        }
    }
    
    private func checkDeviceLatestPositionFirstTime() {
        showActivityIndicator()
        StateController.fromBackground = false
        let vehicleId = vehicle.id
        let secondaryId = vehicle.secondaryId
        let hostServer = vehicle.hostServer
        let usedValues = vehicle.usedValues
        DispatchQueue.global(qos: .background).async {
            if let storedVehicle = DBManager.sharedInstance.getVehicle(id: vehicleId),
                let deviceTime = storedVehicle.deviceTime {
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                let deviceDate = dateFormatter.date(from: deviceTime)!
                DispatchQueue.main.async {
                    let dateFormatterX = DateFormatter()
                    dateFormatterX.timeZone = TimeZone(abbreviation: "UTC")
                    dateFormatterX.dateFormat = "YYYY-MM-dd HH:mm:ss"
                    let deviceDate = dateFormatterX.date(from: deviceTime)!
                    dateFormatterX.dateFormat = StateController.getDateFormat()
                    dateFormatterX.timeZone = TimeZone.current
                    let deviceDateRepresented = dateFormatterX.string(from: deviceDate)
                    self.lastRequestedFromServerDateLabel.text = deviceDateRepresented
                }
                let components = Calendar.current.dateComponents([.hour, .minute], from: deviceDate, to: Date())
                let minutesDifference = components.minute ?? 0
                let vehicleName = storedVehicle.name
                if minutesDifference > 1 {
                    dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                    let dateTime = dateFormatter.string(from: Date())
                    var newPositions: [[String: Any]]?
                    let newPositionsSemaphore = DispatchSemaphore(value: 0)
                    APIClient().perform(DeviceService.createDevicePositionsRequest(deviceId: [secondaryId], dsServer: hostServer, dateTime: dateTime)) { result in
                        switch result {
                        case .success(let response):
                            guard let json = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: [.allowFragments]) as? [[String: Any]], json.count > 0 else {
                                APIClient().perform(SlackService.sendMessage("Could not refresh \(vehicleName) on user: \(StateController.currentUser?.name ?? "") positions from API")) { _ in }
                                debugPrint("error decoding JSON")
                                newPositionsSemaphore.signal()
                                return
                            }
                            newPositions = json
                            newPositionsSemaphore.signal()
                        case .failure:
                            APIClient().perform(SlackService.sendMessage("Error decoding JSON \(vehicleName) on user: \(StateController.currentUser?.name ?? "") positions from API")) { _ in }
                            newPositionsSemaphore.signal()
                        }
                    }
                    newPositionsSemaphore.wait()
                    if newPositions != nil {
                        let newPos = newPositions!.first(where: {
                            $0["deviceid"] as! Int == secondaryId
                        })!
                        DispatchQueue.main.async {
                            DBManager.sharedInstance.updateVehicle(secondaryId, newPosition: newPos)
                        }
                    }
                    DispatchQueue.main.async {
                        self.setupUI()
                        self.getDeviceStatusSince()
                        self.getVehicleLocation()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.setupUI()
                        self.getDeviceStatusSince()
                        if usedValues?.location == nil {
                            self.getVehicleLocation()
                        }
                    }
                }
            }
        }
    }
    
    private func getVehicleLocation() {
        let vehicleName = vehicle.name
        if let latitude = vehicle.usedValues?.latitude,
            let latitudeDouble = Double(latitude),
            let longitude = vehicle.usedValues?.longitude,
            let longitudeDouble = Double(longitude) {
            APIClient().perform(DeviceService.createReverseGeocodingSingleRequest(latitude: latitudeDouble, longitude: longitudeDouble, dsServer: vehicle.hostServer)) { [weak self] result in
                guard let strongSelf = self else {
                    return
                }
                switch result {
                case .success(let response):
                    if let deviceAddress = try? response.decode(to: DeviceAddress.self) {
                        DispatchQueue.main.async {
                            strongSelf.coordinatesLabel.text = latitude + " " + longitude
                            if strongSelf.carMarker == nil {
                                strongSelf.carMarker = GMSMarker()
                            }
                            strongSelf.carMarker.position = CLLocationCoordinate2D(latitude: latitudeDouble, longitude: longitudeDouble)
                            strongSelf.carMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                            strongSelf.carMarker.map = strongSelf.mapView
                            strongSelf.fitToMarker(marker: strongSelf.carMarker, firstTime: strongSelf.firstTime)
                            strongSelf.addressLabel.text = deviceAddress.body.addressFormatted
                            strongSelf.flagImage.image = UIImage(named: (deviceAddress.body.addressElements["country_code"]!)!.uppercased())
                            let realm = try! Realm()
                            try! realm.write {
                                strongSelf.vehicle.usedValues?.location = deviceAddress.body.addressFormatted
                                strongSelf.vehicle.usedValues?.countryCode = deviceAddress.body.addressElements["country_code"]!
                            }
                            strongSelf.hideActivityIndicator()
                        }
                    } else {
                        APIClient().perform(SlackService.sendMessage("Could not get location for \(vehicleName) on user: \(StateController.currentUser?.name ?? "")")) { _ in }
                        debugPrint("error getting location")
                    }
                case .failure:
                    APIClient().perform(SlackService.sendMessage("Could not get location for \(vehicleName) on user: \(StateController.currentUser?.name ?? "")")) { _ in }
                    debugPrint("error getting location")
                }
            }
        }
    }
    
    @objc func updateMap(_ notification: Notification) {
        if !vehicle.isInvalidated {
            if let deviceId = notification.userInfo?["deviceid"] as? Int,
                deviceId == vehicle.id {
                if vehicle != nil {
                    vehicle = DBManager.sharedInstance.getVehicle(id: vehicle.id)
                    setupUI()
                    getDeviceStatusSince()
                }
            }
        }
    }
    
    private func fitToMarker(marker: GMSMarker, firstTime: Bool) {
        let mapLocation = GMSCameraPosition.camera(withLatitude: Double(marker.position.latitude), longitude: Double(marker.position.longitude), zoom: firstTime ? 14 : mapView.camera.zoom)
        guard let userCoordinates = mapView.myLocation?.coordinate, mapView.isMyLocationEnabled else {
            mapView.animate(to: mapLocation)
            return
        }
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(userCoordinates)
        bounds = bounds.includingCoordinate(marker.position)
        mapView.animate(with: GMSCameraUpdate.fit(bounds))
    }
    
    private func setupUI() {
        mapView.settings.myLocationButton = true
        mapView.delegate = self

        if !vehicle.isInvalidated {
            if vehicle.usedValues?.location != nil {
                addressLabel.text = vehicle.usedValues?.location
                flagImage.image = UIImage(named: (vehicle.usedValues!.countryCode!).uppercased())
            }

            if let latitude = vehicle.usedValues?.latitude,
                let longitude = vehicle.usedValues?.longitude {
                coordinatesLabel.text = latitude + " " + longitude
                if self.carMarker == nil {
                    self.carMarker = GMSMarker()
                }
                self.carMarker.position = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
                self.carMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                self.carMarker.map = mapView
                fitToMarker(marker: carMarker, firstTime: firstTime)
                firstTime = false
            } else {
                coordinatesLabel.isHidden = true
            }

            if let deviceTime = vehicle.deviceTime {
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                let deviceDate = dateFormatter.date(from: deviceTime)!
                dateFormatter.dateFormat = StateController.getDateFormat()
                dateFormatter.timeZone = TimeZone.current
                let deviceDateRepresented = dateFormatter.string(from: deviceDate)
                lastVehicleUpdateDateLabel.text = deviceDateRepresented
            }

            if let carStatus = vehicle.usedValues?.status {
                if let status = Int(carStatus) {
                    carStatusImage.isHidden = false
                    switch status {
                    case 0:
                        carStatusImage.image = UIImage(named: "parkedIcon")
                        self.carMarker.icon = UIImage(named: "carParkedPin")
                    case 1:
                        carStatusImage.image = UIImage(named: "neutralIcon")
                        self.carMarker.icon = UIImage(named: "carNeutralPin")
                    case 2:
                        carStatusImage.image = UIImage(named: "drivingIcon")
                        self.carMarker.icon = UIImage(named: "carDrivingPin")
                    default:
                        carStatusImage.isHidden = true
                        break
                    }
                } else {
                    carStatusImage.image = UIImage(named: "parkedIcon")
                    self.carMarker.icon = UIImage(named: "carParkedPin")
                }
            } else {
                carStatusImage.image = UIImage(named: "parkedIcon")
                self.carMarker.icon = UIImage(named: "carParkedPin")
            }

            if let course = vehicle.usedValues?.course {
                self.carMarker.rotation = Double(course)!
            }

            if let mileage = vehicle.usedValues?.mileage {
                if let mileageDouble = Double(mileage) {
                    carKmLabel.text = String(format: "%.0f", mileageDouble + Double(vehicle.attributeCorrections?.attrMileageCorrectVal ?? 0)) + "km"
                } else {
                    carKmLabel.text = "-"
                }
            } else if let mileage = vehicle.oldValues?.mileage {
                if let mileageDouble = Double(mileage) {
                    carKmLabel.text = String(format: "%.0f", mileageDouble + Double(vehicle.attributeCorrections?.attrMileageCorrectVal ?? 0)) + "km"
                } else {
                    carKmLabel.text = "-"
                }
            } else {
                carKmLabel.text = "-"
            }

            if let powerSupply = vehicle.usedValues?.powerSupply {
                if let doublePowerSupply = Double(powerSupply) {
                    carBatteryLabel.text = String(format: "%.1f", doublePowerSupply/1000.0).replacingOccurrences(of: ",", with: ".") + "v"
                } else {
                    carBatteryLabel.text = "-"
                }
            } else if let powerSupply = vehicle.oldValues?.powerSupply {
                if let doublePowerSupply = Double(powerSupply) {
                    carBatteryLabel.text = String(format: "%.1f", doublePowerSupply/1000.0).replacingOccurrences(of: ",", with: ".") + "v"
                } else {
                    carBatteryLabel.text = "-"
                }
            } else {
                carBatteryLabel.text = "-"
            }

            if let fuelLevel = vehicle.usedValues?.fuelLevel {
                if let doubleFuelLevel = Double(fuelLevel), doubleFuelLevel > 0 {
                    fuelLabel.text = String(format: "%.0f", doubleFuelLevel) + "L"
                    fuelLevelIcon.image = UIImage(named: "fuelStarIcon")
                    fuelLabel.textColor = UIColor(red: 0/255, green: 100/255, blue: 255/255, alpha: 1)
                } else {
                    fuelLabel.textColor = UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1)
                    fuelLabel.text = "-"
                    fuelLevelIcon.image = UIImage(named: "fuelLevelNotAvailable")
                }
            } else if let fuelLevel = vehicle.oldValues?.fuelLevel {
                if let doubleFuelLevel = Double(fuelLevel), doubleFuelLevel > 0 {
                    fuelLabel.text = String(format: "%.0f", doubleFuelLevel) + "L"
                    fuelLevelIcon.image = UIImage(named: "fuelStarIcon")
                    fuelLabel.textColor = UIColor(red: 0/255, green: 100/255, blue: 255/255, alpha: 1)
                } else {
                    fuelLabel.textColor = UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1)
                    fuelLabel.text = "-"
                    fuelLevelIcon.image = UIImage(named: "fuelLevelNotAvailable")
                }
            } else {
                fuelLabel.textColor = UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1)
                fuelLabel.text = "-"

                fuelLevelIcon.image = UIImage(named: "fuelLevelNotAvailable")
            }

            if let fuelConsumption = vehicle.usedValues?.fuelConsumption, fuelConsumption != "0" {
                if let doubleFuelConsumption = Double(fuelConsumption) {
                    oilLabel.text = String(format: "%.1f", doubleFuelConsumption).replacingOccurrences(of: ",", with: ".") + "%"
                    fuelConsumptionIcon.image = UIImage(named: "consumptionStarIcon")
                    oilLabel.textColor = UIColor(red: 0/255, green: 100/255, blue: 255/255, alpha: 1)
                } else {
                    oilLabel.textColor = UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1)
                    oilLabel.text = "-"
                    fuelConsumptionIcon.image = UIImage(named: "fuelConsumptionNotAvailable")
                }
            } else if let fuelConsumption = vehicle.oldValues?.fuelConsumption, fuelConsumption != "0" {
                if let doubleFuelConsumption = Double(fuelConsumption) {
                    oilLabel.text = String(format: "%.1f", doubleFuelConsumption).replacingOccurrences(of: ",", with: ".") + "%"
                    fuelConsumptionIcon.image = UIImage(named: "consumptionStarIcon")
                    oilLabel.textColor = UIColor(red: 0/255, green: 100/255, blue: 255/255, alpha: 1)
                } else {
                    oilLabel.textColor = UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1)
                    oilLabel.text = "-"
                    fuelConsumptionIcon.image = UIImage(named: "fuelConsumptionNotAvailable")
                }
            } else {
                oilLabel.textColor = UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1)
                oilLabel.text = "-"
                fuelConsumptionIcon.image = UIImage(named: "fuelConsumptionNotAvailable")
            }

            if let speed = vehicle.usedValues?.speed {
                carSpeedLabel.text = "\(speed) km/h"
            } else if let speed = vehicle.oldValues?.speed {
                carSpeedLabel.text = "\(speed) km/h"
            } else {
                carSpeedLabel.text = "-"
            }

            if let userOptions = StateController.currentUser?.userOptions {
                if userOptions.contains("command-position-transmission") || userOptions.contains("command-buzzer") || userOptions.contains("command-immobiliser") {
                    controlsButton.isHidden = false
                    controlsTrailingConstraint.constant = 0
                } else {
                    controlsButton.isHidden = true
                    controlsTrailingConstraint.constant = -40
                }
            } else {
                controlsButton.isHidden = true
                controlsTrailingConstraint.constant = -40
            }
        }
    }
    
    private func getDeviceStatusSince() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let startDate = dateFormatter.string(from: Date())
        guard let status = vehicle.usedValues?.status else {
            return
        }
        APIClient().perform(DeviceService.createDeviceStatusSinceRequest(dsServer: vehicle.hostServer, deviceId: vehicle.secondaryId, startDate: startDate, status: status)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success(let response):
                if let deviceStatusSince = try? response.decode(to: DeviceStatusSince.self) {
                    let statusTime = deviceStatusSince.body.dateStatusSince
                    let timeDate = dateFormatter.date(from: statusTime)!
                    let calendar = Calendar.current
                    let timeComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: timeDate)
                    let nowComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: Date())
                    
                    let difference = calendar.dateComponents([.hour, .minute], from: timeComponents, to: nowComponents)
                    DispatchQueue.main.async {
                        strongSelf.carStatusLabel.text = "\(difference.hour!)h \(difference.minute!)m"
                    }
                } else {
                    DispatchQueue.main.async {
                        strongSelf.carStatusLabel.text = "-"
                    }
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func addSwipeDownGesture() {
        let slideDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissPanelView(gesture:)))
        slideDown.direction = .down
        panelView.addGestureRecognizer(slideDown)
    }
    
    private func addSwipeUpGesture() {
        let slideUp = UISwipeGestureRecognizer(target: self, action: #selector(showPanelView(gesture:)))
        slideUp.direction = .up
        panelView.addGestureRecognizer(slideUp)
    }
    
    @objc func dismissPanelView(gesture: UISwipeGestureRecognizer) {
        panelViewHeightConstraint.constant = 35
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
            self.topStackView.isHidden = true
        })
    }
    
    @objc func showPanelView(gesture: UISwipeGestureRecognizer) {
        panelViewHeightConstraint.constant = 176
        UIView.animate(withDuration: 0.25, animations: {
            self.topStackView.isHidden = false
        })
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapControls(_ sender: Any) {
        let vehicleControlsCoordinator = VehicleControlsCoordinator(presenter: navigationController ?? UINavigationController(), vehicle: vehicle)
        vehicleControlsCoordinator.start()
    }
    
    @IBAction func didTapHistory(_ sender: Any) {
        let vehicleHistoryCoordinator = VehicleHistoryCoordinator(presenter: navigationController ?? UINavigationController(), vehicle: vehicle)
        vehicleHistoryCoordinator.start()
    }
    
    @IBAction func didTapMaps(_ sender: Any) {
        guard let latitude = vehicle.usedValues?.latitude,
            let longitude = vehicle.usedValues?.longitude else {
                ToastUtil.toastMessageInTheMiddle(message: "no_location_found".localized)
                return
        }
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!) {
            UIApplication.shared.open(URL(string: "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!)
        } else {
            UIApplication.shared.open(URL(string: "https://maps.google.com/?q=@\(latitude),\(longitude)")!)
        }
    }
    
    @IBAction func didTapCopy(_ sender: Any) {
        UIPasteboard.general.string = addressLabel.text! + " " + coordinatesLabel.text!
        ToastUtil.toastMessageInTheMiddle(message: "address_copied".localized)
    }
    
    @IBAction func didTapFuelLevel(_ sender: Any) {
        if fuelLabel.text != "-" {
            let fuelLevelCoordinator = FuelLevelCoordinator(presenter: navigationController ?? UINavigationController(), vehicle: vehicle)
            fuelLevelCoordinator.start()
        }
    }
    
    @IBAction func didTapFuelConsumption(_ sender: Any) {
        if oilLabel.text != "-" {
            let fuelConsumptionCoordinator = FuelConsumptionCoordinator(presenter: navigationController ?? UINavigationController(), vehicle: vehicle)
            fuelConsumptionCoordinator.start()
        }
    }
    
    @IBAction func didTapNotifications(_ sender: Any) {
        let notificationsCoordinator = NotificationsCoordinator(presenter: navigationController ?? UINavigationController(), isFromMenu: false)
        notificationsCoordinator.start()
    }
    
    @IBAction func didTapMapType(_ sender: Any) {
        let satelliteAction = UIAlertAction(title: "satellite".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .satellite
        }
        let hybridAction = UIAlertAction(title: "hybrid".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .hybrid
        }
        let normalAction = UIAlertAction(title: "normal".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.mapView.mapType = .normal
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        presentAlertController(title: nil, message: nil, actions: [satelliteAction, hybridAction, normalAction, cancelAction], style: .actionSheet)
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapView.isMyLocationEnabled = !mapView.isMyLocationEnabled
        return true
    }
}
