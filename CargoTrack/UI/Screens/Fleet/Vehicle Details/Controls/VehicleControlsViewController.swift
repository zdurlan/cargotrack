//
//  VehicleControlsViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 07/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

enum VehiclePositionsFrequency: String {
    case fifteenMinutes
    case thirtyMinutes
    case oneHour
    case twoHours
    case threeHours
    case sixHours
    case tweleveHours
    case twentyFourHours
    
    var localizedFrequency: String {
        switch self {
        case .fifteenMinutes:
            return "15_minutes".localized
        case .thirtyMinutes:
            return "30_minutes".localized
        case .oneHour:
            return "1_hour".localized
        case .twoHours:
            return "2_hours".localized
        case .threeHours:
            return "3_hours".localized
        case .sixHours:
            return "6_hours".localized
        case .tweleveHours:
            return "12_hours".localized
        case .twentyFourHours:
            return "24_hours".localized
        }
    }
}

class VehicleControlsViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private weak var vehicleControlLabel: UILabel! {
        didSet {
            vehicleControlLabel.text = "vehicle_controls".localized
        }
    }
    @IBOutlet private weak var customPeriodView: UIView!
    @IBOutlet private weak var startPeriodTxt: UITextField! {
        didSet {
            dateFormatter.locale = Locale.current
            dateFormatter.dateFormat = StateController.getDateFormat()
            startPeriodTxt.text = dateFormatter.string(from: Date())
        }
    }
    @IBOutlet private weak var endPeriodTxt: UITextField! {
        didSet {
            dateFormatter.locale = Locale.current
            dateFormatter.dateFormat = StateController.getDateFormat()
            endPeriodTxt.text = dateFormatter.string(from: Date())
        }
    }
    @IBOutlet private weak var startDateLabel: UILabel! {
        didSet {
            startDateLabel.text = "start_date".localized
        }
    }
    @IBOutlet private weak var endDateLabel: UILabel! {
        didSet {
            endDateLabel.text = "end_date".localized
        }
    }
    
    var vehicle: Vehicle!
    private var selectedPositionsCell: VehicleControlsPositionTableViewCell!
    private var selectedAlarmCell: VehicleControlsAlarmTableViewCell!
    private var selectedImmobiliserCell: VehicleControlsImmobiliserTableViewCell!
    private var vehiclePositionExpanded = false
    private var vehicleAlarmExpanded = false
    private var vehicleImmobiliserExpanded = false
    private var vehicleCameraExpanded = false
    private var numberOfItems: [String] = []
    private let dateFormatter = DateFormatter()
    private let datePickerStartDate = UIDatePicker()
    private let datePickerEndDate = UIDatePicker()
    private var currentDeviceTransmissionId = 0
    private var vehicleId: Int = 0
    private var vehicleName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vehicleId = vehicle.id
        vehicleName = vehicle.name
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
        registerCells()
        if let userOptions = StateController.currentUser?.userOptions {
            if userOptions.contains("command-position-transmission") {
                numberOfItems.append("command-position-transmission")
            }
            if userOptions.contains("command-buzzer") {
                numberOfItems.append("command-buzzer")
            }
            if userOptions.contains("command-immobiliser") {
                numberOfItems.append("command-immobiliser")
            }
//            if userOptions.contains("command-camera") {
//                numberOfItems.append("command-camera")
//            }
        }
        tableView.reloadData()
        
        datePickerStartDate.datePickerMode = .dateAndTime
        datePickerStartDate.maximumDate = Date()
        datePickerStartDate.locale = Locale.current
        datePickerStartDate.addTarget(self, action: #selector(handlePickerStartDate), for: .valueChanged)
        
        datePickerEndDate.datePickerMode = .dateAndTime
        datePickerEndDate.maximumDate = Date()
        datePickerEndDate.locale = Locale.current
        datePickerEndDate.addTarget(self, action: #selector(handlePickerEndDate), for: .valueChanged)
        
        let toolbarPickerDate = UIToolbar().ToolbarPicker(mySelect: #selector(dismissPickerCustomPeriodDate))
        startPeriodTxt.inputView = datePickerStartDate
        startPeriodTxt.inputAccessoryView = toolbarPickerDate
        
        endPeriodTxt.inputView = datePickerEndDate
        endPeriodTxt.inputAccessoryView = toolbarPickerDate
        
        if numberOfItems.contains("command-position-transmission") {
            getActiveControlsPositionTransmission()
        }
        
        if numberOfItems.contains("command-buzzer") {
            getActiveControlsCommandStatus(type: "buzzer")
        }
        
        if numberOfItems.contains("command-immobiliser") {
            getActiveControlsCommandStatus(type: "immobiliser")
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(receivedCommandNotification(_:)), name: .didReceiveDeviceCommand, object: nil)
    }
    
    private func registerCells() {
        tableView.register(UINib(nibName: "VehicleControlsPositionTableViewCell", bundle: nil), forCellReuseIdentifier: "VehicleControlsPositionTableViewCell")
        tableView.register(UINib(nibName: "VehicleControlsAlarmTableViewCell", bundle: nil), forCellReuseIdentifier: "VehicleControlsAlarmTableViewCell")
        tableView.register(UINib(nibName: "VehicleControlsImmobiliserTableViewCell", bundle: nil), forCellReuseIdentifier: "VehicleControlsImmobiliserTableViewCell")
        tableView.register(UINib(nibName: "VehicleControlsCameraTableViewCell", bundle: nil), forCellReuseIdentifier: "VehicleControlsCameraTableViewCell")
    }
    
    @objc private func receivedCommandNotification(_ notification: Notification) {
        getActiveControlsCommandStatus(type: "buzzer")
        getActiveControlsCommandStatus(type: "immobiliser")
    }
    
    private func getActiveControlsCommandStatus(type: String) {
        showActivityIndicator()
        APIClient().perform(DeviceService.getDeviceCommandStatusRequest(vehicleId: vehicleId, type: type)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success(let response):
                if let deviceCommandResponse = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: .mutableContainers) {
                    if (deviceCommandResponse as! [String: Int])["status"] != 0 {
                        DispatchQueue.main.async {
                            if type == "buzzer" {
                                strongSelf.selectedAlarmCell.configureOn()
                            } else {
                                strongSelf.selectedImmobiliserCell.configureOn()
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            if type == "buzzer" {
                                strongSelf.selectedAlarmCell.resetToDefaults()
                            } else {
                                strongSelf.selectedImmobiliserCell.configureOff()
                            }
                        }
                    }
                } else {
                    APIClient().perform(SlackService.sendMessage("Error getting active controls command transmission on \(StateController.currentUser?.name ?? "") \(strongSelf.vehicleName)")) { _ in }
                }
            case .failure:
                APIClient().perform(SlackService.sendMessage("Error getting active controls status command on \(StateController.currentUser?.name ?? "") \(strongSelf.vehicleName)")) { _ in }
            }
        }
    }
    
    private func getActiveControlsPositionTransmission() {
        showActivityIndicator()
        APIClient().perform(DeviceService.getActiveDevicePositionTransmissionJob(vehicleId: vehicleId)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success(let response):
                if let devicePositionTransmissionResponse = try? response.decode(to: DevicePositionTransmissionResponse.self) {
                    if devicePositionTransmissionResponse.body.status == "success" {
                        DispatchQueue.main.async {
                            let positionsCell = strongSelf.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! VehicleControlsPositionTableViewCell
                            if let devicePositionTransmissionJob = devicePositionTransmissionResponse.body.devPosTrJob {
                                positionsCell.configure(with: devicePositionTransmissionJob)
                                strongSelf.currentDeviceTransmissionId = devicePositionTransmissionJob.id
                            }
                        }
                    }
                } else {
                    APIClient().perform(SlackService.sendMessage("Error getting active controls position transmission on \(StateController.currentUser?.name ?? "") \(strongSelf.vehicleName)")) { _ in }
                }
            case .failure:
                APIClient().perform(SlackService.sendMessage("Error getting active controls position transmission on \(StateController.currentUser?.name ?? "") \(strongSelf.vehicleName)")) { _ in }
            }
        }
    }
    
    private func showCustomPeriodView() {
        UIView.animate(withDuration: 0.25, animations: {
            self.customPeriodView.alpha = 1
        })
    }
    
    @objc private func handlePickerStartDate() {
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = StateController.getDateFormat()
        startPeriodTxt.text = dateFormatter.string(from: datePickerStartDate.date)
    }
    
    @objc private func handlePickerEndDate() {
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = StateController.getDateFormat()
        endPeriodTxt.text = dateFormatter.string(from: datePickerEndDate.date)
    }
    
    @objc private func dismissPickerCustomPeriodDate() {
        view.endEditing(true)
    }

    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapOutsideCustomPeriodView(_ sender: Any) {
        UIView.animate(withDuration: 0.25, animations: {
            self.customPeriodView.alpha = 0
        })
    }
    
    @IBAction func didTapApply(_ sender: Any) {
        let calendar = Calendar.current
        let date1 = calendar.startOfDay(for: datePickerStartDate.date)
        let date2 = calendar.startOfDay(for: datePickerEndDate.date)
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        if components.day ?? 0 < 62 {
            selectedPositionsCell.todayLabel.text = "\(startPeriodTxt.text!) - \(endPeriodTxt.text!)"
            UIView.animate(withDuration: 0.25, animations: {
                self.customPeriodView.alpha = 0
            })
        } else {
            presentAlertController(title: "", message: "maximum_interval".localized)
        }
    }
}

extension VehicleControlsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            if numberOfItems.contains("command-position-transmission") {
                vehiclePositionExpanded = !vehiclePositionExpanded
            } else if numberOfItems.contains("command-buzzer") {
                vehicleAlarmExpanded = !vehicleAlarmExpanded
            } else {
                vehicleImmobiliserExpanded = !vehicleImmobiliserExpanded
            }
        case 1:
            if numberOfItems.contains("command-position-transmission") {
                vehicleAlarmExpanded = !vehicleAlarmExpanded
            } else if numberOfItems.contains("command-buzzer") {
                vehicleImmobiliserExpanded = !vehicleImmobiliserExpanded
            }
        case 2:
            vehicleImmobiliserExpanded = !vehicleImmobiliserExpanded
        case 3:
            vehicleCameraExpanded = !vehicleCameraExpanded
        default:
            break
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            if numberOfItems.contains("command-position-transmission") {
                return vehiclePositionExpanded ? 617 : 54
            } else if numberOfItems.contains("command-buzzer") {
                return vehicleAlarmExpanded ? 150 : 54
            } else {
                return vehicleImmobiliserExpanded ? 150 : 54
            }
        case 1:
            if numberOfItems.contains("command-position-transmission") {
                return vehicleAlarmExpanded ? 150 : 54
            } else {
                return vehicleImmobiliserExpanded ? 150 : 54
            }
        case 2:
            return vehicleImmobiliserExpanded ? 150 : 54
        case 3:
            return vehicleCameraExpanded ? 320 : 54
        default:
            return 0
        }
    }
}

extension VehicleControlsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfItems.count
    }
    
    
    @objc private func dismissPickerFrequency() {
        selectedPositionsCell.thirtyMinutesTxt.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentItem = numberOfItems[indexPath.row]
        switch currentItem {
        case "command-position-transmission":
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleControlsPositionTableViewCell", for: indexPath) as! VehicleControlsPositionTableViewCell
            cell.setExpanded(vehiclePositionExpanded)
            self.selectedPositionsCell = cell
            cell.vehicleControlsDelegate = self
            cell.selectionStyle = .none
            let toolbarPickerFrequency = UIToolbar().ToolbarPicker(mySelect: #selector(dismissPickerFrequency))
            cell.thirtyMinutesTxt.inputAccessoryView = toolbarPickerFrequency
            return cell
        case "command-buzzer":
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleControlsAlarmTableViewCell", for: indexPath) as! VehicleControlsAlarmTableViewCell
            cell.setExpanded(vehicleAlarmExpanded)
            cell.vehicleControlsDelegate = self
            self.selectedAlarmCell = cell
            cell.selectionStyle = .none
            return cell
        case "command-immobiliser":
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleControlsImmobiliserTableViewCell", for: indexPath) as! VehicleControlsImmobiliserTableViewCell
            cell.setExpanded(vehicleImmobiliserExpanded)
            cell.vehicleControlsDelegate = self
            self.selectedImmobiliserCell = cell
            cell.selectionStyle = .none
            return cell
        case "command-camera":
            let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleControlsCameraTableViewCell", for: indexPath) as! VehicleControlsCameraTableViewCell
            cell.setExpanded(vehicleCameraExpanded)
            cell.selectionStyle = .none
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
}

extension VehicleControlsViewController: VehicleControlPositionsDelegate {
    func didTapTime(cell: VehicleControlsPositionTableViewCell) {
        let todayAction = UIAlertAction(title: "today".localized, style: .default) { action in
            cell.todayLabel.text = "today".localized
        }
        let customPeriodAction = UIAlertAction(title: "custom_period".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.selectedPositionsCell = cell
            strongSelf.showCustomPeriodView()
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        presentAlertController(title: nil, message: nil, actions: [todayAction, customPeriodAction, cancelAction], style: .actionSheet)
    }
    
    func didTapSave(isSave: Bool, frequency: String, email: String, phone: String, sendAddress: Bool, sendStatus: Bool, sendSpeed: Bool) {
        if isSave {
            var startDate = ""
            var endDate = ""
            let calendar = Calendar.current
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            if selectedPositionsCell.todayLabel.text == "today".localized {
                let startOfDay = calendar.startOfDay(for: Date())
                var components = DateComponents()
                components.day = 1
                components.second = -1
                let dateAtEnd = calendar.date(byAdding: components, to: startOfDay)!
                startDate = dateFormatter.string(from: startOfDay)
                endDate = dateFormatter.string(from: dateAtEnd)
            } else {
                startDate = dateFormatter.string(from: datePickerStartDate.date)
                endDate = dateFormatter.string(from: datePickerEndDate.date)
            }
            var frequencyMinutes = ""
            switch frequency {
            case VehiclePositionsFrequency.fifteenMinutes.localizedFrequency:
                frequencyMinutes = "15"
            case VehiclePositionsFrequency.thirtyMinutes.localizedFrequency:
                frequencyMinutes = "30"
            case VehiclePositionsFrequency.oneHour.localizedFrequency:
                frequencyMinutes = "60"
            case VehiclePositionsFrequency.twoHours.localizedFrequency:
                frequencyMinutes = "\(2*60)"
            case VehiclePositionsFrequency.threeHours.localizedFrequency:
                frequencyMinutes = "\(3*60)"
            case VehiclePositionsFrequency.sixHours.localizedFrequency:
                frequencyMinutes = "\(6*60)"
            case VehiclePositionsFrequency.tweleveHours.localizedFrequency:
                frequencyMinutes = "\(12*60)"
            case VehiclePositionsFrequency.twentyFourHours.localizedFrequency:
                frequencyMinutes = "\(24*60)"
            default:
                break
            }
            showActivityIndicator()
            APIClient().perform(DeviceService.saveDevicePositionTransmissionJob(vehicleId: vehicleId, startDate: startDate, endDate: endDate, frequency: frequencyMinutes, email: email, phone: phone, sendAddress: sendAddress, sendStatus: sendStatus, sendSpeed: sendSpeed)) { [weak self] result in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.hideActivityIndicator()
                strongSelf.getActiveControlsPositionTransmission()
            }
        } else {
            showActivityIndicator()
            APIClient().perform(DeviceService.deactivateDevicePositionTransmissionJob(transmissionId: currentDeviceTransmissionId)) { [weak self] result in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.hideActivityIndicator()
                DispatchQueue.main.async {
                    strongSelf.selectedPositionsCell.resetToDefaults()
                }
            }
        }
    }
}

extension VehicleControlsViewController: VehicleControlImmobiliserDelegate {
    func didTapLock(isLock: Bool) {
        let command = isLock ? "on" : "off"
        APIClient().perform(DeviceService.sendCommandRequest(command: command, commandTime: "", commandType: "immobiliser", vehicleId: vehicleId)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
        }
    }
}

extension VehicleControlsViewController: VehicleControlAlarmDelegate {
    func didTapStart(isStart: Bool, timeInterval: String) {
        showActivityIndicator()
        let command = isStart ? "on" : "off"
        APIClient().perform(DeviceService.sendCommandRequest(command: command, commandTime: timeInterval, commandType: "buzzer", vehicleId: vehicleId)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
        }
    }
}
