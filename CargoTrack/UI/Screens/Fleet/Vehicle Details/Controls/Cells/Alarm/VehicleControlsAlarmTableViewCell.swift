//
//  VehicleControlsAlarmTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 07/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

protocol VehicleControlAlarmDelegate: class {
    func didTapStart(isStart: Bool, timeInterval: String)
}

class VehicleControlsAlarmTableViewCell: UITableViewCell {
    @IBOutlet private weak var thirtySecondsTxt: UITextField! {
        didSet {
            thirtySecondsTxt.text = "30_seconds".localized
            thirtySecondsTxt.delegate = self
        }
    }
    @IBOutlet private weak var startButton: UIButton! {
        didSet {
            startButton.setTitle("start_button".localized, for: .normal)
        }
    }
    @IBOutlet private weak var soundTheAlarmLabel: UILabel! {
        didSet {
            soundTheAlarmLabel.text = "sound_the_alarm".localized
        }
    }
    @IBOutlet private weak var refreshIcon: UIImageView!
    @IBOutlet private weak var activateAlarmLabel: UILabel! {
        didSet {
            activateAlarmLabel.text = "activate_vehicle_alarm".localized
        }
    }
    @IBOutlet private weak var arrowButton: UIButton!
    
    weak var vehicleControlsDelegate: VehicleControlAlarmDelegate?
    private let frequencyPicker = UIPickerView()
    private let pickerValues = ["30_seconds".localized, "1_minute".localized, "3_minutes".localized, "6_minutes".localized]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        thirtySecondsTxt.inputView = frequencyPicker
        
        frequencyPicker.delegate = self
        frequencyPicker.dataSource = self
        frequencyPicker.selectRow(0, inComponent: 0, animated: false)
    }
    
    @IBAction func didTapStart(_ sender: Any) {
        var isStart = true
        if startButton.currentTitle == "stop_button".localized {
            isStart = false
        }
        var timeInterval = ""
        switch thirtySecondsTxt.text! {
        case pickerValues[0]:
            timeInterval = "30"
        case pickerValues[1]:
            timeInterval = "60"
        case pickerValues[2]:
            timeInterval = "180"
        case pickerValues[3]:
            timeInterval = "360"
        default:
            break
        }
        vehicleControlsDelegate?.didTapStart(isStart: isStart, timeInterval: timeInterval)
    }
    
    func configureOn() {
        startButton.setTitle("stop_button".localized, for: .normal)
        startButton.backgroundColor = UIColor(red: 0/255, green: 200/255, blue: 125/255, alpha: 1)
    }
    
    func resetToDefaults() {
        startButton.setTitle("start_button".localized, for: .normal)
        startButton.backgroundColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
    }
    
    func setExpanded(_ shouldExpand: Bool) {
        thirtySecondsTxt.isHidden = !shouldExpand
        startButton.isHidden = !shouldExpand
        soundTheAlarmLabel.isHidden = !shouldExpand
        refreshIcon.isHidden = !shouldExpand
        arrowButton.isHidden = !shouldExpand
    }
}

extension VehicleControlsAlarmTableViewCell: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerValues[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        thirtySecondsTxt.text = pickerValues[row]
    }
}

extension VehicleControlsAlarmTableViewCell: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerValues.count
    }
}

extension VehicleControlsAlarmTableViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == thirtySecondsTxt {
            return false
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
