//
//  VehicleControlsCameraTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 07/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class VehicleControlsCameraTableViewCell: UITableViewCell {
    @IBOutlet private weak var cameraView: UIView!
    @IBOutlet private weak var vehicleCameraLabel: UILabel! {
        didSet {
            vehicleCameraLabel.text = "vehicle_camera".localized
        }
    }
    @IBOutlet private weak var takeSnapshotButton: UIButton! {
        didSet {
            takeSnapshotButton.setTitle("take_snapshot".localized, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setExpanded(_ shouldExpand: Bool) {
        cameraView.isHidden = !shouldExpand
    }
}
