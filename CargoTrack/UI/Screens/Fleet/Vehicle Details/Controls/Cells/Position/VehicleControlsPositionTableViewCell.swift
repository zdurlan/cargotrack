//
//  VehicleControlsPositionTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 07/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

protocol VehicleControlPositionsDelegate: class {
    func didTapTime(cell: VehicleControlsPositionTableViewCell)
    func didTapSave(isSave: Bool, frequency: String, email: String, phone: String, sendAddress: Bool, sendStatus: Bool, sendSpeed: Bool)
}

class VehicleControlsPositionTableViewCell: UITableViewCell {
    @IBOutlet private weak var vehiclePositionStackView: UIStackView!
    @IBOutlet private weak var saveButton: UIButton! {
        didSet {
            saveButton.setTitle("save_button".localized, for: .normal)
        }
    }
    @IBOutlet private weak var coordinatesLabel: UILabel! {
        didSet {
            coordinatesLabel.text = "coordinates".localized
        }
    }
    @IBOutlet private weak var statusLabel: UILabel! {
        didSet {
            statusLabel.text = "status".localized
        }
    }
    @IBOutlet private weak var addressLabel: UILabel! {
        didSet {
            addressLabel.text = "address".localized
        }
    }
    @IBOutlet private weak var speedLabel: UILabel! {
        didSet {
            speedLabel.text = "speed".localized
        }
    }
    @IBOutlet private weak var sendVehiclePositionLabel: UILabel! {
        didSet {
            sendVehiclePositionLabel.text = "send_vehicle_position".localized
        }
    }
    @IBOutlet private weak var timeIntervalLabel: UILabel! {
        didSet {
            timeIntervalLabel.text = "time_interval".localized
        }
    }
    @IBOutlet weak var todayLabel: UILabel! {
        didSet {
            todayLabel.text = "today".localized
        }
    }
    @IBOutlet private weak var todayButton: UIButton!
    @IBOutlet private weak var updateIntervalLabel: UILabel! {
        didSet {
            updateIntervalLabel.text = "update_interval".localized
        }
    }
    @IBOutlet weak var thirtyMinutesTxt: UITextField! {
        didSet {
            thirtyMinutesTxt.text = "30_minutes".localized
            thirtyMinutesTxt.delegate = self
        }
    }
    @IBOutlet private weak var emailTxt: UITextField! {
        didSet {
            emailTxt.placeholder = "email_address".localized
            emailTxt.text = StateController.currentUser?.email ?? ""
        }
    }
    @IBOutlet private weak var smsTxt: UITextField! {
        didSet {
            smsTxt.placeholder = "sms_phone_number".localized
            smsTxt.text = StateController.currentUser?.phone ?? ""
        }
    }
    @IBOutlet private weak var detailsToSendLabel: UILabel! {
        didSet {
            detailsToSendLabel.text = "details_to_send".localized
        }
    }
    @IBOutlet private weak var statusSwitch: UISwitch!
    @IBOutlet private weak var addressSwitch: UISwitch!
    @IBOutlet private weak var speedSwitch: UISwitch!
    
    weak var vehicleControlsDelegate: VehicleControlPositionsDelegate?
    
    private let frequencyPicker = UIPickerView()
    private let pickerValues = ["15_minutes".localized, "30_minutes".localized, "1_hour".localized, "2_hours".localized, "3_hours".localized, "6_hours".localized, "12_hours".localized, "24_hours".localized]
    
    override func awakeFromNib() {
        super.awakeFromNib()

        thirtyMinutesTxt.inputView = frequencyPicker

        frequencyPicker.delegate = self
        frequencyPicker.dataSource = self
        frequencyPicker.selectRow(1, inComponent: 0, animated: false)
    }

    func setExpanded(_ shouldExpand: Bool) {
        vehiclePositionStackView.isHidden = !shouldExpand
        saveButton.isHidden = !shouldExpand
    }
    
    func resetToDefaults() {
        saveButton.setTitle("save_button".localized, for: .normal)
        saveButton.backgroundColor = UIColor(red: 0/255, green: 200/255, blue: 125/255, alpha: 1)
    }
    
    func configure(with devicePosition: DevicePositionTransmission) {
        saveButton.setTitle("deactivate".localized, for: .normal)
        saveButton.backgroundColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
        emailTxt.text = devicePosition.email
        smsTxt.text = devicePosition.phone
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let beginDate = dateFormatter.date(from: devicePosition.periodBegin)!
        let endDate = dateFormatter.date(from: devicePosition.periodEnd)!
        dateFormatter.dateFormat = StateController.getDateFormat().replacingOccurrences(of: "HH:mm:ss", with: "")
        dateFormatter.timeZone = TimeZone.current
        let beginDateRepresented = dateFormatter.string(from: beginDate)
        let endDateRepresented = dateFormatter.string(from: endDate)
        todayLabel.text = "\(beginDateRepresented) - \(endDateRepresented)"
        switch devicePosition.frequency {
        case 15:
            thirtyMinutesTxt.text = "15_minutes".localized
            frequencyPicker.selectRow(0, inComponent: 0, animated: false)
        case 30:
            thirtyMinutesTxt.text = "30_minutes".localized
            frequencyPicker.selectRow(1, inComponent: 0, animated: false)
        case 60:
            thirtyMinutesTxt.text = "1_hour".localized
            frequencyPicker.selectRow(2, inComponent: 0, animated: false)
        case 120:
            thirtyMinutesTxt.text = "2_hours".localized
            frequencyPicker.selectRow(3, inComponent: 0, animated: false)
        case 180:
            thirtyMinutesTxt.text = "3_hours".localized
            frequencyPicker.selectRow(4, inComponent: 0, animated: false)
        case 360:
            thirtyMinutesTxt.text = "6_hours".localized
            frequencyPicker.selectRow(5, inComponent: 0, animated: false)
        case 720:
            thirtyMinutesTxt.text = "12_hours".localized
            frequencyPicker.selectRow(6, inComponent: 0, animated: false)
        case 1440:
            thirtyMinutesTxt.text = "24_hours".localized
            frequencyPicker.selectRow(7, inComponent: 0, animated: false)
        default:
            break
        }
        statusSwitch.isOn = devicePosition.sendStatus == 1 ? true : false
        addressSwitch.isOn = devicePosition.sendAddress == 1 ? true : false
        speedSwitch.isOn = devicePosition.sendSpeed == 1 ? true : false
    }
    
    @IBAction func didTapTime(_ sender: Any) {
        vehicleControlsDelegate?.didTapTime(cell: self)
    }
    
    @IBAction func didSwitchCoordinates(_ sender: UISwitch) {
        coordinatesLabel.textColor = sender.isOn ? UIColor(red: 0/255, green: 100/255, blue: 255/255, alpha: 1) : UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1)
    }
    
    @IBAction func didSwitchStatus(_ sender: UISwitch) {
        statusLabel.textColor = sender.isOn ? UIColor(red: 0/255, green: 100/255, blue: 255/255, alpha: 1) : UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1)
    }
    
    @IBAction func didSwitchAddress(_ sender: UISwitch) {
        addressLabel.textColor = sender.isOn ? UIColor(red: 0/255, green: 100/255, blue: 255/255, alpha: 1) : UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1)
    }
    
    @IBAction func didSwitchSpeed(_ sender: UISwitch) {
        speedLabel.textColor = sender.isOn ? UIColor(red: 0/255, green: 100/255, blue: 255/255, alpha: 1) : UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1)
    }
    
    @IBAction func didTapSave(_ sender: Any) {
        var isSave = true
        if saveButton.currentTitle == "deactivate".localized {
            isSave = false
        }
        vehicleControlsDelegate?.didTapSave(isSave: isSave, frequency: thirtyMinutesTxt.text!, email: emailTxt.text!, phone: smsTxt.text!, sendAddress: addressSwitch.isOn, sendStatus: statusSwitch.isOn, sendSpeed: speedSwitch.isOn)
    }
}

extension VehicleControlsPositionTableViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == thirtyMinutesTxt {
            return false
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

extension VehicleControlsPositionTableViewCell: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerValues[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        thirtyMinutesTxt.text = pickerValues[row]
    }
}

extension VehicleControlsPositionTableViewCell: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerValues.count
    }
}
