//
//  VehicleControlsImmobiliserTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 07/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

protocol VehicleControlImmobiliserDelegate: class {
    func didTapLock(isLock: Bool)
}

class VehicleControlsImmobiliserTableViewCell: UITableViewCell {
    @IBOutlet private weak var lockGrayIcon: UIImageView!
    @IBOutlet private weak var lockButton: UIButton! {
        didSet {
            lockButton.setTitle("lock_button".localized, for: .normal)
        }
    }
    @IBOutlet private weak var currentStatusLabel: UILabel! {
        didSet {
            currentStatusLabel.text = "current_status".localized
        }
    }
    @IBOutlet private weak var unlockedButton: UIButton! {
        didSet {
            unlockedButton.setTitle("unlocked".localized, for: .normal)
        }
    }
    @IBOutlet private weak var activateVehicleLabel: UILabel! {
        didSet {
            activateVehicleLabel.text = "activate_vehicle_immobiliser".localized
        }
    }
    
    weak var vehicleControlsDelegate: VehicleControlImmobiliserDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureOn() {
        lockButton.setTitle("unlock_button".localized, for: .normal)
        lockButton.backgroundColor = UIColor(red: 0/255, green: 200/255, blue: 125/255, alpha: 1)
        unlockedButton.setTitle("locked".localized, for: .normal)
        unlockedButton.setTitleColor(UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1), for: .normal)
    }
    
    func configureOff() {
        lockButton.setTitle("lock_button".localized, for: .normal)
        lockButton.backgroundColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
        unlockedButton.setTitle("unlocked".localized, for: .normal)
        unlockedButton.setTitleColor(UIColor(red: 0/255, green: 200/255, blue: 125/255, alpha: 1), for: .normal)
    }

    @IBAction func didTapLock(_ sender: Any) {
        var isLock = false
        if lockButton.currentTitle == "lock_button".localized {
            isLock = true
        }
        vehicleControlsDelegate?.didTapLock(isLock: isLock)
    }
    
    func setExpanded(_ shouldExpand: Bool) {
        lockGrayIcon.isHidden = !shouldExpand
        lockButton.isHidden = !shouldExpand
        currentStatusLabel.isHidden = !shouldExpand
        unlockedButton.isHidden = !shouldExpand
    }
    
}
