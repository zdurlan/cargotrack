//
//  MyFleetViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 27/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import RealmSwift
import Starscream
import UIKit

enum VehicleFilterType {
    case stationary
    case moving
    case none
}

class MyFleetViewController: BaseLightStatusBarViewController {
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar! {
        didSet {
            searchBar.placeholder = "search_vehicles".localized
        }
    }
    @IBOutlet private weak var myFleetTitleLabel: UILabel! {
        didSet {
            myFleetTitleLabel.text = "my_fleet_title".localized
        }
    }
    @IBOutlet private var notificationsButton: UIButton!
    
    private var myFleetList: [Vehicle] = []
    private var deviceAttributeLogic: DeviceAttributeLogic!
    private var filteredFleetList: [Vehicle] = []
    private var isFilteredButtonActive = false
    private var dsServersDictionary: [String: [Int]] = [:]
    private var filterType: VehicleFilterType = .none
    private var timer7: Timer?
    private var timer55: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "MyFleetTableViewCell", bundle: nil), forCellReuseIdentifier: "MyFleetTableViewCell")
        searchBar.delegate = self
        tableView.reloadData()
        getDeviceList()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: .didUpdateVehicles, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadLatestPositions), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        timer7 = Timer.scheduledTimer(withTimeInterval: 7, repeats: true) { _ in
            if WSSClient.sharedInstance.areWSSNotConnected() {
                self.reloadLatestPositions()
            }
        }
        timer55 = Timer.scheduledTimer(withTimeInterval: 55, repeats: true) { _ in
            WSSClient.sharedInstance.sendPingMessage(dsServer: DSServer.ds1.rawValue)
            WSSClient.sharedInstance.sendPingMessage(dsServer: DSServer.ds2.rawValue)
            WSSClient.sharedInstance.sendPingMessage(dsServer: DSServer.ds3.rawValue)
            WSSClient.sharedInstance.sendPingMessage(dsServer: DSServer.ds4.rawValue)
            WSSClient.sharedInstance.sendPingMessage(dsServer: DSServer.ds5.rawValue)
            WSSClient.sharedInstance.sendPingMessage(dsServer: DSServer.ds6.rawValue)
            WSSClient.sharedInstance.sendPingMessage(dsServer: DSServer.ds7.rawValue)
            WSSClient.sharedInstance.sendPingMessage(dsServer: DSServer.ds8.rawValue)
            WSSClient.sharedInstance.sendPingMessage(dsServer: DSServer.ds9.rawValue)
        }
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
        if StateController.fromBackground {
            StateController.fromBackground = false
            reloadLatestPositions()
        }
        checkUnreadNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer7?.invalidate()
        timer55?.invalidate()
    }
    
    @objc func reloadLatestPositions() {
        let configuration = Realm.Configuration(
            encryptionKey: nil,
            schemaVersion: 3,
            migrationBlock: { migration, _ in
                migration.enumerateObjects(ofType: "Vehicle", { _, new in
                    new?["fuelConsumption"] = FuelConsumptionSensor()
                    new?["leakTreshold"] = 10
                    new?["refuelingTreshold"] = 10
                })
        })
        
        let realm = try! Realm(configuration: configuration)
        let vehicles = realm.objects(Vehicle.self)
        for vehicle in vehicles {
            let vehicleRef = ThreadSafeReference(to: vehicle)
            DispatchQueue.global(qos: .background).async {
                let realm = try! Realm()
                guard let vehicle = realm.resolve(vehicleRef) else {
                    return // object was deleted
                }
                if let deviceTime = vehicle.deviceTime {
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                    dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                    let deviceDate = dateFormatter.date(from: deviceTime)!
                    let components = Calendar.current.dateComponents([.hour, .minute], from: deviceDate, to: Date())
                    let minutesDifference = components.minute ?? 0
                    if vehicle.isInvalidated {
                        return
                    }
                    let vehicleName = vehicle.name
                    if minutesDifference > 1 {
                        let dateTime = dateFormatter.string(from: Date())
                        var newPositions: [[String: Any]]?
                        let newPositionsSemaphore = DispatchSemaphore(value: 0)
                        APIClient().perform(DeviceService.createDevicePositionsRequest(deviceId: [vehicle.secondaryId], dsServer: vehicle.hostServer, dateTime: dateTime)) { result in
                            switch result {
                            case .success(let response):
                                guard let json = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: [.allowFragments]) as? [[String: Any]], json.count > 0 else {
                                    APIClient().perform(SlackService.sendMessage("Could not refresh \(vehicleName) on user: \(StateController.currentUser?.name ?? "") positions from API")) { _ in }
                                    debugPrint("error decoding JSON")
                                    newPositionsSemaphore.signal()
                                    return
                                }
                                newPositions = json
                                newPositionsSemaphore.signal()
                            case .failure:
                                APIClient().perform(SlackService.sendMessage("Error decoding JSON \(vehicleName) on user: \(StateController.currentUser?.name ?? "") positions from API")) { _ in }
                            }
                        }
                        newPositionsSemaphore.wait()
                        if newPositions != nil {
                            let newPos = newPositions!.first(where: {
                                $0["deviceid"] as! Int == vehicle.secondaryId
                            })!
                            DBManager.sharedInstance.updateVehicleFromBackground(vehicle, newPosition: newPos)
                        }
                    }
                }
            }
            WSSClient.sharedInstance.webSockets.forEach {
                $0.connect()
            }
        }
    }
    
    @objc func reloadData() {
        myFleetList = Array(DBManager.sharedInstance.getVehiclesFromDB())
        tableView.reloadData()
    }
    
    private func getDeviceList() {
        if DBManager.sharedInstance.getVehiclesFromDB().count > 0 {
            myFleetList = Array(DBManager.sharedInstance.getVehiclesFromDB())
            DBManager.sharedInstance.getLocationsForEveryVehicle()
            getDevicesDSServersAndSetupWSS()
            tableView.reloadData()
        } else {
            showActivityIndicator()
            APIClient().perform(DeviceService.createDeviceListRequest()) { [weak self] result in
                guard let strongSelf = self else {
                    return
                }
                switch result {
                case .success(let response):
                    if let vehicles = try? response.decode(to: [Vehicle].self) {
                        strongSelf.myFleetList = vehicles.body
                        strongSelf.getDevicesDSServersAndSetupWSS()
                        strongSelf.getDevicePositions()
                    } else {
                        strongSelf.hideActivityIndicator()
                        DispatchQueue.main.async {
                            APIClient().perform(SlackService.sendMessage("Device list error on user: \(StateController.currentUser?.name ?? "")")) { _ in }
                            strongSelf.presentAlertController(title: "error_title".localized, message: "device_list_error".localized)
                        }
                    }
                case .failure(let error):
                    strongSelf.hideActivityIndicator()
                    DispatchQueue.main.async {
                        strongSelf.presentAlertController(title: "error_title".localized, message: error.localizedDescription)
                    }
                }
            }
        }
    }
    
    private func checkUnreadNotifications() {
        showActivityIndicator()
        APIClient().perform(NotificationsService.getUnreadNotificationsCount()) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success(let response):
                if let notificationsResponse = try? response.decode(to: CountResponse.self) {
                    DispatchQueue.main.async {
                        strongSelf.notificationsButton.isHidden = notificationsResponse.body.total == 0
                    }
                } else {
                    DispatchQueue.main.async {
                        APIClient().perform(SlackService.sendMessage("Notifications count on user: \(StateController.currentUser?.name ?? "")")) { _ in }
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    strongSelf.presentAlertController(title: "error_title".localized, message: error.localizedDescription)
                }
            }
        }
    }
    
    private func getDevicesDSServersAndSetupWSS() {
        myFleetList.forEach { vehicle in
            let hostServer = vehicle.hostServer
            let vehicleId = vehicle.secondaryId
            if self.dsServersDictionary[hostServer] != nil {
                self.dsServersDictionary[hostServer]?.append(vehicleId)
            } else {
                self.dsServersDictionary[hostServer] = [vehicleId]
            }
        }
        setupWSS()
    }
    
    private func setupWSS() {
        WSSClient.sharedInstance.webSockets.forEach {
            $0.disconnect()
        }
        WSSClient.sharedInstance.webSockets.removeAll()

        for (hostServer, deviceIdArray) in dsServersDictionary {
            WSSClient.sharedInstance.createWSSocketFleet(dsServer: hostServer, deviceIds: deviceIdArray)
        }
    }
    
    private func getDevicePositions() {
        let dispatchGroup = DispatchGroup()

        for (hostServer, deviceIds) in dsServersDictionary {
            dispatchGroup.enter()
            APIClient().perform(DeviceService.createDevicePositionsRequest(deviceId: deviceIds, dsServer: hostServer)) { [weak self] result in
                guard let strongSelf = self else {
                    return
                }
                switch result {
                case .success(let response):
                    if let json = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: [.allowFragments]) as? [[String: Any]], json.count > 0 {
                        for currentDevice in strongSelf.myFleetList.filter({ $0.hostServer == hostServer }) {
                            if let positions = json.first(where: {
                                $0["deviceid"] as! Int == currentDevice.secondaryId
                            }) {
                                if currentDevice.name == "B72HCU" {
                                    strongSelf.deviceAttributeLogic = DeviceAttributeLogic(rawValues: positions, definitions: currentDevice.attributeDefinitions)
                                }
                                let device = strongSelf.myFleetList.first(where: {
                                    $0.secondaryId == currentDevice.secondaryId
                                })!
                                device.usedValues = currentDevice.getData(deviceAttributeLogic: strongSelf.deviceAttributeLogic)
                                DispatchQueue.main.async {
                                    if device.usedValues == nil {
                                        strongSelf.myFleetList.removeAll(where: {
                                            $0.secondaryId == device.secondaryId
                                        })
                                    }
                                    strongSelf.tableView.reloadData()
                                }
                            } else {
                                APIClient().perform(SlackService.sendMessage("vehicle \(currentDevice.name) on user: \(StateController.currentUser?.name ?? "") not configured")) { _ in }
                                strongSelf.myFleetList.removeAll(where: {
                                    $0.secondaryId == currentDevice.secondaryId
                                })
                                DispatchQueue.main.async {
                                    strongSelf.tableView.reloadData()
                                }
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            strongSelf.tableView.reloadData()
                        }
                        APIClient().perform(SlackService.sendMessage("Error decoding JSON - check vehicle DS1 on user: \(StateController.currentUser?.name ?? "")")) { _ in }
                    }
                case .failure(let error):
                    print(error)
                }
                strongSelf.hideActivityIndicator()
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            DBManager.sharedInstance.addVehiclesToDB(strongSelf.myFleetList)
            DBManager.sharedInstance.getLocationsForEveryVehicle()
        }
        
    }
    
    private func isFiltering() -> Bool {
        return !searchBarIsEmpty() || isFilteredButtonActive
    }
    
    private func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchBar.text?.isEmpty ?? true
    }
    
    @IBAction func didTapNotifications(_ sender: Any) {
        let notificationsCoordinator = NotificationsCoordinator(presenter: navigationController ?? UINavigationController(), isFromMenu: false)
        notificationsCoordinator.start()
    }
    
    @IBAction func didTapMenu(_ sender: Any) {
        var menuAlreadyPresent = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MainMenuViewController.self) {
                menuAlreadyPresent = true
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        if !menuAlreadyPresent {
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
            view.window!.layer.add(transition, forKey: kCATransition)
            let mainMenuCoordinator = MainMenuCoordinator(presenter: navigationController ?? UINavigationController())
            mainMenuCoordinator.start()
        }
    }
    
    @IBAction func didTapMap(_ sender: Any) {
        let allVehiclesMapCoordinator = AllVehiclesMapCoordinator(presenter: navigationController ?? UINavigationController(), vehicles: myFleetList)
        allVehiclesMapCoordinator.start()
    }
    
    @IBAction func didTapFilter(_ sender: Any) {
        let allVehiclesAction = UIAlertAction(title: "all_vehicles".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.searchBar.text = ""
            strongSelf.isFilteredButtonActive = false
            strongSelf.filterType = .none
            strongSelf.tableView.reloadData()
        }
        let movingVehiclesAction = UIAlertAction(title: "moving_vehicles".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.filteredFleetList.removeAll()
            strongSelf.myFleetList.forEach {
                if let carStatus = $0.usedValues?.status {
                    guard let status = Int(carStatus) else {
                        return
                    }
                    switch status {
                    case 2:
                        strongSelf.filteredFleetList.append($0)
                    default:
                        break
                    }
                }
            }
            strongSelf.searchBar.text = ""
            strongSelf.isFilteredButtonActive = true
            strongSelf.filterType = .moving
            strongSelf.tableView.reloadData()
        }
        let stationaryVehiclesAction = UIAlertAction(title: "stationary_vehicles".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.filteredFleetList.removeAll()
            strongSelf.myFleetList.forEach {
                if let carStatus = $0.usedValues?.status {
                    guard let status = Int(carStatus) else {
                        return
                    }
                    switch status {
                    case 0:
                        strongSelf.filteredFleetList.append($0)
                    default:
                        break
                    }
                }
            }
            strongSelf.searchBar.text = ""
            strongSelf.isFilteredButtonActive = true
            strongSelf.filterType = .stationary
            strongSelf.tableView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        presentAlertController(title: nil, message: nil, actions: [allVehiclesAction, movingVehiclesAction, stationaryVehiclesAction, cancelAction], style: .actionSheet)
    }
}

extension MyFleetViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vehicle = isFiltering() ? filteredFleetList[indexPath.row] : myFleetList[indexPath.row]
        let vehicleDetailsCoordinator = VehicleDetailsCoordinator(presenter: navigationController ?? UINavigationController(), vehicle: vehicle)
        vehicleDetailsCoordinator.start()
    }
}

extension MyFleetViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredFleetList.count
        }
        return myFleetList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyFleetTableViewCell", for: indexPath) as! MyFleetTableViewCell
        let vehicle = isFiltering() ? filteredFleetList[indexPath.row] : myFleetList[indexPath.row]
        cell.configure(with: vehicle)
        return cell
    }
}

extension MyFleetViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            if !isFilteredButtonActive {
                filteredFleetList = myFleetList.filter {
                    if let label = $0.label {
                        return label.lowercased().contains(searchText.lowercased())
                    } else {
                        return $0.name.lowercased().contains(searchText.lowercased())
                    }
                }
            } else {
                filteredFleetList = filteredFleetList.filter {
                    if let label = $0.label {
                        return label.lowercased().contains(searchText.lowercased())
                    } else {
                        return $0.name.lowercased().contains(searchText.lowercased())
                    }
                }
            }
        } else {
            filteredFleetList.removeAll()
            myFleetList.forEach {
                if let carStatus = $0.usedValues?.status {
                    guard let status = Int(carStatus) else {
                        return
                    }
                    if filterType == .moving {
                        switch status {
                        case 2:
                            filteredFleetList.append($0)
                        default:
                            break
                        }
                    } else if filterType == .stationary {
                        switch status {
                        case 0:
                            filteredFleetList.append($0)
                        default:
                            break
                        }
                    }
                }
            }
        }
        tableView.reloadData()
    }
}
