//
//  MainMenuViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 25/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import KeychainSwift
import Presentr
import UIKit

private struct LinkConstants {
    static let fbLink = "https://www.facebook.com/CargoTrackMonitorizareGPS/"
    static let linkedInLink = "https://www.linkedin.com/company/cargo-track/"
    static let youtubeLink = "https://www.youtube.com/channel/UClP42P_KZHjGJg8lC8g-OAg"
}

class MainMenuViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var fleetButton: UIButton!
    @IBOutlet private weak var myProfileButton: UIButton!
    @IBOutlet private weak var notificationsButton: UIButton!
    @IBOutlet private weak var settingsButton: UIButton!
    @IBOutlet private weak var signOutButton: UIButton!
    @IBOutlet private var taxaDrumButton: UIButton!
    @IBOutlet private var fleetSeparatorView: UIView!
    @IBOutlet private var taxaDrumSeparatorView: UIView!
    @IBOutlet private var accountingButton: UIButton!
    @IBOutlet private var accountingSeparatorView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        checkUnreadNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fleetButton.setTitle("fleet".localized, for: .normal)
        myProfileButton.setTitle("my_profile".localized, for: .normal)
        notificationsButton.setTitle("notifications".localized, for: .normal)
        settingsButton.setTitle("settings_title".localized, for: .normal)
        signOutButton.setTitle("logout_button".localized, for: .normal)
        taxaDrumButton.setTitle("road_tax".localized, for: .normal)
        accountingButton.setTitle("financial".localized, for: .normal)

        if !(StateController.currentUser?.userRights?.contains(where: { $0.id == 26 }) ?? false) || (StateController.currentUser?.isSuspended ?? 0) > 0 {
            taxaDrumButton.setTitleColor(.gray, for: .normal)
            taxaDrumButton.isUserInteractionEnabled = false
        }
        
        if !(StateController.currentUser?.userRights?.contains(where: { $0.id == 6 }) ?? false || (StateController.currentUser?.mobileAppAsWrapper ?? false)) || (StateController.currentUser?.isSuspended ?? 0) > 0 {
            fleetButton.setTitleColor(.gray, for: .normal)
            fleetButton.isUserInteractionEnabled = false
        }

        if !(StateController.currentUser?.userRights?.contains(where: { $0.id == 81 }) ?? false) {
            accountingButton.setTitleColor(.gray, for: .normal)
            accountingButton.isUserInteractionEnabled = false
        }
    }
    
    private func checkUnreadNotifications() {
        showActivityIndicator()
        APIClient().perform(NotificationsService.getUnreadNotificationsCount()) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success(let response):
                if let notificationsResponse = try? response.decode(to: CountResponse.self) {
                    DispatchQueue.main.async {
                        if notificationsResponse.body.total > 0 {
                            strongSelf.notificationsButton.setImage(UIImage(named: "newNotificationIconWithStar"), for: .normal)
                        } else {
                            strongSelf.notificationsButton.setImage(UIImage(named: "notificationsIcon"), for: .normal)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        strongSelf.notificationsButton.setImage(UIImage(named: "notificationsIcon"), for: .normal)
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    strongSelf.presentAlertController(title: "error_title".localized, message: error.localizedDescription)
                }
            }
        }
    }
    
    private func showLogoutAlert() {
        let alertTitle = "logout_alert_title".localized
        let alert = UIAlertController(title: alertTitle, message: nil, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "cancel_button".localized, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "logout_button".localized, style: .destructive, handler: { [weak self] (_) in
            self?.logoutUser()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    private func logoutUser() {
        UserDefaults.standard.set(nil, forKey: "user")
        let keychain = KeychainSwift()
        if !UserDefaults.standard.bool(forKey: "isAuthEnabled") {
            keychain.delete("accessToken")
        }
//        if UserDefaults.standard.bool(forKey: "rememberMe") {
//            UserDefaults.standard.set(stateController.currentUser.username, forKey: "username")
//        }
        UserDefaults.standard.synchronize()
        WSSClient.sharedInstance.webSockets.forEach {
            $0.disconnect()
        }
        WSSClient.sharedInstance.webSockets.removeAll()
        DBManager.sharedInstance.deleteAllVehiclesFromDB()
        StateController.currentUser = nil
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: OnboardingViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func didTapFleet(_ sender: Any) {
        if StateController.currentUser?.mobileAppAsWrapper ?? false {
            let fleetWebViewCoordinator = FleetWebviewCoordinator(presenter: navigationController ?? UINavigationController())
            fleetWebViewCoordinator.start()
        } else {
            let fleetCoordinator = FleetCoordinator(presenter: navigationController ?? UINavigationController())
            fleetCoordinator.start()
        }
    }
    
    @IBAction func didTapMyProfile(_ sender: Any) {
        let profileCoordinator = MyProfileCoordinator(presenter: navigationController ?? UINavigationController())
        profileCoordinator.start()
    }
    
    @IBAction func didTapNotifications(_ sender: Any) {
        let notificationsCoordinator = NotificationsCoordinator(presenter: navigationController ?? UINavigationController(), isFromMenu: true)
        notificationsCoordinator.start()
    }
    
    @IBAction func didTapSettings(_ sender: Any) {
        let settingsCoordinator = SettingsCoordinator(presenter: navigationController ?? UINavigationController())
        settingsCoordinator.start()
    }
    
    @IBAction func didTapSignOut(_ sender: Any) {
        showLogoutAlert()
    }
    
    @IBAction func didTapFacebook(_ sender: Any) {
        if let url = URL(string: LinkConstants.fbLink) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func didTapLinkedIn(_ sender: Any) {
        if let url = URL(string: LinkConstants.linkedInLink) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func didTapYoutube(_ sender: Any) {
        if let url = URL(string: LinkConstants.youtubeLink) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func didTapTerms(_ sender: Any) {
        let presenter = Presentr(presentationType: .popup)
        let controller = TextPopupViewController()
        controller.textToShow = "terms_and_agreements_text".localized
        customPresentViewController(presenter, viewController: controller, animated: true, completion: nil)
    }
    
    @IBAction func didTapRoadTax(_ sender: Any) {
        let roadTaxCoordinator = RoadTaxCoordinator(presenter: navigationController ?? UINavigationController())
        roadTaxCoordinator.isFromFingerprint = false
        roadTaxCoordinator.start()
    }

    @IBAction func didTapAccounting(_ sender: Any) {
        let financialCoordinator = FinancialCoordinator(presenter: navigationController ?? UINavigationController())
        financialCoordinator.start()
    }
}
