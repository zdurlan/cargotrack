//
//  RoadTaxBalanceTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 01/07/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

struct CarPosition {
    let name: String
    let date: String
    let status: PositionStatus
    
    init(name: String, date: String, status: PositionStatus) {
        self.name = name
        self.date = date
        self.status = status
    }
}

enum PositionStatus {
    case notRegistered
    case notLinked
    case active
    case suspended
    
    var localizedDescription: String {
        switch self {
        case .notRegistered:
            return "not_registered".localized
        case .notLinked:
            return "not_linked".localized
        case .active:
            return "active".localized
        case .suspended:
            return "suspended".localized
        }
    }
}

class RoadTaxBalanceTableViewCell: UITableViewCell {
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var statusView: UIView!
    @IBOutlet private var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(with position: CarPosition) {
        nameLabel.textColor = UIColor(red: 90/255, green: 100/255, blue: 120/255, alpha: 1)
        dateLabel.textColor = UIColor(red: 140/255, green: 150/255, blue: 170/255, alpha: 1)
        nameLabel.text = position.name
        dateLabel.text = position.date
        switch position.status {
        case .active:
            statusView.backgroundColor = UIColor(red: 0/255, green: 200/255, blue: 125/255, alpha: 1)
            statusLabel.text = "active".localized
        case .suspended:
            statusView.backgroundColor = UIColor(red: 255/255, green: 175/255, blue: 50/255, alpha: 1)
            statusLabel.text = "suspended".localized
        case .notRegistered:
            statusView.backgroundColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
            statusLabel.text = "not_registered".localized.uppercased()
        case .notLinked:
            statusView.backgroundColor = UIColor(red: 255/255, green: 175/255, blue: 50/255, alpha: 1)
            statusLabel.text = "not_linked".localized.uppercased()
        }
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar.current
        dateFormatter.dateFormat = StateController.getDateFormat()
        dateFormatter.timeZone = TimeZone.current
        let lastTransmissionDate = dateFormatter.date(from: position.date) ?? Date()
        if getMinutesDifferenceFromTwoDates(start: lastTransmissionDate, end: Date()) >= 45 {
            backgroundColor = UIColor(red: 236/255, green: 238/255, blue: 240/255, alpha: 1)
            nameLabel.textColor = UIColor.red
            dateLabel.textColor = UIColor.red
        }
    }
    
    private func getMinutesDifferenceFromTwoDates(start: Date, end: Date) -> Int {
        let timeComponents = Calendar.current.dateComponents([.hour, .minute], from: start)
        let nowComponents = Calendar.current.dateComponents([.hour, .minute], from: end)
        
        return Calendar.current.dateComponents([.minute], from: timeComponents, to: nowComponents).minute!
    }
}
