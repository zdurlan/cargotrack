//
//  RoadTaxPaymentViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 12/08/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

enum CurrencyType: String {
    case ron
    case eur
}

class RoadTaxPaymentViewController: BaseLightStatusBarViewController {
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "BGTOLL"
        }
    }
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    var taxState: TaxState!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
    }

    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapRON(_ sender: Any) {
        let coordinator = PaymentTopUpCoordinator(presenter: navigationController ?? UINavigationController(), currency: .ron)
        coordinator.start()
    }
    
    @IBAction func didTapEUR(_ sender: Any) {
        let coordinator = PaymentTopUpCoordinator(presenter: navigationController ?? UINavigationController(), currency: .eur)
        coordinator.start()
    }
}
