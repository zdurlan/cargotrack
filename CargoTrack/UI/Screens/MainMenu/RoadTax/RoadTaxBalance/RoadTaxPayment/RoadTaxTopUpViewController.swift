//
//  RoadTaxTopUpViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 12/08/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit
import WebKit

class RoadTaxTopUpViewController: BaseLightStatusBarViewController {
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "top_up".localized + " " + currency.rawValue.uppercased()
        }
    }
    @IBOutlet private var instructionsLabel: UILabel! {
        didSet {
            instructionsLabel.text = "please_insert_amount_top_up".localized
        }
    }
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var amountTxt: UITextField! {
        didSet {
            amountTxt.delegate = self
            amountTxt.addTarget(self, action: #selector(textFieldDidChange(_:)),
                                for: UIControl.Event.editingChanged)
        }
    }
    @IBOutlet private var currencyLabel: UILabel!
    @IBOutlet private var accountWillBeLoadedWithLabel: UILabel! {
        didSet {
            accountWillBeLoadedWithLabel.text = "account_will_be_loaded_with".localized
        }
    }
    @IBOutlet private var loadedValueLabel: UILabel! {
        didSet {
            loadedValueLabel.text = "0 \(currency.rawValue.uppercased())"
        }
    }
    @IBOutlet private var serviceFeeLabel: UILabel! {
        didSet {
            serviceFeeLabel.text = "service_fee".localized
        }
    }
    @IBOutlet private var serviceFeeValueLabel: UILabel! {
        didSet {
            serviceFeeValueLabel.text = "0 \(currency.rawValue.uppercased())"
        }
    }
    @IBOutlet private var totalInvoiceLabel: UILabel! {
        didSet {
            totalInvoiceLabel.text = "total_invoice".localized
        }
    }
    @IBOutlet private var totalInvoiceValueLabel: UILabel! {
        didSet {
            totalInvoiceValueLabel.text = "0 \(currency.rawValue.uppercased())"
        }
    }
    @IBOutlet private var payButton: UIButton! {
        didSet {
            payButton.setTitle("pay".localized, for: .normal)
        }
    }
    @IBOutlet private var webActivityIndicator: UIActivityIndicatorView!
    @IBOutlet private var webViewContaiiner: UIView!
    private var webView: WKWebView!
    private var urlObservation: NSKeyValueObservation?
    
    var currency: CurrencyType!
    private var currencyResponse: CurrencyModel!
    private var amountExchanged: String = ""
    private var chargeFee = ""
    private var chargeFeeVAT = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
        getCurrencyModel()
    }
    
    private func getCurrencyModel() {
        showActivityIndicator()
        APIClient().perform(DeviceService.getCurrencyConversion(from: currency)) { result in
            self.hideActivityIndicator()
            switch result {
            case .success(let response):
                if let currencyResponse = try? response.decode(to: CurrencyModel.self) {
                    self.currencyResponse = currencyResponse.body
                } else {
                    self.presentAlertController(title: "", message: "error_message".localized)
                }
            case .failure(let error):
                print(error)
                self.presentAlertController(title: "", message: "error_message".localized)
            }
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        for vc in navigationController?.viewControllers ?? [] {
            if vc is RoadTaxBalanceViewController {
                navigationController?.popToViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func didTapPay(_ sender: Any) {
        loadPage()
    }
    
    private func setupWebView() {
        DispatchQueue.main.async {
            self.webView = WKWebView()
            self.webView.scrollView.delegate = self
            self.webView.uiDelegate = self
            self.webView.navigationDelegate = self
            self.webViewContaiiner.addSubview(self.webView)
            self.webView.pin(to: self.webViewContaiiner)
            UIView.animate(withDuration: 0.5) {
                self.webViewContaiiner.alpha = 1
                self.webViewContaiiner.isHidden = false
                self.webActivityIndicator.alpha = 1
                self.webActivityIndicator.isHidden = false
            }
        }
    }
    
    private func setupWebViewRetry() {
        webActivityIndicator.startAnimating()
        webView.reload()
    }
    
    private func getMobileWrapperURL(onComplete: @escaping (URL?) -> Void) {
        showActivityIndicator()
        APIClient().perform(DeviceService.rechargeWith(currencyModel: currencyResponse, amount: amountTxt.text!, amountExchanged: amountExchanged, chargeFee: chargeFee, chargeFeeVAT: chargeFeeVAT, currencyType: currency)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success(let response):
                if response.statusCode == 406 {
                    if let currencyResponse = try? response.decode(to: CurrencyModel.self) {
                        strongSelf.currencyResponse = currencyResponse.body
                        DispatchQueue.main.async {
                            strongSelf.calculateValues(with: strongSelf.amountTxt.text!)
                            strongSelf.loadPage()
                        }
                    } else {
                        strongSelf.presentAlertController(title: "", message: "error_message".localized)
                    }
                } else {
                    if let x = try? response.decode(to: PaymentURL.self) {
                        strongSelf.setupWebView()
                        onComplete(URL(string: x.body.paymentUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
                    } else {
                        strongSelf.presentAlertController(title: "", message: "error_message".localized)
                    }
                }
            case .failure(let error):
                print(error)
                strongSelf.presentAlertController(title: "", message: "error_message".localized)
            }
        }
    }
    
    private func loadPage() {
        webActivityIndicator.startAnimating()
        getMobileWrapperURL { url in
            DispatchQueue.main.async {
                if !(url?.absoluteString.isEmpty ?? true) {
                    self.webView.load(URLRequest(url: url!))
                }
            }
        }
    }
    
    private func calculateValues(with text: String) {
        let inputValue = Double(text.replacingOccurrences(of: ",", with: ".")) ?? 0
        let exchangeValue = inputValue * currencyResponse.exchangeRate
        let commissionValue = exchangeValue * currencyResponse.commissionPercent / 100
        let comissionValueWithFee = commissionValue + (commissionValue * Double(currencyResponse.vatPercent)/100)
        chargeFee = String(format: "%.2f", commissionValue)
        chargeFeeVAT = String(format: "%.2f", comissionValueWithFee - commissionValue)
        let invoiceValue = exchangeValue + comissionValueWithFee
        amountExchanged = "\(exchangeValue)"
        loadedValueLabel.text = "\(text) BGN" + " - " + String(format: "%.2f", exchangeValue) + " " + currency.rawValue.uppercased()
        serviceFeeValueLabel.text = String(format: "%.2f", comissionValueWithFee)
        totalInvoiceValueLabel.text = String(format: "%.2f", invoiceValue)
    }
    
    private func makeRetryAlertActions() -> [UIAlertAction] {
        let retryAction = UIAlertAction(title: "retry_button".localized, style: .default) { [weak self] (alert) in
            self?.setupWebViewRetry()
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel) { [weak self] (alert) in
            self?.navigationController?.popViewController(animated: true)
        }
        return [retryAction, cancelAction]
    }
    
    deinit {
        urlObservation?.invalidate()
    }
}

extension RoadTaxTopUpViewController: WKUIDelegate, WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        webActivityIndicator.stopAnimating()
        presentAlertController(title: "error_title".localized, message: "page_not_load".localized, actions: makeRetryAlertActions())
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webActivityIndicator.stopAnimating()
    }
}

extension RoadTaxTopUpViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.maximumZoomScale = 1.0
    }
}

extension RoadTaxTopUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        calculateValues(with: textField.text!)
    }
}
