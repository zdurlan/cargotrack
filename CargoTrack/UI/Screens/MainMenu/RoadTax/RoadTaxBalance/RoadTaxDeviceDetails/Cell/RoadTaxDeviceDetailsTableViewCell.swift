//
//  RoadTaxDeviceDetailsTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 01/07/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

enum CellType {
    case phone
    case tractorAxle
    case trailerAxle
}

class RoadTaxDeviceDetailsTableViewCell: UITableViewCell {
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    @IBOutlet private var editButton: UIButton!
    @IBOutlet private var trailingValueConstraint: NSLayoutConstraint!
    @IBOutlet var valueTextField: UITextField! {
        didSet {
            valueTextField.placeholder = "enter_data".localized
        }
    }
    
    var cellType: CellType? {
        if nameLabel.text == "responsible_phone_number".localized {
            return .phone
        } else if nameLabel.text == "tractor_axels".localized {
            return .tractorAxle
        } else if nameLabel.text == "trailer_axels".localized {
            return .trailerAxle
        }
        return nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with text: String, value: String?, shouldShowEdit: Bool) {
        nameLabel.text = text
        valueLabel.text = value
        editButton.isHidden = !shouldShowEdit
        if editButton.isHidden {
            trailingValueConstraint.constant = -30
        }
    }
    
    @IBAction func didTapEdit(_ sender: Any) {
        valueTextField.isHidden = !valueTextField.isHidden
        valueLabel.isHidden = !valueLabel.isHidden
    }
}
