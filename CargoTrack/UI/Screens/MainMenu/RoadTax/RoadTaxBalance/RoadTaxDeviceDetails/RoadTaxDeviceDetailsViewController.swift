//
//  RoadTaxDeviceDetailsViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 01/07/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

struct DeviceDetailsRoadTax: Codable {
    let country: String
    let deviceId: Int
    let lastEntry: String
    let lastExit: String
}

class RoadTaxDeviceDetailsViewController: BaseLightStatusBarViewController {
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.text = device.name
        }
    }
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var errorView: UIView!
    @IBOutlet private var errorLabel: UILabel!
    @IBOutlet private var tableView: UITableView!
    
    var device: RoadTaxDevice!
    var balance: Balance!
    private var lastEntryDate = ""
    private var lastExitDate = ""
    private var phone: String?
    private var tractorAxle: String?
    private var trailerAxle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "RoadTaxDeviceDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "RoadTaxDeviceDetailsTableViewCell")
        getLastTransmission()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
    }
    
    func getLastTransmission() {
        if device.dateSuspendedSince != nil {
            if device.isAutomatically == 1 {
                errorView.isHidden = false
                if balance is BGBalance {
                    if (balance as! BGBalance).balance < (balance as! BGBalance).tresholdLow {
                        errorLabel.text = "vehicle_suspended_funds".localized(withArguments: ("\(device.date ?? "")"))
                    } else {
                        errorLabel.text = "vehicle_suspended_not_transmitting".localized(withArguments: ("\(device.date ?? "")"))
                    }
                }
            } else {
                if device.dateSuspendedSince != nil {
                    errorView.isHidden = false
                    errorLabel.text = "vehicle_suspended_since".localized(withArguments: ("\(device.dateSuspendedSince ?? "")"))
                }
            }
        }
        showActivityIndicator()
        APIClient().perform(UserService.getUserRoadTaxDeviceLatestDate(deviceId: [device.id], dsServer: device.deviceDataServer ?? "", country: device.roadtaxCountry ?? "")) { result in
            switch result {
            case .success(let x):
                if let response = try? x.decode(to: [DeviceDetailsRoadTax].self) {
                    DispatchQueue.main.async {
                        self.lastExitDate = response.body.first?.lastExit ?? ""
                        self.lastEntryDate = response.body.first?.lastEntry ?? ""
                        self.tableView.reloadData()
                        self.hideActivityIndicator()
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.presentAlertController(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }
    
    private func saveDetailsBG() {
        showActivityIndicator()
        APIClient().perform(UserService.saveUserRoadTaxDetailsBG(deviceId: device.id, phone: phone ?? "", tractorAxle: Int(tractorAxle ?? "") ?? 0, trailerAxle: Int(trailerAxle ?? "") ?? 0)) { result in
            self.hideActivityIndicator()
            switch result {
            case .success(let response):
                if let json = try? JSONSerialization.jsonObject(with: response.body!, options: .mutableLeaves) as? [String: String] {
                    if json["message"] == "success" {
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
                        }
                    } else {
                        let error = json["error"] ?? ""
                        self.presentAlertController(title: "update_failed".localized, message: "update_vehicle_not_saved".localized + "\n" + error + "\n" + "try_later".localized)
                    }
                } else {
                    self.presentAlertController(title: "update_failed".localized, message: "update_vehicle_not_saved".localized + "\n" + "try_later".localized)
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.presentAlertController(title: "update_failed".localized, message: "update_vehicle_not_saved".localized + "\n" + error.localizedDescription + "\n" + "try_later".localized)
                }
            }
        }
    }
    
    private func saveDetailsHU() {
        showActivityIndicator()
        APIClient().perform(UserService.saveUserRoadTaxDetailsHU(deviceId: device.id, phone: phone ?? "")) { result in
            self.hideActivityIndicator()
            switch result {
            case .success:
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.presentAlertController(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }
    
    @IBAction func didTapSave(_ sender: Any) {
        if balance is BGBalance {
            var cell = tableView.cellForRow(at: IndexPath(row: 6, section: 0)) as! RoadTaxDeviceDetailsTableViewCell
            self.phone = cell.valueTextField.isHidden ? cell.valueLabel.text : cell.valueTextField.text
            
            cell = tableView.cellForRow(at: IndexPath(row: 5, section: 0)) as! RoadTaxDeviceDetailsTableViewCell
            self.trailerAxle = cell.valueTextField.isHidden ? cell.valueLabel.text : cell.valueTextField.text
            
            cell = tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! RoadTaxDeviceDetailsTableViewCell
            self.tractorAxle = cell.valueTextField.isHidden ? cell.valueLabel.text : cell.valueTextField.text
            
            if self.tractorAxle?.isEmpty ?? true || self.trailerAxle?.isEmpty ?? true || self.phone?.isEmpty ?? true {
                presentAlertController(title: "error_title".localized, message: "please_enter_all_required_fields".localized)
            } else {
                saveDetailsBG()
            }
        } else {
            let cell = tableView.cellForRow(at: IndexPath(row: 5, section: 0)) as! RoadTaxDeviceDetailsTableViewCell
            self.phone = cell.valueTextField.text
            if self.phone?.isEmpty ?? true {
                presentAlertController(title: "error_title".localized, message: "please_enter_all_required_fields".localized)
            } else {
                saveDetailsHU()
            }
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension RoadTaxDeviceDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return device.roadtaxCountry?.lowercased() == TaxState.hu.country.lowercased() ? 8 : 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RoadTaxDeviceDetailsTableViewCell", for: indexPath) as! RoadTaxDeviceDetailsTableViewCell
        var textValue = ""
        var value: String? = ""
        var shouldShowEdit = false
        switch indexPath.row {
        case 0:
            textValue = "last_transmission".localized
            value = device.date
        case 1:
            textValue = "OBUID"
            value = device.deviceObuid
        case 2:
            textValue = "country".localized
            value = device.deviceCountry
        case 3:
            textValue = "emission_class".localized
            value = device.eurocode
        case 4:
            if device.roadtaxCountry?.lowercased() == TaxState.hu.country.lowercased() {
                textValue = "axels".localized
                value = device.deviceAxleNumber
            } else {
                textValue = "tractor_axels".localized
                shouldShowEdit = true
                value = "\(device.tractorAxles ?? 0)"
            }
        case 5:
            shouldShowEdit = true
            if device.roadtaxCountry?.lowercased() == TaxState.hu.country.lowercased() {
                textValue = "responsible_phone_number".localized
                value = device.responsiblePhone
            } else {
                value = "\(device.trailerAxles ?? 0)"
                textValue = "trailer_axels".localized
            }
        case 6:
            if device.roadtaxCountry?.lowercased() == TaxState.hu.country.lowercased() {
                textValue = "last_hu_entry_date".localized
                value = lastEntryDate
            } else {
                textValue = "responsible_phone_number".localized
                value = device.responsiblePhone
                shouldShowEdit = true
            }
        case 7:
            if device.roadtaxCountry?.lowercased() == TaxState.hu.country.lowercased() {
                textValue = "last_hu_exit_date".localized
                value = lastExitDate
            } else {
                value = "\(device.grossWeight ?? 0)"
                textValue = "maximum_mass".localized
            }
        case 8:
            textValue = "main_category".localized
            let mainCategory = device.roadtaxBgMainCategory == 1 ? "bugo_device_main_category_1".localized : "bugo_device_main_category_2".localized
            value = mainCategory
        case 9:
            textValue = "gross_weight".localized
            let grossWeight = device.roadtaxBgWeightCategory == 1 ? "bugo_device_gross_weight_1".localized : "bugo_device_gross_weight_2".localized
            value = grossWeight
        case 10:
            textValue = "last_bg_entry_date".localized
            value = lastEntryDate
        case 11:
            textValue = "last_bg_exit_date".localized
            value = lastExitDate
        default:
            break
        }
        cell.selectionStyle = .none
        cell.configure(with: textValue, value: value, shouldShowEdit: shouldShowEdit)
        return cell
    }
}
