//
//  RoadTaxChooseServiceViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 28/06/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class RoadTaxChooseServiceViewController: BaseLightStatusBarViewController {
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "road_tax".localized
        }
    }
    @IBOutlet private var pleaseChooseServiceLabel: UILabel! {
        didSet {
            pleaseChooseServiceLabel.text = "please_choose_service".localized
        }
    }
    private var datasource: [TaxState] = []
    var isFromFingerprint = false
    private var alreadyTapped = false
    private var hasReloaded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "RoadTaxCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RoadTaxCollectionViewCell")
        getRoadTaxServices()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        alreadyTapped = false
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
    }
    
    private func getRoadTaxServices() {
        showActivityIndicator()
        APIClient().perform(UserService.getUserRoadTaxRequest()) { result in
            self.hideActivityIndicator()
            switch result {
            case .success(let x):
                if let response = try? x.decode(to: [String].self) {
                    response.body.forEach {
                        guard let taxState = TaxState(rawValue: $0) else {
                            return
                        }
                        self.datasource.append(taxState)
                    }
                    DispatchQueue.main.async {
                        self.hasReloaded = true
                        self.collectionView.reloadData()
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.presentAlertController(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        if isFromFingerprint {
            var menuAlreadyPresent = false
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MainMenuViewController.self) {
                    menuAlreadyPresent = true
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            if !menuAlreadyPresent {
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = CATransitionType.push
                transition.subtype = CATransitionSubtype.fromLeft
                transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
                view.window!.layer.add(transition, forKey: kCATransition)
                let mainMenuCoordinator = MainMenuCoordinator(presenter: navigationController ?? UINavigationController())
                mainMenuCoordinator.start()
            }
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
}

extension RoadTaxChooseServiceViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hasReloaded ? 2 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RoadTaxCollectionViewCell", for: indexPath) as! RoadTaxCollectionViewCell
        if datasource.count == 2 {
            let state = datasource[indexPath.row]
            cell.configure(for: state)
        } else if indexPath.row == 0 {
            if datasource.contains(.hu) {
                cell.configure(for: .hu)
            } else {
                cell.configure(for: .bg)
            }
        } else {
            if datasource.contains(.hu) {
                cell.configure(for: .emptyBG)
            } else {
                cell.configure(for: .emptyHU)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !alreadyTapped {
            alreadyTapped = true
            let taxState = datasource[indexPath.row]
            let coordinator = RoadTaxBalanceCoordinator(presenter: navigationController ?? UINavigationController(), taxState: taxState)
            coordinator.start()
        }
    }
}
