//
//  RoadTaxCollectionViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 28/06/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

enum TaxState: String {
    case hu = "hu-go"
    case bg = "bgtoll"
    case emptyBG
    case emptyHU
    
    var country: String {
        switch self {
        case .hu:
            return "HU"
        case .bg:
            return "BG"
        case .emptyBG, .emptyHU:
            return ""
        }
    }
}

class RoadTaxCollectionViewCell: UICollectionViewCell {
    @IBOutlet private var image: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(for state: TaxState) {
        containerView.alpha = 1
        isUserInteractionEnabled = true
        image.image = nil
        nameLabel.text = ""
        if state == .hu {
            image.image = UIImage(named: "taxaDrumHu")
            nameLabel.text = "hungary".localized
        } else if state == .bg {
            image.image = UIImage(named: "taxaDrumBg")
            nameLabel.text = "bulgary".localized
        } else {
            containerView.alpha = 0.4
            isUserInteractionEnabled = false
            if state == .emptyHU {
                image.image = UIImage(named: "taxaDrumHu")
                nameLabel.text = "hungary".localized
            } else {
                image.image = UIImage(named: "taxaDrumBg")
                nameLabel.text = "bulgary".localized
            }
        }
    }
}
