//
//  RoadTaxBalanceViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 30/06/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

protocol Balance { }

struct HUBalance: Codable, Balance {
    let HU: Int
}

struct BGBalance: Codable, Balance {
    let pendingAmount: Double
    let currency: String
    let minimumAccountBalance: Double
    let tresholdLow: Double
    let balance: Double
    let tresholdHigh: Double
}

class RoadTaxBalanceViewController: BaseLightStatusBarViewController {
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var searchBar: UISearchBar! {
        didSet {
            searchBar.placeholder = "quick_search".localized
        }
    }
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.text = taxState.rawValue.uppercased()
        }
    }
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var balanceView: UIView!
    @IBOutlet private var balanceTitle: UILabel!
    @IBOutlet private var balanceStatus: UILabel!
    @IBOutlet private var pendingAmountLabel: UILabel!
    @IBOutlet private var rechargeButton: UIButton!
    
    private var datasource: [RoadTaxDevice] = []
    private var filteredDatasource: [RoadTaxDevice] = []
    private var balance: Balance!
    var taxState: TaxState!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "RoadTaxBalanceTableViewCell", bundle: nil), forCellReuseIdentifier: "RoadTaxBalanceTableViewCell")
        searchBar.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(getAccountBalance), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
        getAccountBalance()
    }
    
    @objc private func getAccountBalance() {
        showActivityIndicator()
        APIClient().perform(UserService.getUserRoadTaxBalance(taxState: taxState)) { result in
            switch result {
            case .success(let x):
                if let response = try? x.decode(to: HUBalance.self) {
                    let balance = response.body.HU
                    self.balance = response.body
                    DispatchQueue.main.async {
                        self.rechargeButton.isHidden = true
                        self.pendingAmountLabel.isHidden = true
                        switch balance {
                        case 0:
                            self.balanceStatus.text = "normal".localized
                            self.balanceView.backgroundColor = UIColor(red: 39/255, green: 67/255, blue: 151/255, alpha: 1)
                        case 1:
                            self.balanceStatus.text = "low".localized
                            self.balanceView.backgroundColor = UIColor(red: 255/255, green: 170/255, blue: 50/255, alpha: 1)
                        default:
                            self.balanceStatus.text = "empty".localized
                            self.balanceView.backgroundColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
                        }
                    }
                } else if let response = try? x.decode(to: BGBalance.self) {
                    DispatchQueue.main.async {
                        self.rechargeButton.isHidden = false
                        self.balance = response.body
                        let balance = response.body.balance
                        let pendingAmount = response.body.pendingAmount
                        if pendingAmount > 0 {
                            self.pendingAmountLabel.isHidden = false
                            self.pendingAmountLabel.text = "\(pendingAmount) \("pending".localized)"
                        }
                        self.balanceStatus.text = "\(balance) \(response.body.currency)"
                        if balance >= response.body.tresholdLow && balance <= response.body.tresholdHigh {
                            self.balanceView.backgroundColor = UIColor(red: 255/255, green: 170/255, blue: 50/255, alpha: 1)
                        } else if balance > response.body.tresholdHigh {
                            self.balanceView.backgroundColor = UIColor(red: 39/255, green: 67/255, blue: 151/255, alpha: 1)
                        } else {
                            self.balanceView.backgroundColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
                        }
                    }
                }
                self.getDeviceList()
            case .failure(let error):
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.presentAlertController(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }
    
    private func getDeviceList() {
        APIClient().perform(UserService.getUserRoadTaxDeviceList()) { result in
            switch result {
            case .success(let x):
                if let response = try? x.decode(to: [RoadTaxDevice].self) {
                    self.datasource = response.body.filter { $0.roadtaxCountry?.lowercased() == self.taxState.country.lowercased() }
                    self.getUserRoadTaxDeviceLastPosition()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.hideActivityIndicator()
                    self.presentAlertController(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }
    
    private func getUserRoadTaxDeviceLastPosition() {
        let deviceIDsDS1 = datasource.compactMap { vehicle -> Int? in
            if vehicle.deviceDataServer == DSServer.ds1.rawValue {
                return vehicle.id
            }
            return nil
        }
        
        let deviceIDsDS2 = datasource.compactMap { vehicle -> Int? in
            if vehicle.deviceDataServer == DSServer.ds2.rawValue {
                return vehicle.id
            }
            return nil
        }
        
        let deviceIDsDS3 = datasource.compactMap { vehicle -> Int? in
            if vehicle.deviceDataServer == DSServer.ds3.rawValue {
                return vehicle.id
            }
            return nil
        }
        
        let deviceIDsDS4 = datasource.compactMap { vehicle -> Int? in
            if vehicle.deviceDataServer == DSServer.ds4.rawValue {
                return vehicle.id
            }
            return nil
        }
        
        let deviceIDsDS5 = datasource.compactMap { vehicle -> Int? in
            if vehicle.deviceDataServer == DSServer.ds5.rawValue {
                return vehicle.id
            }
            return nil
        }
        
        let deviceIDsDS6 = datasource.compactMap { vehicle -> Int? in
            if vehicle.deviceDataServer == DSServer.ds6.rawValue {
                return vehicle.id
            }
            return nil
        }
        
        let deviceIDsDS7 = datasource.compactMap { vehicle -> Int? in
            if vehicle.deviceDataServer == DSServer.ds7.rawValue {
                return vehicle.id
            }
            return nil
        }
    
        let group = DispatchGroup()
        if !deviceIDsDS1.isEmpty {
            group.enter()
            APIClient().perform(UserService.getUserRoadTaxDeviceLastPosition(deviceId: deviceIDsDS1, dsServer: DSServer.ds1.rawValue)) { result in
                switch result {
                case .success(let x):
                    if let response = try? x.decode(to: [Int: String].self) {
                        response.body.keys.forEach { key in
                            if let index = self.datasource.firstIndex(where: { $0.id == key }) {
                                let date = response.body[key] ?? ""
                                let dateFormatter = DateFormatter()
                                dateFormatter.timeZone = TimeZone(identifier: "UTC")
                                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let dateAsDate = dateFormatter.date(from: date) ?? Date()
                                dateFormatter.timeZone = TimeZone.current
                                dateFormatter.dateFormat = StateController.getDateFormat()
                                let dateAsString = dateFormatter.string(from: dateAsDate)
                                self.datasource[index].date = dateAsString
                            }
                        }
                        group.leave()
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        group.leave()
                        self.presentAlertController(title: "Error", message: error.localizedDescription)
                    }
                }
            }
            if !deviceIDsDS2.isEmpty {
                group.enter()
                APIClient().perform(UserService.getUserRoadTaxDeviceLastPosition(deviceId: deviceIDsDS2, dsServer: DSServer.ds2.rawValue)) { result in
                    switch result {
                    case .success(let x):
                        if let response = try? x.decode(to: [Int: String].self) {
                            response.body.keys.forEach { key in
                                if let index = self.datasource.firstIndex(where: { $0.id == key }) {
                                    let date = response.body[key] ?? ""
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.timeZone = TimeZone(identifier: "UTC")
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let dateAsDate = dateFormatter.date(from: date) ?? Date()
                                    dateFormatter.timeZone = TimeZone.current
                                    dateFormatter.dateFormat = StateController.getDateFormat()
                                    let dateAsString = dateFormatter.string(from: dateAsDate)
                                    self.datasource[index].date = dateAsString
                                }
                            }
                            group.leave()
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            group.leave()
                            self.presentAlertController(title: "Error", message: error.localizedDescription)
                        }
                    }
                }
            }
            if !deviceIDsDS3.isEmpty {
                group.enter()
                APIClient().perform(UserService.getUserRoadTaxDeviceLastPosition(deviceId: deviceIDsDS3, dsServer: DSServer.ds3.rawValue)) { result in
                    switch result {
                    case .success(let x):
                        if let response = try? x.decode(to: [Int: String].self) {
                            response.body.keys.forEach { key in
                                if let index = self.datasource.firstIndex(where: { $0.id == key }) {
                                    let date = response.body[key] ?? ""
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.timeZone = TimeZone(identifier: "UTC")
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let dateAsDate = dateFormatter.date(from: date) ?? Date()
                                    dateFormatter.timeZone = TimeZone.current
                                    dateFormatter.dateFormat = StateController.getDateFormat()
                                    let dateAsString = dateFormatter.string(from: dateAsDate)
                                    self.datasource[index].date = dateAsString
                                }
                            }
                            group.leave()
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            group.leave()
                            self.presentAlertController(title: "Error", message: error.localizedDescription)
                        }
                    }
                }
            }
            if !deviceIDsDS4.isEmpty {
                group.enter()
                APIClient().perform(UserService.getUserRoadTaxDeviceLastPosition(deviceId: deviceIDsDS4, dsServer: DSServer.ds4.rawValue)) { result in
                    switch result {
                    case .success(let x):
                        if let response = try? x.decode(to: [Int: String].self) {
                            response.body.keys.forEach { key in
                                if let index = self.datasource.firstIndex(where: { $0.id == key }) {
                                    let date = response.body[key] ?? ""
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.timeZone = TimeZone(identifier: "UTC")
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let dateAsDate = dateFormatter.date(from: date) ?? Date()
                                    dateFormatter.timeZone = TimeZone.current
                                    dateFormatter.dateFormat = StateController.getDateFormat()
                                    let dateAsString = dateFormatter.string(from: dateAsDate)
                                    self.datasource[index].date = dateAsString
                                }
                            }
                            group.leave()
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            group.leave()
                            self.presentAlertController(title: "Error", message: error.localizedDescription)
                        }
                    }
                }
            }
            if !deviceIDsDS5.isEmpty {
                group.enter()
                APIClient().perform(UserService.getUserRoadTaxDeviceLastPosition(deviceId: deviceIDsDS5, dsServer: DSServer.ds5.rawValue)) { result in
                    switch result {
                    case .success(let x):
                        if let response = try? x.decode(to: [Int: String].self) {
                            response.body.keys.forEach { key in
                                if let index = self.datasource.firstIndex(where: { $0.id == key }) {
                                    let date = response.body[key] ?? ""
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.timeZone = TimeZone(identifier: "UTC")
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let dateAsDate = dateFormatter.date(from: date) ?? Date()
                                    dateFormatter.timeZone = TimeZone.current
                                    dateFormatter.dateFormat = StateController.getDateFormat()
                                    let dateAsString = dateFormatter.string(from: dateAsDate)
                                    self.datasource[index].date = dateAsString
                                }
                            }
                            group.leave()
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            group.leave()
                            self.presentAlertController(title: "Error", message: error.localizedDescription)
                        }
                    }
                }
            }
            if !deviceIDsDS6.isEmpty {
                group.enter()
                APIClient().perform(UserService.getUserRoadTaxDeviceLastPosition(deviceId: deviceIDsDS6, dsServer: DSServer.ds6.rawValue)) { result in
                    switch result {
                    case .success(let x):
                        if let response = try? x.decode(to: [Int: String].self) {
                            response.body.keys.forEach { key in
                                if let index = self.datasource.firstIndex(where: { $0.id == key }) {
                                    let date = response.body[key] ?? ""
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.timeZone = TimeZone(identifier: "UTC")
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let dateAsDate = dateFormatter.date(from: date) ?? Date()
                                    dateFormatter.timeZone = TimeZone.current
                                    dateFormatter.dateFormat = StateController.getDateFormat()
                                    let dateAsString = dateFormatter.string(from: dateAsDate)
                                    self.datasource[index].date = dateAsString
                                }
                            }
                            group.leave()
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            group.leave()
                            self.presentAlertController(title: "Error", message: error.localizedDescription)
                        }
                    }
                }
            }
            if !deviceIDsDS7.isEmpty {
                group.enter()
                APIClient().perform(UserService.getUserRoadTaxDeviceLastPosition(deviceId: deviceIDsDS7, dsServer: DSServer.ds7.rawValue)) { result in
                    switch result {
                    case .success(let x):
                        if let response = try? x.decode(to: [Int: String].self) {
                            response.body.keys.forEach { key in
                                if let index = self.datasource.firstIndex(where: { $0.id == key }) {
                                    let date = response.body[key] ?? ""
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.timeZone = TimeZone(identifier: "UTC")
                                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let dateAsDate = dateFormatter.date(from: date) ?? Date()
                                    dateFormatter.timeZone = TimeZone.current
                                    dateFormatter.dateFormat = StateController.getDateFormat()
                                    let dateAsString = dateFormatter.string(from: dateAsDate)
                                    self.datasource[index].date = dateAsString
                                }
                            }
                            group.leave()
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            group.leave()
                            self.presentAlertController(title: "Error", message: error.localizedDescription)
                        }
                    }
                }
            }
        }
        group.notify(queue: .main) { [weak self] in
            self?.hideActivityIndicator()
            self?.tableView.reloadData()
            self?.setupUI()
        }
    }
    
    private func setupUI() {
        switch taxState {
        case .hu:
            balanceTitle.text = "hugo_account_balance".localized
        case .bg:
            balanceTitle.text = "bg_account_balance".localized
        case .none, .emptyBG, .emptyHU:
            break
        }
    }
    
    private func isFiltering() -> Bool {
        return !searchBarIsEmpty()
    }
    
    private func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchBar.text?.isEmpty ?? true
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapBalance(_ sender: Any) {
        let coordinator = PaymentChooseCurrencyCoordinator(presenter: navigationController ?? UINavigationController())
        coordinator.start()
    }
}

extension RoadTaxBalanceViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredDatasource.count
        }
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RoadTaxBalanceTableViewCell") as! RoadTaxBalanceTableViewCell
        let device = isFiltering() ? filteredDatasource[indexPath.row] : datasource[indexPath.row]
        cell.selectionStyle = .none
        let carPosition = CarPosition(name: device.name ?? "", date: device.date ?? "", status: getStatus(from: device))
        cell.configure(with: carPosition)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let device = datasource[indexPath.row]
        let coordinator = RoadTaxDeviceDetailsCoordinator(presenter: navigationController ?? UINavigationController(), device: device, balance: balance)
        coordinator.start()
    }
    
    private func getStatus(from device: RoadTaxDevice) -> PositionStatus {
        switch device.roadtaxStatus {
        case 0:
            return .notRegistered
        case 1:
            return .active
        case 2:
            return .suspended
        default:
            return .notLinked
        }
    }
}

extension RoadTaxBalanceViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            filteredDatasource = datasource.filter {
                let status = getStatus(from: $0)
                return $0.name?.lowercased().contains(searchText.lowercased()) ?? false || status.localizedDescription.lowercased().contains(searchText.lowercased())
            }
        } else {
            filteredDatasource.removeAll()
        }
        tableView.reloadData()
    }
}
