//
//  FinancialViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 16/09/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import QuickLook
import UIKit
import WebKit

class FinancialViewController: BaseLightStatusBarViewController {
    @IBOutlet private var searchBarTopConstraint: NSLayoutConstraint!
    @IBOutlet private var errorView: UIView!
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "financial".localized
        }
    }
    @IBOutlet private var errorLabel: UILabel!
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var balanceView: UIView!
    @IBOutlet private var payButton: UIButton! {
        didSet {
            payButton.setTitle("pay_all".localized, for: .normal)
        }
    }
    @IBOutlet private var balanceTitle: UILabel! {
        didSet {
            balanceTitle.text = "total_invoices".localized.uppercased()
        }
    }
    @IBOutlet private var searchBar: UISearchBar! {
        didSet {
            searchBar.placeholder = "quick_search".localized
        }
    }
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var paymentAmount: UILabel!
    @IBOutlet private var webviewContainer: UIView!
    @IBOutlet private var webActivityIndicator: UIActivityIndicatorView!

    private var webView: WKWebView!
    private var urlObservation: NSKeyValueObservation?
    
    var isFromFingerprint = false
    private var datasource: [InvoiceModel] = []
    private var filteredDatasource: [InvoiceModel] = []
    private var selectedInvoice: InvoiceModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "FinancialTableViewCell", bundle: nil), forCellReuseIdentifier: "FinancialTableViewCell")
        searchBar.delegate = self
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15

        getUserSuspended()
        getInvoiceList()
    }
    
    private func setupLayout() {
        searchBarTopConstraint.constant = 38
        errorView.isHidden = true
    }

    private func getUserSuspended() {
        if (StateController.currentUser?.isSuspended ?? 0) > 0 {
            self.errorView.isHidden = false
            self.balanceView.backgroundColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
            self.searchBarTopConstraint.constant = 100
        }
    }

    private func getInvoiceList() {
        showActivityIndicator()
        APIClient().perform(DeviceService.getInvoiceList()) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success(let x):
                if let response = try? x.decode(to: BillModel.self) {
                    let invoices = response.body.invoices.map { InvoiceModel(id: $0.id, serial: $0.serial, filename: $0.filename, companyId: $0.companyId, total: $0.total, paidAmount: $0.paidAmount, paymentStatus: $0.paymentStatus, date: $0.date, dateDue: $0.dateDue, number: $0.number, isSelected: false) }
                    strongSelf.datasource = invoices
                    var totalAmount: Double = 0
                    var totalPaid: Double = 0
                    for invoice in strongSelf.datasource where invoice.paymentStatus != 2 {
                        totalAmount += (Double(invoice.total) ?? 0)
                        totalPaid += Double(invoice.paidAmount ?? "0") ?? 0
                    }
                    let total = totalAmount - totalPaid
                    DispatchQueue.main.async {
                        strongSelf.tableView.reloadData()
                        strongSelf.paymentAmount.text = String(format: "%.2f RON", total)
                        strongSelf.balanceView.isHidden = false
                        let currentDate = Date().startOfDay
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd"
                        loop: for invoice in invoices {
                            let dueDate = formatter.date(from: invoice.dateDue) ?? Date()
                            if invoice.paidAmount ?? "0" < invoice.total {
                                if dueDate >= currentDate {
                                    strongSelf.balanceView.backgroundColor = UIColor(red: 255/255, green: 170/255, blue: 50/255, alpha: 1)
                                    break loop
                                } else {
                                    strongSelf.balanceView.backgroundColor = UIColor(red: 39/255, green: 67/255, blue: 151/255, alpha: 1)
                                }
                            } else {
                                strongSelf.balanceView.backgroundColor = UIColor(red: 39/255, green: 67/255, blue: 151/255, alpha: 1)
                            }
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        strongSelf.tableView.reloadData()
                        strongSelf.paymentAmount.text = String(format: "%.2f RON", 0.00)
                        strongSelf.balanceView.isHidden = false
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    strongSelf.presentAlertController(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }

    private func isFiltering() -> Bool {
        return !searchBarIsEmpty()
    }

    private func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchBar.text?.isEmpty ?? true
    }

    @IBAction func didTapBack(_ sender: Any) {
        if isFromFingerprint {
            var menuAlreadyPresent = false
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MainMenuViewController.self) {
                    menuAlreadyPresent = true
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            if !menuAlreadyPresent {
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = CATransitionType.push
                transition.subtype = CATransitionSubtype.fromLeft
                transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
                view.window!.layer.add(transition, forKey: kCATransition)
                let mainMenuCoordinator = MainMenuCoordinator(presenter: navigationController ?? UINavigationController())
                mainMenuCoordinator.start()
            }
        } else {
            navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func didTapPay(_ sender: Any) {
        if payButton.currentTitle == "pay_all".localized {
            var sameCompanyInvoices: [InvoiceModel] = []
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            if isFiltering() {
                let firstDueDate = filteredDatasource.last(where: {
                    if $0.paymentStatus != 2 {
                        return true
                    }
                    return false
                })
                filteredDatasource.forEach {
                    if $0.companyId == firstDueDate?.companyId {
                        sameCompanyInvoices.append($0)
                    }
                }
            } else {
                let firstDueDate = datasource.last(where: {
                    if $0.paymentStatus != 2 {
                        return true
                    }
                    return false
                })
                datasource.forEach {
                    if $0.companyId == firstDueDate?.companyId {
                        sameCompanyInvoices.append($0)
                    }
                }
            }
            tableView.visibleCells.forEach {
                guard let cell = $0 as? FinancialTableViewCell else {
                    return
                }
                if sameCompanyInvoices.contains(cell.bill) && cell.paymentStatus != 2 {
                    let total = (Double(cell.bill.total) ?? 0)
                    let paidAmount = (Double(cell.bill.paidAmount ?? "0") ?? 0)
                    if total - paidAmount > 0.01 {
                        cell.selectCheckView()
                    }
                }
            }
            payInvoices(sameCompanyInvoices)
        } else {
            var selectedInvoices: [InvoiceModel] = []
            if isFiltering() {
                filteredDatasource.forEach {
                    if $0.isSelected ?? false {
                        selectedInvoices.append($0)
                    }
                }
            } else {
                datasource.forEach {
                    if $0.isSelected ?? false {
                        selectedInvoices.append($0)
                    }
                }
            }
            payInvoices(selectedInvoices)
        }
    }

    private func payInvoices(_ invoices: [InvoiceModel]) {
        showActivityIndicator()
        APIClient().perform(DeviceService.initializePayment(invoices: invoices.map { $0.id })) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success(let x):
                if let response = try? x.decode(to: PaymentURLModel.self) {
                    DispatchQueue.main.async {
                        strongSelf.setupWebView()
                        strongSelf.loadPage(url: URL(string: response.body.paymentUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    strongSelf.presentAlertController(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }

    private func setupWebView() {
        DispatchQueue.main.async {
            self.webView = WKWebView()
            self.webView.scrollView.delegate = self
            self.webView.uiDelegate = self
            self.webView.navigationDelegate = self
            self.webviewContainer.addSubview(self.webView)
            self.webView.pin(to: self.webviewContainer)
            UIView.animate(withDuration: 0.5) {
                self.webviewContainer.alpha = 1
                self.webviewContainer.isHidden = false
                self.webActivityIndicator.alpha = 1
                self.webActivityIndicator.isHidden = false
            }
        }
    }

    private func setupWebViewRetry() {
        webActivityIndicator.startAnimating()
        webView.reload()
    }

    private func loadPage(url: URL?) {
        webActivityIndicator.startAnimating()
        DispatchQueue.main.async {
            if !(url?.absoluteString.isEmpty ?? true) {
                self.webView.load(URLRequest(url: url!))
            }
        }
    }

    deinit {
        urlObservation?.invalidate()
    }
}

extension FinancialViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredDatasource.count
        }
        return datasource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FinancialTableViewCell") as! FinancialTableViewCell
        let bill = isFiltering() ? filteredDatasource[indexPath.row] : datasource[indexPath.row]
        var sameCompanyInvoices: [InvoiceModel] = []
        if isFiltering() {
            filteredDatasource.forEach {
                if $0.companyId == selectedInvoice?.companyId {
                    sameCompanyInvoices.append($0)
                }
            }
        } else {
            datasource.forEach {
                if $0.companyId == selectedInvoice?.companyId {
                    sameCompanyInvoices.append($0)
                }
            }
        }
        cell.configure(with: bill, enabled: payButton.currentTitle == "pay_all".localized ? true : sameCompanyInvoices.contains(bill))
        cell.selectionStyle = .none
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 53
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! FinancialTableViewCell
        if cell.bill.paymentStatus != 2 {
            let total = (Double(cell.bill.total) ?? 0)
            let paidAmount = (Double(cell.bill.paidAmount ?? "0") ?? 0)
            if total - paidAmount > 0.01 {
                cell.selectCheckView()
                let invoice = isFiltering() ? filteredDatasource[indexPath.row] : datasource[indexPath.row]
                selectedInvoice = invoice
                tableView.reloadData()
                var containsChecked = false
                if isFiltering() {
                    if let index = filteredDatasource.firstIndex(where: { $0.id == invoice.id }) {
                        filteredDatasource[index].isSelected = cell.selectedCheck
                    }
                    filteredDatasource.forEach {
                        if $0.isSelected ?? false {
                            containsChecked = true
                        }
                    }
                } else {
                    if let index = datasource.firstIndex(where: { $0.id == invoice.id }) {
                        datasource[index].isSelected = cell.selectedCheck
                    }
                    datasource.forEach {
                        if $0.isSelected ?? false {
                            containsChecked = true
                        }
                    }
                }
                if containsChecked {
                    payButton.setTitle("pay_selected".localized, for: .normal)
                } else {
                    payButton.setTitle("pay_all".localized, for: .normal)
                }
            }
        }
    }
}

extension FinancialViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            filteredDatasource = datasource.filter {
                return ($0.serial + $0.number).lowercased().contains(searchText.lowercased().replacingOccurrences(of: " ", with: ""))
            }
        } else {
            filteredDatasource.removeAll()
        }
        tableView.reloadData()
    }
}

extension FinancialViewController: FinancialCellDelegate {
    private func showInvoice(_ url: TemporaryFileURL) {
        DispatchQueue.main.async {
            self.hideActivityIndicator()
            let title = url.contentURL.lastPathComponent
            let coordinator = QLPreviewCoordinator(navigationController: self.navigationController ?? UINavigationController(), title: title, tempURL: url)
            coordinator.start()
        }
    }

    func didDownload(on cell: FinancialTableViewCell) {
        showActivityIndicator()
        APIClient().perform(DeviceService.downloadInvoice(id: cell.bill.id)) { [weak self] result in
            switch result {
            case .success(let x):
                if let response = try? x.decode(to: DownloadInvoiceModel.self) {
                    self?.saveBase64StringToPDF(response.body.data, title: response.body.filename)
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self?.hideActivityIndicator()
                    self?.presentAlertController(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }

    private func saveBase64StringToPDF(_ base64String: String, title: String) {
        guard let convertedData = Data(base64Encoded: base64String)
            else {
                return
        }

        showInvoice(saveDataToTempFolder(convertedData, name: title)!)
    }

    private func saveDataToTempFolder(_ data: Data, name: String) -> TemporaryFileURL? {
        let fileman = FileManager.default
        let tempURL = TemporaryFileURL(endpoint: name, fileManager: fileman)

        do {
            try data.write(to: tempURL.contentURL)
        } catch {
            assertionFailure("failed to copy the file to temp")
            return nil
        }

        return tempURL
    }

    private func makeRetryAlertActions() -> [UIAlertAction] {
        let retryAction = UIAlertAction(title: "retry_button".localized, style: .default) { [weak self] (alert) in
            self?.setupWebViewRetry()
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel) { [weak self] (alert) in
            self?.navigationController?.popViewController(animated: true)
        }
        return [retryAction, cancelAction]
    }
}

extension FinancialViewController: WKUIDelegate, WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        webActivityIndicator.stopAnimating()
        presentAlertController(title: "error_title".localized, message: "page_not_load".localized, actions: makeRetryAlertActions())
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webActivityIndicator.stopAnimating()
    }
}

extension FinancialViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.maximumZoomScale = 1.0
    }
}
