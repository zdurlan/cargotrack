//
//  FinancialTableViewCell.swift
//  
//
//  Created by Alin Zdurlan on 17/09/2020.
//

import UIKit

protocol FinancialCellDelegate: class {
    func didDownload(on cell: FinancialTableViewCell)
}

class FinancialTableViewCell: UITableViewCell {
    @IBOutlet private var checkView: UIView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var valueLabel: UILabel!
    @IBOutlet private var dueValueLabel: UILabel!
    @IBOutlet private var valueLabelTopConstraint: NSLayoutConstraint!

    weak var delegate: FinancialCellDelegate?
    var bill: InvoiceModel!
    var selectedCheck: Bool = false
    var paymentStatus: Int = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func configure(with bill: InvoiceModel, enabled: Bool) {
        self.bill = bill
        paymentStatus = bill.paymentStatus
        checkView.backgroundColor = bill.isSelected ?? false ? UIColor(red: 39/255, green: 67/255, blue: 151/255, alpha: 1) : .white
        nameLabel.text = bill.serial + " " + bill.number
        valueLabel.text = "Total: \(bill.total) RON"
        if !enabled {
            nameLabel.alpha = 0.5
            dateLabel.alpha = 0.5
            valueLabel.alpha = 0.5
            isUserInteractionEnabled = false
        } else {
            nameLabel.alpha = 1
            dateLabel.alpha = 1
            valueLabel.alpha = 1
            isUserInteractionEnabled = true
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let billDateAsDate = formatter.date(from: bill.date) ?? Date()
        formatter.dateFormat = StateController.getDateFormat().components(separatedBy: " ").first
        let dueDateString = formatter.string(from: billDateAsDate)
        dateLabel.text = dueDateString
        valueLabelTopConstraint.constant = 0
        if bill.paymentStatus != 2 {
            let total = (Double(bill.total) ?? 0)
            let paidAmount = (Double(bill.paidAmount ?? "0") ?? 0)
            if total - paidAmount > 0.01 {
                let currentDate = Date().startOfDay
                formatter.dateFormat = "yyyy-MM-dd"
                let dueDateAsDate = formatter.date(from: bill.dateDue) ?? Date()
                if dueDateAsDate < currentDate {
                    nameLabel.textColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
                    dateLabel.textColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
                    valueLabel.textColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
                }
                if total - paidAmount != 0 && total - paidAmount != total {
                    dueValueLabel.isHidden = false
                    dueValueLabel.text = String(format: "\("due".localized): %.2f RON", total - paidAmount)
                    valueLabelTopConstraint.constant = 0
                } else {
                    valueLabelTopConstraint.constant = 12
                    dueValueLabel.isHidden = true
                }
            } else {
                nameLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
                dateLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
                valueLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
                dueValueLabel.isHidden = true
                valueLabelTopConstraint.constant = 12
                nameLabel.alpha = 0.5
                dateLabel.alpha = 0.5
                valueLabel.alpha = 0.5
                checkView.backgroundColor = UIColor(red: 180/255, green: 190/255, blue: 200/255, alpha: 1)
            }
        }
        if bill.paymentStatus == 2 {
            nameLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
            dateLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
            valueLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
            dueValueLabel.isHidden = true
            valueLabelTopConstraint.constant = 12
            nameLabel.alpha = 0.5
            dateLabel.alpha = 0.5
            valueLabel.alpha = 0.5
            checkView.backgroundColor = UIColor(red: 180/255, green: 190/255, blue: 200/255, alpha: 1)
        }
    }

    func selectCheckView() {
        selectedCheck = !selectedCheck
        checkView.backgroundColor = selectedCheck ? UIColor(red: 39/255, green: 67/255, blue: 151/255, alpha: 1) : .white
    }
    
    @IBAction func didTapDownload(_ sender: Any) {
        delegate?.didDownload(on: self)
    }
}
