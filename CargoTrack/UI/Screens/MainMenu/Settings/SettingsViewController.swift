//
//  SettingsViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 03/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class SettingsViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var topView: UIView!
    @IBOutlet private weak var fingerprintSwitch: UISwitch!
    @IBOutlet private weak var fingerprintTextLabel: UILabel!
    @IBOutlet private weak var settingsTitleLabel: UILabel!
    @IBOutlet private weak var preferencesLabel: UILabel!
    @IBOutlet private weak var languageLabel: UILabel!
    @IBOutlet private weak var languageButton: UIButton!

    @IBOutlet private weak var unitOfMeasurementLabel: UILabel!
    @IBOutlet private weak var unitOfMeasurementButton: UIButton!
    @IBOutlet private weak var dateFormatButton: UIButton!
    @IBOutlet private weak var stopDurationTxt: UITextField! {
        didSet {
            stopDurationTxt.delegate = self
            stopDurationTxt.addTarget(self, action: #selector(textFieldDidChange(_:)),
                                         for: UIControl.Event.editingChanged)
            stopDurationTxt.returnKeyType = .done
        }
    }
    @IBOutlet private weak var showCoordinatesSwitch: UISwitch!
    @IBOutlet private weak var stopDurationLabel: UILabel! {
        didSet {
            stopDurationLabel.text = "stop_duration".localized
        }
    }
    @IBOutlet private weak var dateFormatLabel: UILabel! {
        didSet {
            dateFormatLabel.text = "date_format".localized
        }
    }
    @IBOutlet private weak var showCoordinatesOnMapLabel: UILabel! {
        didSet {
            showCoordinatesOnMapLabel.text = "show_coordinates".localized
        }
    }

    var stateController: StateController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpLayout()
        setupUI()
    }
    
    private func setUpLayout() {
        topView.layer.shadowOffset = CGSize(width: 0, height: 1)
        topView.layer.shadowColor = UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1).cgColor
        topView.layer.shadowOpacity = 0.5
        topView.layer.shadowRadius = 15
        topView.layer.masksToBounds = false
        
        if UserDefaults.standard.bool(forKey: "isAuthEnabled") {
            fingerprintSwitch.setOn(true, animated: false)
        }
    
        if UserDefaults.standard.string(forKey: "historyPrecision") == nil {
            UserDefaults.standard.set(100, forKey: "historyPrecision")
            UserDefaults.standard.synchronize()
        }
    }
    
    private func setupUI() {
        if !UIDevice().notXIphone {
            fingerprintTextLabel.text = "faceid_authentication".localized
        } else {
            fingerprintTextLabel.text = "fingerprint_authentication_settings".localized
        }
        
        settingsTitleLabel.text = "settings_title".localized
        preferencesLabel.text = "settings_subtitle_preferences".localized
        languageLabel.text = "language".localized
        languageButton.setTitle(UserDefaults.standard.string(forKey: "language")?.localized ?? "english".localized, for: .normal)

        unitOfMeasurementLabel.text = "unitOfMeasurement".localized
        unitOfMeasurementButton.setTitle(UserDefaults.standard.string(forKey: "unitOfMeasurement")?.localized ?? "metric".localized, for: .normal)
        showCoordinatesSwitch.setOn(StateController.getShowCoordinatesOnFleetScreenValue(), animated: false)
        dateFormatButton.setTitle(StateController.getDateFormat().replacingOccurrences(of: "HH:mm:ss", with: ""), for: .normal)
        stopDurationTxt.text = "\(StateController.getStopDuration())"
        stopDurationLabel.text = "stop_duration".localized
        showCoordinatesOnMapLabel.text = "show_coordinates".localized
        dateFormatLabel.text = "date_format".localized
    }
    
    @IBAction func didTapMenu(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didSwitchFingerprint(_ sender: UISwitch) {
        if sender.isOn {
            AuthenticationManager.checkAuthMethod { (success, authenticationError) in
                if success {
                    DispatchQueue.main.async {
                        UserDefaults.standard.set(true, forKey: "isAuthEnabled")
                        UserDefaults.standard.synchronize()
                    }
                } else {
                    DispatchQueue.main.async {
                        switch authenticationError {
                        case .some(let error):
                            switch error {
                            case .noBiometrics:
                                self.presentAlertController(title: "error_title".localized, message: "device_not_configured_fingerprint".localized)
                                self.fingerprintSwitch.setOn(false, animated: true)
                            case .failure:
                                self.presentAlertController(title: "error_title".localized, message: "fingerprint_failed".localized)
                                self.fingerprintSwitch.setOn(false, animated: true)
                            }
                        case .none:
                            break
                        }
                    }
                }
            }
        } else {
            UserDefaults.standard.set(false, forKey: "isAuthEnabled")
            UserDefaults.standard.synchronize()
        }
    }
    
    @IBAction func didTapEnglish(_ sender: Any) {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let englishAction = UIAlertAction(title: "english".localized, style: .default) {  [weak self] (action) in
            guard let strongSelf = self else {
                return
            }
            UserDefaults.standard.set("english", forKey: "language")
            Bundle.setLanguage(lang: "en")
            strongSelf.setupUI()
        }
        let romanianAction = UIAlertAction(title: "romanian".localized, style: .default) { [weak self] (action) in
            guard let strongSelf = self else {
                return
            }
            UserDefaults.standard.set("romanian", forKey: "language")
            Bundle.setLanguage(lang: "ro")
            strongSelf.setupUI()
        }
        let germanAction = UIAlertAction(title: "german".localized, style: .default) {  [weak self] (action) in
            guard let strongSelf = self else {
                return
            }
            UserDefaults.standard.set("german", forKey: "language")
            Bundle.setLanguage(lang: "de")
            strongSelf.setupUI()
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        actionSheetController.addAction(englishAction)
        actionSheetController.addAction(romanianAction)
        actionSheetController.addAction(germanAction)
        actionSheetController.addAction(cancelAction)
        present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func didTapUnitOfMeasurement(_ sender: Any) {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let metricAction = UIAlertAction(title: "metric".localized, style: .default) { [weak self] _ in
            UserDefaults.standard.set("metric", forKey: "unitOfMeasurement")
            self?.unitOfMeasurementButton.setTitle("metric".localized, for: .normal)
        }
        let imperialAction = UIAlertAction(title: "imperial".localized, style: .default) { [weak self] _ in
            UserDefaults.standard.set("imperial", forKey: "unitOfMeasurement")
            self?.unitOfMeasurementButton.setTitle("imperial".localized, for: .normal)
        }
        UserDefaults.standard.synchronize()
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        actionSheetController.addAction(metricAction)
        actionSheetController.addAction(imperialAction)
        actionSheetController.addAction(cancelAction)
        present(actionSheetController, animated: true, completion: nil)
    }

    @IBAction func didTapDateFormat(_ sender: Any) {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let yyyyMmDdAction = UIAlertAction(title: "YYYY-MM-DD", style: .default) {  [weak self] (action) in
            guard let strongSelf = self else {
                return
            }
            UserDefaults.standard.set("YYYY-MM-dd HH:mm:ss", forKey: "dateFormat")
            strongSelf.dateFormatButton.setTitle("YYYY-MM-DD", for: .normal)
        }
        let yyyyDdMmAction = UIAlertAction(title: "YYYY-DD-MM", style: .default) { [weak self] (action) in
            guard let strongSelf = self else {
                return
            }
            UserDefaults.standard.set("YYYY-dd-MM HH:mm:ss", forKey: "dateFormat")
            strongSelf.dateFormatButton.setTitle("YYYY-DD-MM", for: .normal)
        }
        let mmDdYyyyAction = UIAlertAction(title: "MM-DD-YYYY", style: .default) {  [weak self] (action) in
            guard let strongSelf = self else {
                return
            }
            UserDefaults.standard.set("MM-dd-YYYY HH:mm:ss", forKey: "dateFormat")
            strongSelf.dateFormatButton.setTitle("MM-DD-YYYY", for: .normal)
        }
        let ddMmYyyyAction = UIAlertAction(title: "DD-MM-YYYY", style: .default) {  [weak self] (action) in
            guard let strongSelf = self else {
                return
            }
            UserDefaults.standard.set("dd-MM-YYYY HH:mm:ss", forKey: "dateFormat")
            strongSelf.dateFormatButton.setTitle("DD-MM-YYYY", for: .normal)
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        actionSheetController.addAction(yyyyMmDdAction)
        actionSheetController.addAction(yyyyDdMmAction)
        actionSheetController.addAction(mmDdYyyyAction)
        actionSheetController.addAction(ddMmYyyyAction)
        actionSheetController.addAction(cancelAction)
        present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func didSwitchCoordinates(_ sender: UISwitch) {
        UserDefaults.standard.set(sender.isOn, forKey: "showCoordinatesOnFleet")
    }
}

extension SettingsViewController: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        UserDefaults.standard.set(Int(textField.text!) ?? 0, forKey: "stopDuration")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text!.isEmpty {
            stopDurationTxt.text = "0"
        }
        return true
    }
}
