//
//  MyProfileDetailsTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 30/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class MyProfileDetailsTableViewCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet weak var valueText: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(with index: IndexPath, isEditMode: Bool) {
        valueText.textColor = tintColor
        switch index.row {
        case 0:
            titleLabel.text = "email_address".localized
            valueText.text = StateController.currentUser?.email
            valueText.tag = 0
        case 1:
            titleLabel.text = "phone".localized
            valueText.text = StateController.currentUser?.phone
            valueText.tag = 1
        default:
            break
        }
        valueText.isUserInteractionEnabled = isEditMode
    }
}
