//
//  MyProfileViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 28/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import DTPhotoViewerController
import Kingfisher
import UIKit

class MyProfileViewController: BaseLightStatusBarViewController {
    @IBOutlet private weak var nameTxt: UITextField! {
        didSet {
            nameTxt.text = StateController.currentUser?.name
        }
    }
    @IBOutlet private weak var addPhotoLabel: UILabel! {
        didSet {
            addPhotoLabel.text = "add_photo".localized
        }
    }
    @IBOutlet private weak var profileImage: UIImageView! {
        didSet {
            if let image = StateController.currentUser?.userPhoto {
                let base64Image = image.replacingOccurrences(of: "data:image/jpeg;base64", with: "")
                if let imageData = Data(base64Encoded: base64Image, options: .ignoreUnknownCharacters),
                    imageData.count > 0 {
                    let userImage = UIImage(data: imageData)
                    profileImage.kf.base.image = userImage
                    profileImageConstraints.forEach { $0.constant = 0 }
                    nameTxtTopConstraint.constant = 10
                } else {
                    profileImage.image = UIImage(named: "bigProfileIcon")
                    nameTxtTopConstraint.constant = 30
                }
            }
        }
    }
    @IBOutlet private weak var gradientView: UIView!
    @IBOutlet private weak var companyLabel: UILabel! {
        didSet {
            companyLabel.text = StateController.currentUser?.username
        }
    }
    @IBOutlet private weak var detailsTableView: UITableView!
    @IBOutlet private weak var editButton: UIButton!
    @IBOutlet var profileImageConstraints: [NSLayoutConstraint]!
    @IBOutlet private var nameTxtTopConstraint: NSLayoutConstraint!
    
    var stateController: StateController!
    private var isEditMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailsTableView.register(UINib(nibName: "MyProfileDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "MyProfileDetailsTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor(red: 234/255, green: 235/255, blue: 236/255, alpha: 1).cgColor, UIColor(red: 245/255, green: 250/255, blue: 255/255, alpha: 1).cgColor]
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: gradientView.bounds.size.width, height: gradientView.bounds.size.height)
        
        self.gradientView.layer.insertSublayer(gradient, at: 0)
    }
    
    @IBAction func didTapMenu(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func didTapEdit(_ sender: Any) {
        if editButton.currentImage?.pngData() == UIImage(named: "editButton")?.pngData() {
            editButton.setImage(nil, for: .normal)
            editButton.setTitle("save_button".localized, for: .normal)
            isEditMode = true
        } else {
            editButton.setImage(UIImage(named: "editButton"), for: .normal)
            editButton.setTitle(nil, for: .normal)
            isEditMode = false
            saveProfileData()
        }
        addPhotoLabel.isHidden = !isEditMode
        if isEditMode {
            if profileImage.image?.pngData() == UIImage(named: "bigProfileIcon")?.pngData() {
                addPhotoLabel.text = "add_photo".localized
            } else {
                addPhotoLabel.isHidden = true
            }
        }
        nameTxt.isUserInteractionEnabled = isEditMode ? true : false
        detailsTableView.reloadData()
    }
    
    private func saveProfileData() {
        let emailCell = detailsTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! MyProfileDetailsTableViewCell
        let phoneCell = detailsTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! MyProfileDetailsTableViewCell
        let email = emailCell.valueText.text ?? ""
        let phone = phoneCell.valueText.text ?? ""
        if String.isValidEmail(email) {
            if phone.count == 10 {
                var base64StringToSend = ""
                var hasChangedPhoto = true
                if profileImage.image?.pngData() == UIImage(named: "bigProfileIcon")?.pngData() {
                    hasChangedPhoto = false
                }
                if let image = profileImage.image, hasChangedPhoto {
                    let imageData = image.jpegData(compressionQuality: 1)!
                    base64StringToSend = "data:image/jpeg;base64,\(imageData.base64EncodedString(options: .lineLength64Characters))"
                }
                showActivityIndicator()
                APIClient().perform(UserService.updateUserRequest(name: nameTxt.text!, email: email, phone: phone, userPhoto: hasChangedPhoto ? base64StringToSend : nil)) { result in
                    self.hideActivityIndicator()
                    switch result {
                    case .success(let response):
                        if let responsex = try? response.decode(to: SuccessJSON.self) {
                            if responsex.body.message == "success" {
                                DispatchQueue.main.async {
                                    StateController.currentUser?.name = self.nameTxt.text!
                                    StateController.currentUser?.email = email
                                    StateController.currentUser?.phone = phone
                                    StateController.currentUser?.userPhoto = base64StringToSend
                                    
                                    if let encoded = try? JSONEncoder().encode(StateController.currentUser) {
                                        UserDefaults.standard.set(encoded, forKey: "user")
                                        UserDefaults.standard.synchronize()
                                    }
                                }
                            } else {
                                self.presentAlertController(title: "", message: "add_photo_error".localized)
                            }
                        } else {
                            self.presentAlertController(title: "", message: "error_message".localized)
                        }
                    case .failure:
                        self.presentAlertController(title: "", message: "error_message".localized)
                    }
                }
            } else {
                 presentAlertController(title: "", message: "invalid_phone".localized)
            }
        } else {
            presentAlertController(title: "", message: "invalid_email".localized)
        }
    }
    
    @IBAction func didTapPhoto(_ sender: Any) {
        if isEditMode {
            let cameraAction = UIAlertAction(title: "Camera", style: .default) { _ in
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType =  .camera
                self.present(myPickerController, animated: true, completion: nil)
            }
            let photoLibraryAction = UIAlertAction(title: "photo_library".localized, style: .default) { _ in
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType =  .photoLibrary
                self.present(myPickerController, animated: true, completion: nil)
            }
            let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
            let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            actionSheetController.addAction(cameraAction)
            actionSheetController.addAction(photoLibraryAction)
            actionSheetController.addAction(cancelAction)
            present(actionSheetController, animated: true, completion: nil)
        } else {
            let viewController = DTPhotoViewerController(referencedView: profileImage, image: profileImage.image)
            self.present(viewController, animated: true, completion: nil)
        }
    }
}

extension MyProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}

extension MyProfileViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileDetailsTableViewCell", for: indexPath) as! MyProfileDetailsTableViewCell
        cell.accessoryType = isEditMode ? .disclosureIndicator : .none
        cell.selectionStyle = .none
        cell.configure(with: indexPath, isEditMode: isEditMode)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = ProfileHeaderView.loadFromNib() as! ProfileHeaderView
        view.backgroundColor = UIColor(red: 240/255, green: 245/255, blue: 250/255, alpha: 1)
        view.configure()
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
}

extension MyProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        profileImage.kf.base.image = image
        profileImageConstraints.forEach { $0.constant = 0 }
        nameTxtTopConstraint.constant = 10
        dismiss(animated: true)
    }
}
