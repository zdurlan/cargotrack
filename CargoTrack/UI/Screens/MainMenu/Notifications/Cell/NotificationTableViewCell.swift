//
//  NotificationTableViewCell.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 03/06/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var summaryLabel: UILabel! {
        didSet {
            summaryLabel.textColor = UIColor(red: 115/255, green: 125/255, blue: 135/255, alpha: 1)
        }
    }
    @IBOutlet private var notificationIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with notification: NotificationPayload) {
        dateLabel.text = notification.createdAt
        summaryLabel.text = notification.summary
        summaryLabel.font = notification.dateRead == nil ? UIFont.systemFont(ofSize: 16, weight: .bold) : UIFont.systemFont(ofSize: 16)
        switch notification.userNotificationTypeId {
        case 1:
            notificationIcon.image = UIImage(named: "financialNotificationIcon")
        case 2:
            notificationIcon.image = UIImage(named: "systemNotificationIcon")
        case 3:
            notificationIcon.image = UIImage(named: "eventsNotificationIcon")
        case 4:
            notificationIcon.image = UIImage(named: "reportsNotificationIcon")
        case 5:
            notificationIcon.image = UIImage(named: "marketingNotificationIcon")
        default:
            return
        }
        switch notification.priority {
        case 1:
            notificationIcon.tintColor = UIColor(red: 0/255, green: 200/255, blue: 125/255, alpha: 1)
        case 2:
            notificationIcon.tintColor = UIColor(red: 255/255, green: 175/255, blue: 50/255, alpha: 1)
        case 3:
            notificationIcon.tintColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
        case 4:
            notificationIcon.tintColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
        default:
            return
        }
    }
}
