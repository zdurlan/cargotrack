//
//  NotificationDetailsViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 18/09/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit
import WebKit

class NotificationDetailsViewController: BaseLightStatusBarViewController, WKNavigationDelegate {
    var notification: NotificationPayload!
    
    @IBOutlet private var notificationDate: UILabel!
    @IBOutlet private var notificationTitle: UILabel!
    @IBOutlet private var notificationIcon: UIImageView!
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "notifications_title".localized
        }
    }
    @IBOutlet private var notificationView: UIView!
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var notificationMessage: UILabel!

    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    
    private var webView: WKWebView!
    private var urlObservation: NSKeyValueObservation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotificationView()
        if isHtml(notification.preview ?? notification.message) {
            setupWebView()
        } else {
            containerView.isHidden = true
            notificationMessage.isHidden = false
        }
    }
    
    private func isHtml(_ value: String) -> Bool {
        let validateTest = NSPredicate(format:"SELF MATCHES %@", "<[a-z][\\s\\S]*>")
        return validateTest.evaluate(with: value)
    }
    
    private func setupNotificationView() {
        notificationTitle.text = notification.summary
        notificationIcon.image = UIImage(named: "notificationWheelIcon")
        notificationDate.text = notification.createdAt
        notificationMessage.text = notification.message
        switch notification.userNotificationTypeId {
        case 1:
            notificationIcon.image = UIImage(named: "financialNotificationIcon")
        case 2:
            notificationIcon.image = UIImage(named: "systemNotificationIcon")
        case 3:
            notificationIcon.image = UIImage(named: "eventsNotificationIcon")
        case 4:
            notificationIcon.image = UIImage(named: "reportsNotificationIcon")
        case 5:
            notificationIcon.image = UIImage(named: "marketingNotificationIcon")
        default:
            return
        }
        switch notification.priority {
        case 1:
            notificationIcon.tintColor = UIColor(red: 0/255, green: 200/255, blue: 125/255, alpha: 1)
        case 2:
            notificationIcon.tintColor = UIColor(red: 255/255, green: 175/255, blue: 50/255, alpha: 1)
        case 3:
            notificationIcon.tintColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
        case 4:
            notificationIcon.tintColor = UIColor(red: 255/255, green: 25/255, blue: 75/255, alpha: 1)
        default:
            return
        }
    }
    
    private func setupWebView() {
        webView = WKWebView()
        containerView.addSubview(webView)
        webView.pin(to: containerView, insets: UIEdgeInsets(top: 24, left: 8, bottom: 16, right: -8))
        let scale = "<meta name=\"viewport\"content=\"initial-scale=1.0\"/>"
        webView.loadHTMLString(scale+notification.message, baseURL: nil)
        webView.navigationDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
    }

    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else {
            decisionHandler(.allow)
            return
        }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
        decisionHandler(.allow)
    }
}

extension URLRequest {
    var isHttpLink: Bool {
        return self.url?.scheme?.contains("http") ?? false
    }
}
