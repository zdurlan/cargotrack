//
//  NotificationsViewController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 03/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Presentr
import UIKit
import WebKit

enum NotificationFilterType {
    case unread
    case read
    case none
}

class NotificationsViewController: BaseLightStatusBarViewController {
    @IBOutlet private var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var notificationsTitleLabel: UILabel! {
        didSet {
            notificationsTitleLabel.text = "notifications_title".localized
        }
    }
    @IBOutlet private var menuButton: UIButton!
    @IBOutlet private var centerConstraintNavTitle: NSLayoutConstraint!
    @IBOutlet private var searchBar: UISearchBar! {
        didSet {
            searchBar.placeholder = "quick_search".localized
        }
    }

    private var isFilteredButtonActive = false
    private var notificationsList: [NotificationPayload] = []
    private var filteredDatasource: [NotificationPayload] = []
    private var filterType: NotificationFilterType = .none

    var isLoading = false
    var isFromMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        getNotificationsList()
        if !isFromMenu {
            menuButton.setImage(UIImage(named: "backButton"), for: .normal)
        }
        searchBar.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        navBarHeightConstraint.constant = UIDevice().notXIphone ? 64 : 88
        centerConstraintNavTitle.constant = UIDevice().notXIphone ? 10 : 15
    }
    
    private func getNotificationsList() {
        showActivityIndicator()
        isLoading = true
        APIClient().perform(NotificationsService.getNotificationsList(skip: notificationsList.count, limit: 50)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success(let response):
                guard let json = try? response.decode(to: [NotificationPayload].self) else {
                    APIClient().perform(SlackService.sendMessage("Couldn't get notifications for \(StateController.currentUser?.name ?? "")")) { _ in }
                    debugPrint("error decoding JSON")
                    return
                }
                strongSelf.notificationsList.append(contentsOf: json.body)
                DispatchQueue.main.async {
                    strongSelf.tableView.reloadData()
                    strongSelf.isLoading = false
                }
            case .failure:
                APIClient().perform(SlackService.sendMessage("Couldn't get notifications for \(StateController.currentUser?.name ?? "")")) { _ in }
            }
        }
    }
    
    private func readNotification(id: Int) {
        APIClient().perform(NotificationsService.readNotification(notificationId: id)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success:
                if let row = strongSelf.notificationsList.firstIndex(where: {$0.id == id}) {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-mm-dd HH:mm:ss"
                    let date = dateFormatter.string(from: Date())
                    strongSelf.notificationsList[row].dateRead = date
                    DispatchQueue.main.async {
                        strongSelf.tableView.reloadData()
                    }
                }
            case .failure:
                APIClient().perform(SlackService.sendMessage("Couldn't set notification read status for notification id \(id)")) { _ in }
            }
        }
    }
    
    private func deleteNotification(id: Int) {
        APIClient().perform(NotificationsService.deleteNotification(notificationId: id)) { [weak self] result in
            guard let strongSelf = self else {
                return
            }
            switch result {
            case .success:
                if let row = strongSelf.notificationsList.firstIndex(where: {$0.id == id}) {
                    strongSelf.notificationsList.remove(at: row)
                    DispatchQueue.main.async {
                        strongSelf.tableView.reloadData()
                    }
                }
            case .failure:
                strongSelf.presentAlertController(title: "error_title".localized, message: "could_not_delete_notification".localized)
                APIClient().perform(SlackService.sendMessage("Couldn't delete notification for notification id \(id)")) { _ in }
            }
        }
    }
    
    private func isFiltering() -> Bool {
        return !searchBarIsEmpty() || isFilteredButtonActive
    }
    
    private func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchBar.text?.isEmpty ?? true
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    private func showNotificationDetails(_ notification: NotificationPayload) {
        let detailsCoordinator = NotificationDetailsCoordinator(presenter: navigationController ?? UINavigationController(), notification: notification)
        detailsCoordinator.start()
    }

    @IBAction func didTapFilter(_ sender: Any) {
        let allNotificationsAction = UIAlertAction(title: "all_notifications".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.searchBar.text = ""
            strongSelf.isFilteredButtonActive = false
            strongSelf.filterType = .none
            strongSelf.tableView.reloadData()
        }
        let unreadNotificationsAction = UIAlertAction(title: "unread_notifications".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.filteredDatasource.removeAll()
            strongSelf.notificationsList.forEach {
                if $0.dateRead == nil {
                    strongSelf.filteredDatasource.append($0)
                }
            }
            strongSelf.searchBar.text = ""
            strongSelf.isFilteredButtonActive = true
            strongSelf.filterType = .unread
            strongSelf.tableView.reloadData()
        }
        let readNotificationsAction = UIAlertAction(title: "read_notifications".localized, style: .default) { [weak self] action in
            guard let strongSelf = self else {
                return
            }
            strongSelf.filteredDatasource.removeAll()
            strongSelf.notificationsList.forEach {
                if $0.dateRead != nil {
                    strongSelf.filteredDatasource.append($0)
                }
            }
            strongSelf.searchBar.text = ""
            strongSelf.isFilteredButtonActive = true
            strongSelf.filterType = .read
            strongSelf.tableView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "cancel_button".localized, style: .cancel, handler: nil)
        presentAlertController(title: nil, message: nil, actions: [allNotificationsAction, readNotificationsAction, unreadNotificationsAction, cancelAction], style: .actionSheet)
    }
}

extension NotificationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedNotification = isFiltering() ? filteredDatasource[indexPath.row] : notificationsList[indexPath.row]
        readNotification(id: selectedNotification.id)
        showNotificationDetails(selectedNotification)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let selectedNotification = notificationsList[indexPath.row]
        if (editingStyle == .delete) {
            deleteNotification(id: selectedNotification.id)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height

        if maximumOffset - currentOffset <= 40 && isLoading == false {
            getNotificationsList()
        }
    }
}

extension NotificationsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredDatasource.count
        }
        return notificationsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        let notification = isFiltering() ? filteredDatasource[indexPath.row] : notificationsList[indexPath.row]
        cell.selectionStyle = .none
        cell.configure(with: notification)
        return cell
    }
}

extension NotificationsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            if !isFilteredButtonActive {
                filteredDatasource = notificationsList.filter {
                    return $0.summary.lowercased().contains(searchText.lowercased())
                }
            } else {
                filteredDatasource = filteredDatasource.filter {
                    return $0.summary.lowercased().contains(searchText.lowercased())
                }
            }
        } else {
            filteredDatasource.removeAll()
            notificationsList.forEach {
                if $0.dateRead != nil, filterType == .read {
                    filteredDatasource.append($0)
                } else if $0.dateRead == nil, filterType == .unread {
                    filteredDatasource.append($0)
                }
            }
        }
        tableView.reloadData()
    }
}
