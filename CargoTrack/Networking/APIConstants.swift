//
//  APIConstants.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 26/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//
import Foundation
enum DSServer: String {
    case ds1 = "ds1.cargotrack.ro"
    case ds2 = "ds2.cargotrack.ro"
    case ds3 = "ds3.cargotrack.ro"
    case ds4 = "ds4.cargotrack.ro"
    case ds5 = "ds5.cargotrack.ro"
    case ds6 = "ds6.cargotrack.ro"
    case ds7 = "ds7.cargotrack.ro"
    case ds8 = "ds8.cargotrack.ro"
    case ds9 = "ds9.cargotrack.ro"
}

struct ApiConstants {
    static var BaseURLString = "https://ds.cargotrack.ro/" + ApiConstants.BaseURLVersion //CHANGE when release
    static let BaseURLVersion = "cargoapp/6.1/"
    
    struct User {
        static let LoginPath = "user/login"
        static let TryUsPath = "contact"
        static let MobileWrapperPath = "user/mobile/wrapper"
        static let SessionExpiredPath = "user/verifytoken"
        static let ForgotPasswordPath = "user/passwordrecovery/sendemail"
        static let UpdateUserPath = "user/update"
        static let UserRightsPath = "user/getangularmoduleoptions/map"
        static let UserRoadTaxPath = "user/getangularmoduleoptions/roadtax"
        static let UserSuspended = "user/manage/issuspended/"
        static let UserHUBalance = "roadtax/balance"
        static let UserBGBalance = "roadtax/bg/balance/info"
        static let UserRoadTaxDeviceList = "roadtax/device/list"
        static let UserRoadTaxDeviceLastPosition = "roadtax/device/lasttransmission"
        static let UserRoadTaxDeviceLatest = "roadtax/device/latest"
        static let UserRoadTaxDeviceEditHU = "roadtax/hu/device/edit"
        static let UserRoadTaxDeviceEditBG = "roadtax/bg/device/edit"
    }
    
    struct Devices {
        static let DeviceListPath = "device/list"
        static let DevicePositionsPath = "device/latestposition"
        static let ReverseGeocodingMultiplePath = "geocode/reverse/multi?language=ro"
        static let ReverseGeocodingSinglePath = "geocode/reverse/"
        static let StatusSincePath = "device/statussince"
        static let HistoryPath = "device/history"
        // static let SaveDevicePositionTransmissionPath = "savedevicepositiontransmissionjob"
        static let SaveDevicePositionTransmissionPath = "devicepositiontransmissionjob/savejob"
        // static let GetActiveDevicePositionTransmissionPath = "getactivedevicepositiontransmissionjob/"
        static let GetActiveDevicePositionTransmissionPath = "devicepositiontransmissionjob/getactive/"
        // static let DeactivateDevicePositionTransmissionPath = "desactivatedevicepositiontransmissionjob"
        static let DeactivateDevicePositionTransmissionPath = "devicepositiontransmissionjob/desactivate"
        static let DeviceCommandStatus = "device/command/status"
        // static let SendCommand = "sendcommand"
        static let SendCommand = "device/command/sendcommand"
        static let UpdateFuelConsumptionPath = "device/update/fuelconsumption"
        static let GetRefuelingsPath = "report/data/refuelings"
        static let GetHistoryPath = "report/data/history"
        static let GetConversionQuotePath = "roadtax/bg/recharge/quote/"
        static let PayRoadTaxPath = "roadtax/bg/recharge/init"
        static let InvoiceList = "accounting/invoices"
        static let DownloadInvoice = "accounting/downloadinvoice/"
        static let InitPayment = "accounting/payment/init"
    }
    
    struct Notifications {
        static let NotificationsListPath = "notification/list/"
        static let NotificationsReadPath = "notification/read/"
        static let NotificationsDeletePath = "notification/delete/"
        static let UnreadNotificationsCountPath = "notification/total/unread"
    }
}
