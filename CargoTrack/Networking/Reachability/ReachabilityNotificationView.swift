//
//  ReachabilityNotificationView.swift
//  Life
//
//  Created by Andrei-Liviu Zahariea on 11/04/2019.
//  Copyright © 2019 CGM. All rights reserved.
//

import Foundation
import UIKit

class ReachabilityNotificationView: NibLoadedView {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var mainView: UIView!

    static func reachabilityNotificationView(isReachable: Bool) -> ReachabilityNotificationView {
        let notificationView:ReachabilityNotificationView = ReachabilityNotificationView()
        notificationView.setLayout(isReachable: isReachable)
        return notificationView
    }

    func setLayout(isReachable: Bool) {
        if isReachable {
            titleLabel.text = "Internet connection found"
            mainView.backgroundColor = UIColor.green
        } else {
            titleLabel.text = "No internet connection"
            mainView.backgroundColor = UIColor.black
        }
    }
}
