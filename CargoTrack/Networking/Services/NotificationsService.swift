//
//  NotificationsService.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 08/08/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

class NotificationsService {
    static func getNotificationsList(skip: Int, limit: Int) -> APIRequest {
        let getNotificationsListRequest = APIRequest(method: .get, path: nil, baseUrl: URL(string: "https://\(StateController.currentUser?.clientDataServer ?? "")/\(ApiConstants.BaseURLVersion)\(ApiConstants.Notifications.NotificationsListPath)\(skip)?limit=\(limit)")!)
        getNotificationsListRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        return getNotificationsListRequest
    }
    
    static func readNotification(notificationId: Int) -> APIRequest {
        let readNotificationRequest = APIRequest(method: .put, path: nil, baseUrl: URL(string: "https://\(StateController.currentUser?.clientDataServer ?? "")/\(ApiConstants.BaseURLVersion)\(ApiConstants.Notifications.NotificationsReadPath)\(notificationId)")!)
        readNotificationRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        return readNotificationRequest
    }
    
    static func deleteNotification(notificationId: Int) -> APIRequest {
        let deleteNotificationRequest = APIRequest(method: .delete, path: nil, baseUrl: URL(string: "https://\(StateController.currentUser?.clientDataServer ?? "")/\(ApiConstants.BaseURLVersion)\(ApiConstants.Notifications.NotificationsDeletePath)\(notificationId)")!)
        deleteNotificationRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        return deleteNotificationRequest
    }
    
    static func getUnreadNotificationsCount() -> APIRequest {
        let getUnreadNotificationsCountRequest = APIRequest(method: .get, path: nil, baseUrl: URL(string: "https://\(StateController.currentUser?.clientDataServer ?? "")/\(ApiConstants.BaseURLVersion)\(ApiConstants.Notifications.UnreadNotificationsCountPath)")!)
        getUnreadNotificationsCountRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json"), HTTPHeader(field: "Accept", value: "application/json")]
        return getUnreadNotificationsCountRequest
    }
}
