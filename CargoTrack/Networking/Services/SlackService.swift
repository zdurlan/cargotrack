//
//  SlackClient.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 02/08/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

class SlackService {
    static let baseURL = "https://hooks.slack.com/services/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"
    
    static func sendMessage(_ message: String) -> APIRequest {
        let slackMessageRequest = APIRequest(method: .post, path: nil, baseUrl: URL(string: baseURL)!)
        slackMessageRequest.headers = [HTTPHeader(field: "Content-type", value: "application/json")]
        let jsonDictionary = ["text": message]
        var jsonData = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
        } catch {
            print(error.localizedDescription)
        }
        slackMessageRequest.body = jsonData
        return slackMessageRequest
    }
}
