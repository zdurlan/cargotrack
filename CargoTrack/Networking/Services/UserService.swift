//
//  AuthService.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 26/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

class UserService {
    static let shared = UserService()
    var accessToken: String?
    
    static func createLoginRequest(username: String, password: String, rememberMe: Bool) -> APIRequest {
        let loginRequest = APIRequest(method: .post, path: ApiConstants.User.LoginPath)
        var appStoreVersion = ""
        do {
            appStoreVersion = try getAppStoreVersion()
        } catch {
            appStoreVersion = ""
        }
        loginRequest.queryItems = [
            URLQueryItem(name: "username", value: username),
            URLQueryItem(name: "password", value: password),
            URLQueryItem(name: "rememberme", value: String(rememberMe)),
            URLQueryItem(name: "version", value: appStoreVersion),
            URLQueryItem(name: "app", value: "ios")
        ]
        return loginRequest
    }

    static func createTryUsRequest(name: String, email: String, phone: String, company: String, message: String) -> APIRequest {
        let tryUsRequest = APIRequest(method: .post, path: ApiConstants.User.TryUsPath)
        tryUsRequest.queryItems = [
            URLQueryItem(name: "name", value: name),
            URLQueryItem(name: "email", value: email),
            URLQueryItem(name: "phone", value: phone),
            URLQueryItem(name: "company", value: company),
            URLQueryItem(name: "message", value: message)
        ]
        return tryUsRequest
    }
    
    static func createSessionExpiredRequest() -> APIRequest {
        let sessionExpiredRequest = APIRequest(method: .get, path: ApiConstants.User.SessionExpiredPath)
        sessionExpiredRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? "")]
        return sessionExpiredRequest
    }
    
    static func createMobileAsWrapperRequest() -> APIRequest {
        let mobileWrapperRequest = APIRequest(method: .get, path: ApiConstants.User.MobileWrapperPath)
        mobileWrapperRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? "")]
        return mobileWrapperRequest
    }
    
    static func forgotPasswordRequest(email: String) -> APIRequest {
        let forgotPasswordRequest = APIRequest(method: .post, path: ApiConstants.User.ForgotPasswordPath)
        forgotPasswordRequest.queryItems = [
            URLQueryItem(name: "loginData", value: email)
        ]
        return forgotPasswordRequest
    }
    
    static func getUserRightsRequest() -> APIRequest {
        let userRightsRequest = APIRequest(method: .get, path: ApiConstants.User.UserRightsPath)
        userRightsRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? "")]
        return userRightsRequest
    }
    
    static func getUserRoadTaxRequest() -> APIRequest {
        let userRightsRequest = APIRequest(method: .get, path: ApiConstants.User.UserRoadTaxPath)
        userRightsRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? "")]
        return userRightsRequest
    }
    
    static func updateUserRequest(name: String?, email: String?, phone: String?, userPhoto: String?) -> APIRequest {
        let updateUserRequest = APIRequest(method: .post, path: ApiConstants.User.UpdateUserPath)
        updateUserRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? ""),
        HTTPHeader(field: "Content-type", value: "application/json")]
        var jsonDictionary = [
            "name": name,
            "email": email,
            "phone": phone
        ]
        if userPhoto != nil {
            jsonDictionary["user_photo"] = userPhoto
        }
        var jsonData = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
        } catch {
            print(error.localizedDescription)
        }
        updateUserRequest.body = jsonData
        return updateUserRequest
    }
    
    static func getUserSuspended(userId: Int) -> APIRequest {
        let baseUrl = URL(string: "https://\(StateController.currentUser?.clientDataServer ?? "")/\(ApiConstants.BaseURLVersion)\(ApiConstants.User.UserSuspended)\(userId)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!
        let getUserSuspendedRequest = APIRequest(method: .get, path: nil, baseUrl: baseUrl)
        getUserSuspendedRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        return getUserSuspendedRequest
    }
    
    static func getUserRoadTaxBalance(taxState: TaxState) -> APIRequest {
        if taxState == .hu {
            let baseUrl = URL(string: "https://\(StateController.currentUser?.clientDataServer ?? "")/\(ApiConstants.BaseURLVersion)\(ApiConstants.User.UserHUBalance)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!
            let getUserRoadTaxBalanceRequest = APIRequest(method: .get, path: nil, baseUrl: baseUrl)
            getUserRoadTaxBalanceRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
            return getUserRoadTaxBalanceRequest
        } else {
            let getUserRoadTaxBalanceRequest = APIRequest(method: .get, path: ApiConstants.User.UserBGBalance)
            getUserRoadTaxBalanceRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
            return getUserRoadTaxBalanceRequest
        }
    }
    
    static func getUserRoadTaxDeviceList() -> APIRequest {
        let getUserRoadTaxBalanceRequest = APIRequest(method: .get, path: ApiConstants.User.UserRoadTaxDeviceList)
        getUserRoadTaxBalanceRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        return getUserRoadTaxBalanceRequest
    }
    
    static func getUserRoadTaxDeviceLastPosition(deviceId: [Int], dsServer: String) -> APIRequest {
        let baseUrl = URL(string: "https://\(dsServer)/\(ApiConstants.BaseURLVersion)\(ApiConstants.User.UserRoadTaxDeviceLastPosition)")!
        let getUserRoadTaxDeviceLastPositionRequest = APIRequest(method: .post, path: nil, baseUrl: baseUrl)
        let jsonDictionary = [
            "device_ids": deviceId
        ]
        var jsonData = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
        } catch {
            print(error.localizedDescription)
        }
        getUserRoadTaxDeviceLastPositionRequest.body = jsonData
        getUserRoadTaxDeviceLastPositionRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        return getUserRoadTaxDeviceLastPositionRequest
    }
    
    static func getUserRoadTaxDeviceLatestDate(deviceId: [Int], dsServer: String, country: String) -> APIRequest {
        let baseUrl = URL(string: "https://\(dsServer)/\(ApiConstants.BaseURLVersion)\(ApiConstants.User.UserRoadTaxDeviceLatest)")!
        let getUserRoadTaxDeviceLatestRequest = APIRequest(method: .post, path: nil, baseUrl: baseUrl)
        let jsonDictionary = [
            "device_ids": deviceId,
            "country": country
        ] as [String : Any]
        var jsonData = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
        } catch {
            print(error.localizedDescription)
        }
        getUserRoadTaxDeviceLatestRequest.body = jsonData
        getUserRoadTaxDeviceLatestRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json"), HTTPHeader(field: "Accept", value: "application/json")]
        return getUserRoadTaxDeviceLatestRequest
    }
    
    static func saveUserRoadTaxDetailsBG(deviceId: Int, phone: String, tractorAxle: Int, trailerAxle: Int) -> APIRequest {
        let editBGRequest = APIRequest(method: .post, path: ApiConstants.User.UserRoadTaxDeviceEditBG)
        editBGRequest.queryItems = [
            URLQueryItem(name: "device_id", value: String(deviceId)),
            URLQueryItem(name: "tractor_axles", value: String(tractorAxle)),
            URLQueryItem(name: "trailer_axles", value: String(trailerAxle)),
            URLQueryItem(name: "responsible_phone", value: phone)
        ]
        editBGRequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json"), HTTPHeader(field: "Accept", value: "application/json")]
        return editBGRequest
    }
    
    static func saveUserRoadTaxDetailsHU(deviceId: Int, phone: String) -> APIRequest {
        let editHURequest = APIRequest(method: .post, path: ApiConstants.User.UserRoadTaxDeviceEditHU)
        editHURequest.queryItems = [
            URLQueryItem(name: "device_id", value: String(deviceId)),
            URLQueryItem(name: "responsible_phone", value: phone)
        ]
        editHURequest.headers = [HTTPHeader(field: "Authorization", value: shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json"), HTTPHeader(field: "Accept", value: "application/json")]
        return editHURequest
    }
}

extension UserService {
    static private func getAppStoreVersion() throws -> String {
        guard let info = Bundle.main.infoDictionary,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                return ""
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            return ""
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            return version
        }
        return ""
    }
}
