//
//  DeviceService.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 05/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

class DeviceService {
    static func createDeviceListRequest() -> APIRequest {
        let deviceListRequest = APIRequest(method: .get, path: ApiConstants.Devices.DeviceListPath)
        deviceListRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        return deviceListRequest
    }
    
    static func createDevicePositionsRequest(deviceId: [Int], dsServer: String, dateTime: String? = nil) -> APIRequest {
        let baseUrl = dateTime != nil ? URL(string: "https://\(dsServer)/\(ApiConstants.BaseURLVersion)\(ApiConstants.Devices.DevicePositionsPath)/\(dateTime!)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! : URL(string: "https://\(dsServer)/\(ApiConstants.BaseURLVersion)")!
        let devicePositionsRequest = APIRequest(method: .post, path: dateTime != nil ? nil : ApiConstants.Devices.DevicePositionsPath, baseUrl: baseUrl)
        devicePositionsRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        var jsonData = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: deviceId, options: .prettyPrinted)
        } catch {
            print(error.localizedDescription)
        }
        
        devicePositionsRequest.body = jsonData
        return devicePositionsRequest
    }
    
    static func createReverseGeocodingMultipleRequest(latLongDictionary: [String: Double]) -> APIRequest {
        let reverseGeocodeRequest = APIRequest(method: .post, path: ApiConstants.Devices.ReverseGeocodingMultiplePath)
        reverseGeocodeRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        var jsonData = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: latLongDictionary, options: .prettyPrinted)
        } catch {
            print(error.localizedDescription)
        }
        
        reverseGeocodeRequest.body = jsonData
        return reverseGeocodeRequest
    }
    
    static func createReverseGeocodingSingleRequest(latitude: Double, longitude: Double, dsServer: String) -> APIRequest {
        let reverseGeocodeRequest = APIRequest(method: .get, path: nil, baseUrl: URL(string: "https://\(dsServer)/\(ApiConstants.BaseURLVersion)\( ApiConstants.Devices.ReverseGeocodingSinglePath)\(latitude)/\(longitude)")!)
        reverseGeocodeRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        reverseGeocodeRequest.queryItems = [
            URLQueryItem(name: "language", value: "en")
        ]
        return reverseGeocodeRequest
    }
    
    static func createReverseGeocodingSingleRequest(vehicleValues: UsedValues, dsServer: String) -> APIRequest {
        let reverseGeocodeRequest = APIRequest(method: .get, path: nil, baseUrl: URL(string: "https://\(dsServer)/\(ApiConstants.BaseURLVersion)\( ApiConstants.Devices.ReverseGeocodingSinglePath)\(vehicleValues.latitude!)/\(vehicleValues.longitude!)")!)
        reverseGeocodeRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        reverseGeocodeRequest.queryItems = [
            URLQueryItem(name: "language", value: "en")
        ]
        return reverseGeocodeRequest
    }
    
    static func createDeviceStatusSinceRequest(dsServer: String, deviceId: Int, startDate: String, status: String) -> APIRequest {
        let statusSinceRequest = APIRequest(method: .post, path: nil, baseUrl: URL(string: "https://\(dsServer)/\(ApiConstants.BaseURLVersion)\( ApiConstants.Devices.StatusSincePath)")!)
        statusSinceRequest.queryItems = [
            URLQueryItem(name: "deviceid", value: String(deviceId)),
            URLQueryItem(name: "startdate", value: startDate),
            URLQueryItem(name: "status", value: String(status))
        ]
        statusSinceRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        return statusSinceRequest
    }
    
    static func createDeviceHistoryRequest(deviceId: Int, startDate: String, endDate: String, dsServer: String) -> APIRequest {
        let deviceHistoryRequest = APIRequest(method: .post, path: nil, baseUrl: URL(string: "https://\(dsServer)/\(ApiConstants.BaseURLVersion)\( ApiConstants.Devices.GetHistoryPath)")!)
        deviceHistoryRequest.queryItems = [
            URLQueryItem(name: "device_id", value: String(deviceId)),
            URLQueryItem(name: "period_begin", value: startDate),
            URLQueryItem(name: "period_end", value: endDate)
        ]
        deviceHistoryRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        return deviceHistoryRequest
    }
    
    static func saveDevicePositionTransmissionJob(vehicleId: Int, startDate: String, endDate: String, frequency: String, email: String, phone: String, sendAddress: Bool, sendStatus: Bool, sendSpeed: Bool) -> APIRequest {
        let saveDevicePositionTransmissionJobRequest = APIRequest(method: .post, path: ApiConstants.Devices.SaveDevicePositionTransmissionPath)
        saveDevicePositionTransmissionJobRequest.queryItems = [
            URLQueryItem(name: "device_id", value: String(vehicleId)),
            URLQueryItem(name: "user_id", value: String(StateController.currentUser?.userId ?? 0)),
            URLQueryItem(name: "period_begin", value: startDate),
            URLQueryItem(name: "period_end", value: endDate),
            URLQueryItem(name: "frequency", value: frequency),
            URLQueryItem(name: "email", value: email),
            URLQueryItem(name: "phone", value: phone),
            URLQueryItem(name: "send_coords", value: "1"),
            URLQueryItem(name: "send_address", value: sendAddress ? "1" : "0"),
            URLQueryItem(name: "send_status", value: sendStatus ? "1" : "0"),
            URLQueryItem(name: "send_speed", value: sendSpeed ? "1" : "0")
        ]
        saveDevicePositionTransmissionJobRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        return saveDevicePositionTransmissionJobRequest
    }
    
    static func getActiveDevicePositionTransmissionJob(vehicleId: Int) -> APIRequest {
        let getActiveDevicePositionTransmissionJobRequest = APIRequest(method: .get, path: ApiConstants.Devices.GetActiveDevicePositionTransmissionPath+"\(vehicleId)")
        getActiveDevicePositionTransmissionJobRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        return getActiveDevicePositionTransmissionJobRequest
    }
    
    static func deactivateDevicePositionTransmissionJob(transmissionId: Int) -> APIRequest {
        let deactivateDevicePositionTransmissionJobRequest = APIRequest(method: .put, path: ApiConstants.Devices.DeactivateDevicePositionTransmissionPath)
        deactivateDevicePositionTransmissionJobRequest.queryItems = [
            URLQueryItem(name: "id", value: String(transmissionId))
        ]
        deactivateDevicePositionTransmissionJobRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        return deactivateDevicePositionTransmissionJobRequest
    }
    
    static func getDeviceCommandStatusRequest(vehicleId: Int, type: String) -> APIRequest {
        let getDeviceCommandStatusRequest = APIRequest(method: .get, path: ApiConstants.Devices.DeviceCommandStatus)
        getDeviceCommandStatusRequest.queryItems = [
            URLQueryItem(name: "deviceId", value: String(vehicleId)),
            URLQueryItem(name: "type", value: type)
        ]
        getDeviceCommandStatusRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        return getDeviceCommandStatusRequest
    }
    
    static func sendCommandRequest(command: String, commandTime: String, commandType: String, vehicleId: Int) -> APIRequest {
        let sendCommandRequest = APIRequest(method: .post, path: ApiConstants.Devices.SendCommand)
        sendCommandRequest.queryItems = [
            URLQueryItem(name: "command", value: command),
            URLQueryItem(name: "commandTime", value: commandTime),
            URLQueryItem(name: "commandType", value: commandType),
            URLQueryItem(name: "deviceId", value: String(vehicleId)),
            URLQueryItem(name: "userId", value: String(StateController.currentUser?.userId ?? 0))
        ]
        sendCommandRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        return sendCommandRequest
    }
    
    static func updateFuelConsumptionRequest(vehicleId: Int, dsServer: String, dateTime: String, mileage: Double, sensor: Double) -> APIRequest {
        let updateFuelConsumptionRequest = APIRequest(method: .post, path: nil, baseUrl: URL(string: "https://\(dsServer)/\(ApiConstants.BaseURLVersion)\( ApiConstants.Devices.UpdateFuelConsumptionPath)")!)
        updateFuelConsumptionRequest.queryItems = [
            URLQueryItem(name: "id", value: String(vehicleId)),
            URLQueryItem(name: "datetime", value: dateTime),
            URLQueryItem(name: "sensor", value: String(sensor)),
            URLQueryItem(name: "mileage", value: String(mileage))
        ]
        updateFuelConsumptionRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        return updateFuelConsumptionRequest
    }
    
    static func getRefuelingsPath(startDate: String, endDate: String, deviceId: Int, dsServer: String) -> APIRequest {
        let getRefuelingsPath = APIRequest(method: .post, path: nil, baseUrl: URL(string: "https://\(dsServer)/\(ApiConstants.BaseURLVersion)\( ApiConstants.Devices.GetRefuelingsPath)")!)
        let jsonDictionary = [
            "period_begin": startDate,
            "period_end": endDate,
            "device_id": String(deviceId)
        ]
        var jsonData = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
        } catch {
            print(error.localizedDescription)
        }
        getRefuelingsPath.body = jsonData
        getRefuelingsPath.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        return getRefuelingsPath
    }
    
    static func getCurrencyConversion(from currency: CurrencyType) -> APIRequest {
        let getCurrencyConversionRequest = APIRequest(method: .get, path: ApiConstants.Devices.GetConversionQuotePath + "\(currency.rawValue.uppercased())")
        getCurrencyConversionRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        return getCurrencyConversionRequest
    }
    
    static func rechargeWith(currencyModel: CurrencyModel, amount: String, amountExchanged: String, chargeFee: String, chargeFeeVAT: String, currencyType: CurrencyType) -> APIRequest {
        let getRefuelingsPath = APIRequest(method: .post, path: ApiConstants.Devices.PayRoadTaxPath)
        let jsonDictionary = [
            "exchange_rate": String(currencyModel.exchangeRate),
            "recharge_amount": amount,
            "recharge_amount_exchanged": amountExchanged,
            "recharge_fee": chargeFee,
            "recharge_fee_vat": chargeFeeVAT,
            "exchange_currency": currencyType.rawValue.uppercased()
        ]
        var jsonData = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
        } catch {
            print(error.localizedDescription)
        }
        getRefuelingsPath.body = jsonData
        getRefuelingsPath.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        return getRefuelingsPath
    }

    static func getInvoiceList() -> APIRequest {
        let getInvoiceListRequest = APIRequest(method: .get, path: ApiConstants.Devices.InvoiceList)
        getInvoiceListRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        return getInvoiceListRequest
    }

    static func downloadInvoice(id: Int) -> APIRequest {
        let downloadInvoiceRequest = APIRequest(method: .get, path: ApiConstants.Devices.DownloadInvoice + "\(id)")
        downloadInvoiceRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? "")]
        return downloadInvoiceRequest
    }

    static func initializePayment(invoices: [Int]) -> APIRequest {
        let initPaymentRequest = APIRequest(method: .post, path: ApiConstants.Devices.InitPayment)
        let jsonDictionary = [
            "ids": invoices
        ]
        var jsonData = Data()
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
        } catch {
            print(error.localizedDescription)
        }
        initPaymentRequest.body = jsonData
        initPaymentRequest.headers = [HTTPHeader(field: "Authorization", value: UserService.shared.accessToken ?? ""), HTTPHeader(field: "Content-type", value: "application/json")]
        return initPaymentRequest
    }
}
