//
//  Alimentare.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 27/04/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

struct Alimentare: Codable {
    var fuel: Double
    var date: String
    var address: DeviceAddress
    var latitude: Double
    var longitude: Double
    
    var isPositive: Bool {
        return fuel > 0
    }
    
    private enum CodingKeys: String, CodingKey {
        case fuel = "fd", date = "de", address = "loc", latitude = "lat", longitude = "lon"
    }
}
