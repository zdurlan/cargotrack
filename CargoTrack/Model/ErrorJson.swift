//
//  ErrorJson.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 27/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct ErrorJson: Decodable {
    let error: String
}

struct PaymentURLModel: Codable {
    let paymentUrl: String
}
