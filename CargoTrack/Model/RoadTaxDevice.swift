//
//  RoadTaxDevice.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 01/07/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import Foundation

struct RoadTaxDevice: Codable {
    let id: Int
    let eurocode: String?
    let deviceCountry: String?
    let roadtaxCountry: String?
    let tractorAxles: Int?
    let trailerAxles: Int?
    let responsiblePhone: String?
    let deviceAxleNumber: String?
    let roadtaxStatus: Int?
    let roadtaxHuBalanceStatus: Int?
    let dateSuspendedSince: String?
    let deviceObuid: String?
    let grossWeight: Int?
    let name: String?
    let traccarDeviceId: Int?
    let deviceDataServer: String?
    let roadtaxBgMainCategory: Int?
    let roadtaxBgWeightCategory: Int?
    let isAutomatically: Int?
    var date: String?
}
