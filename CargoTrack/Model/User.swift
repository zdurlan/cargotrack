//
//  User.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 26/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct User: Codable {
    let userId: Int
    let clientId: Int
    let clientDataServer: String
    let token: String
    var name: String?
    let username: String?
    var email: String?
    var phone: String?
    let isSuspended: Int?
    let mobileAppAsWrapper: Bool?
    let hostServer: String?
    var userPhoto: String?
    let frontendPreferences: String?
    var stopDuration: Int?
    var userOptions: [String?]?
    var userRights: [UserRight]?
}

struct UserRight: Codable {
    let id: Int
    let name: String
}
