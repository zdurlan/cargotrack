//
//  CountResponse.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 02/06/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import Foundation

struct CountResponse: Codable {
    let total: Int
}
