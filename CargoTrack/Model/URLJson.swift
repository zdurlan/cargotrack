//
//  URLJson.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 27/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct URLJson: Decodable {
    let url: String
}

struct PaymentURL: Decodable {
    let paymentUrl: String
}
