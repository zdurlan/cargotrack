//
//  SuccessJSON.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 31/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct SuccessJSON: Codable {
    let message: String
}
