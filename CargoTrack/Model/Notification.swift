//
//  Notification.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 03/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct NotificationStruct: Codable {
    let type: String
    let payload: NotificationPayload
}

struct NotificationPayload: Codable {
    let id: Int
    let summary: String
    let message: String
    var createdAt: String?
    var dateRead: String?
    var preview: String?
    var userNotificationTypeId: Int?
    var priority: Int
}
