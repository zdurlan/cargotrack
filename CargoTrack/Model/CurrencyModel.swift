//
//  CurrencyModel.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 13/08/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import Foundation

struct CurrencyModel: Codable {
    let vatPercent: Int
    let commissionPercent: Double
    let exchangeRate: Double
}
