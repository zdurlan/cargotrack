//
//  DataReport.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 27/04/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import Foundation

struct Position: Codable {
    var dt: String
    var fuel: Double
}

struct Consumption: Codable {
    var pb: String
    var pe: String
    var con: Double
}

struct DataReport: Codable {
    var chartData: [Position]
    var refuelings: [Alimentare]
    var consumptions: [Consumption]
    var distance: Double
}
