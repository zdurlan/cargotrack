//
//  BillModel.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 01/10/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import Foundation

struct BillModel: Codable {
    let invoices: [InvoiceModel]
}

struct InvoiceModel: Codable, Equatable {
    let id: Int
    let serial: String
    let filename: String?
    let companyId: Int
    let total: String
    let paidAmount: String?
    let paymentStatus: Int
    let date: String
    let dateDue: String
    let number: String
    var isSelected: Bool?

    static func == (lhs: InvoiceModel, rhs: InvoiceModel) -> Bool {
        return lhs.id == rhs.id
    }
}

struct DownloadInvoiceModel: Codable {
    let filename: String
    let data: String
}
