//
//  Vehicle.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 05/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import RealmSwift
import UIKit

@objcMembers class Vehicle: Object, Codable {
    dynamic var id: Int = 0
    dynamic var secondaryId: Int = 0
    dynamic var name: String = ""
    dynamic var label: String?
    dynamic var hostServer: String = ""
    dynamic var refuelingTreshold: Int? = 10
    dynamic var leakTreshold: Int? = 10
    dynamic var deviceTypeId: Int = 0
    dynamic var optFuelCh: Int = 0
    dynamic var attributes: Attribute?
    var attributeDefinitions: AttributeDefinitions
    dynamic var usedValues: UsedValues?
    dynamic var deviceTime: String?
    dynamic var attributeDefinitionsData: Data?
    dynamic var attributeCorrections: AttributeCorrection?
    dynamic var oldValues: UsedValues?
    dynamic var fuelConsumption: FuelConsumptionSensor?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["attributeDefinitions"]
    }
    
    func getData(deviceAttributeLogic: DeviceAttributeLogic) -> UsedValues? {
        let encoder = DictionaryEncoder()
        let encoded = try? encoder.encode(attributeDefinitions) as [String: Any]
        if encoded != nil {
            attributeDefinitionsData = try! JSONSerialization.data(withJSONObject: encoded!, options: JSONSerialization.WritingOptions.prettyPrinted)
            deviceTime = deviceAttributeLogic.rawValues["devicetime"] as? String
            let usedValues = UsedValues()
            if let mileage = attributes?.mileage {
                usedValues.mileage = deviceAttributeLogic.getValue(attributeKey: mileage)
            }
            if let status = attributes?.status {
                usedValues.status = deviceAttributeLogic.getValue(attributeKey: status)
            }
            let fuelConsumptionSemaphore = DispatchSemaphore(value: 0)
            if let fuelConsumptionValue = fuelConsumption?.sensor.value,
                let mileage = fuelConsumption?.mileage.value {
                APIClient().perform(DeviceService.createDevicePositionsRequest(deviceId: [self.secondaryId], dsServer: self.hostServer)) { [weak self] result in
                    guard let strongSelf = self else {
                        return
                    }
                    switch result {
                    case .success(let response):
                        guard let json = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: [.allowFragments]) as? [[String: Any]], json.count > 0 else {
                            print("error decoding JSON")
                            fuelConsumptionSemaphore.signal()
                            return
                        }
                        let newDeviceAttributeLogic = DeviceAttributeLogic(rawValues: json.first(where: {
                            $0["deviceid"] as! Int == strongSelf.secondaryId
                        })!, definitions: strongSelf.attributeDefinitions)
                        let newMileage = newDeviceAttributeLogic.getValue(attributeKey: strongSelf.attributes!.mileage!)
                        let newFuelConsumption = newDeviceAttributeLogic.getValue(attributeKey: strongSelf.attributes!.fuelConsumption!)
                        if newFuelConsumption != "LOGIC_ERROR" && newMileage != "LOGIC_ERROR" {
                            let mileageKm = Double(newMileage)! - Double(mileage)
                            if mileageKm > 0 {
                                let consumption = Double(newFuelConsumption)! - Double(fuelConsumptionValue)
                                let lastConsumption = (100 * consumption) / mileageKm
                                usedValues.fuelConsumption = String(lastConsumption)
                            }
                        }
                        fuelConsumptionSemaphore.signal()
                    case .failure(let error):
                        print(error.localizedDescription)
                        fuelConsumptionSemaphore.signal()
                    }
                }
            } else {
                fuelConsumptionSemaphore.signal()
            }
            fuelConsumptionSemaphore.wait()
            if let fuelLevel = attributes?.fuelLevel {
                usedValues.fuelLevel = deviceAttributeLogic.getValue(attributeKey: fuelLevel)
            }
            if let powerSupply = attributes?.powerSupply {
                usedValues.powerSupply = deviceAttributeLogic.getValue(attributeKey: powerSupply)
            }
            usedValues.speed = deviceAttributeLogic.getValue(attributeKey: PositionAttributes.speed.rawValue)
            usedValues.latitude = deviceAttributeLogic.getValue(attributeKey: PositionAttributes.latitude.rawValue)
            usedValues.longitude = deviceAttributeLogic.getValue(attributeKey: PositionAttributes.longitude.rawValue)
            usedValues.course = deviceAttributeLogic.getValue(attributeKey: PositionAttributes.course.rawValue)
            oldValues = usedValues
            return usedValues
        } else {
            return nil
        }
    }
    
    func getData(deviceAttributeLogicDictionary: DeviceAttributeLogicDictionary) -> UsedValues {
        deviceTime = deviceAttributeLogicDictionary.rawValues["devicetime"] as? String
        let usedValues = UsedValues()
        if let mileage = attributes?.mileage {
            usedValues.mileage = deviceAttributeLogicDictionary.getValue(attributeKey: mileage)
            if usedValues.mileage == "LOGIC_ERROR" {
                usedValues.mileage = self.usedValues?.mileage
            }
        }
        if let status = attributes?.status {
            usedValues.status = deviceAttributeLogicDictionary.getValue(attributeKey: status)
            if usedValues.status == "LOGIC_ERROR" {
                usedValues.status = self.usedValues?.status
            }
        }
        if let fuelLevel = attributes?.fuelLevel {
            usedValues.fuelLevel = deviceAttributeLogicDictionary.getValue(attributeKey: fuelLevel)
            if usedValues.fuelLevel == "LOGIC_ERROR" {
                usedValues.fuelLevel = self.usedValues?.fuelLevel
            }
        }
        if let powerSupply = attributes?.powerSupply {
            usedValues.powerSupply = deviceAttributeLogicDictionary.getValue(attributeKey: powerSupply)
            if usedValues.powerSupply == "LOGIC_ERROR" {
                usedValues.powerSupply = self.usedValues?.powerSupply
            }
        }
        usedValues.speed = deviceAttributeLogicDictionary.getValue(attributeKey: PositionAttributes.speed.rawValue)
        usedValues.latitude = deviceAttributeLogicDictionary.getValue(attributeKey: PositionAttributes.latitude.rawValue)
        usedValues.longitude = deviceAttributeLogicDictionary.getValue(attributeKey: PositionAttributes.longitude.rawValue)
        usedValues.course = deviceAttributeLogicDictionary.getValue(attributeKey: PositionAttributes.course.rawValue)
        return usedValues
    }
}

@objcMembers class AttributeCorrection: Object, Codable {
    dynamic var attrMileageCorrectVal: Int
    dynamic var attrMileageCorrectPercent: Int
}

@objcMembers class UsedValues: Object, Codable {
    dynamic var mileage: String?
    dynamic var fuelConsumption: String?
    dynamic var fuelLevel: String?
    dynamic var powerSupply: String?
    dynamic var status: String?
    dynamic var speed: String?
    dynamic var course: String?
    dynamic var latitude: String?
    dynamic var longitude: String?
    dynamic var location: String?
    dynamic var countryCode: String?
}

enum PositionAttributes: String {
    case latitude
    case longitude
    case speed
    case course
}

@objcMembers class FuelConsumptionSensor: Object, Codable {
    dynamic var datetime: String? = ""
    dynamic var mileage = RealmOptional<Double>()
    dynamic var sensor = RealmOptional<Double>()
}
