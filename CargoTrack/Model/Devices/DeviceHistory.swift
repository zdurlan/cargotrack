//
//  DeviceHistory.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 24/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import AnyCodable
import Foundation

struct VehicleMovingDuration: Codable {
    let moving: Int
    let stopped: Int
    let idling: Int
}

struct DeviceHistory: Codable, Equatable {
    static func == (lhs: DeviceHistory, rhs: DeviceHistory) -> Bool {
        return lhs.period.begin == rhs.period.begin
    }
    
    let duration: VehicleMovingDuration
    let distance: Double
    let address: HistoryAddress
    let period: Period
    let positions: [[String: AnyCodable]]
    var stopNumber: Int? = 0
    
    mutating func setNumber(_ number: Int) {
        stopNumber = number
    }
    
    func getType() -> String {
        return duration.moving == 0 ? "stop" : (duration.idling + duration.stopped) > 0 ? "stoptrip" : "trip"
    }
}

struct FuelConsumptionHistory: Codable {
    let firstValue: Double
    let lastValue: Double
}

struct HistoryAddress: Codable {
    let begin: DeviceAddress
    let end: DeviceAddress
}

struct Period: Codable {
    let begin: String
    let end: String
}
