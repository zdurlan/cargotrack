//
//  DeviceAttributeLogic.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 21/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

class DeviceAttributeLogic {
    let LOGIC_ERROR = "LOGIC_ERROR"
    
    var rawValues: [String: Any] = [:]
    var definitions: AttributeDefinitions
    
    private let decoder = JSONDecoder()
    
    init(rawValues: [String: Any], definitions: AttributeDefinitions) {
        self.rawValues = rawValues
        self.definitions = definitions
    }
    
    public func getValue(attributeKey: String) -> String {
        if rawValues.keys.contains(attributeKey) {
            return String(describing: rawValues[attributeKey]!)
        } else {
            switch definitions {
            case .attributeDefinitionsDict(let attributeDef):
                let attribute = attributeDef[attributeKey]
                if let attribute = attribute {
                    return determineValue(attribute)
                } else {
                    return LOGIC_ERROR
                }
            case .emptyArray:
                return LOGIC_ERROR
            }
        }
    }
    
    private func sortDictionaryKey(dictData: [String: Float]) -> MutableOrderedDictionary {
        // initializing empty ordered dictionary
        let orderedDic = MutableOrderedDictionary()
        // copying normalDic in orderedDic after a sort
        dictData.sorted {
            let firstInt = Int($0.0)!
            let secondInt = Int($1.0)!
            return firstInt < secondInt
            }.forEach { orderedDic.setObject($0.value, forKey: $0.key) }
        // from now, looping on orderedDic will be done in the alphabetical order of the keys
        
        return orderedDic
    }
    
    private func determineValue (_ attributeDefinition: AttributeDefinition) -> String {
        switch attributeDefinition.logic {
        case .arithmetic(let arithmeticLogic):
            return determineArithmeticValue(arithmeticLogic)
        case .conditional(let conditionalLogic):
            return determineConditionalValue(conditionalLogic)
        case .linear(let linearLogic):
            return determineLinearValue(linearLogic)
        case .function(let functionLogic):
            return determineFunctionValue(functionLogic)
        default:
            return LOGIC_ERROR
        }
    }
    
    private func determineArithmeticValue(_ logic: ArithmeticLogic) -> String {
        let result = getValue(attributeKey: logic.baseOperand)
        if result == LOGIC_ERROR {
            return LOGIC_ERROR
        }
        
        var returnResult = Double(result)!
        
        for opList in logic.operandList {
            var stringOperand = opList.fixedValue
            
            if !opList.operand.isEmpty {
                stringOperand = getValue(attributeKey: opList.operand)
                if stringOperand == LOGIC_ERROR {
                    return LOGIC_ERROR
                }
            }
            
            let operand = Double(stringOperand)!
            switch opList.operator {
            case "+":
                returnResult += operand
            case "-":
                returnResult -= operand
            case "*":
                returnResult *= operand
            case "/":
                returnResult /= operand
            case "~":
                returnResult = precisionRound(returnResult, operand)
            default:
                break
            }
        }
        
        return String(returnResult)
    }
    
    private func precisionRound(_ number: Double, _ precision: Double) -> Double {
        let factor = pow(10, precision)
        return round(number * factor) / factor
    }
    
    private func determineConditionalValue(_ logic: ConditionalLogic) -> String {
        for el in logic.conditions {
            var conditionTrue = true
            for element in el.elements {
                let compareTo = getValue(attributeKey: element.attribute)
                let testValue = element.testValue
                let logicOperator = element.logicOperator
                
                if compareTo == LOGIC_ERROR {
                    return LOGIC_ERROR
                }
                
                switch logicOperator {
                case "=":
                    if (compareTo as NSString).intValue != (testValue as NSString).intValue {
                        conditionTrue = false
                    }
                case "!=":
                    if (compareTo as NSString).intValue == (testValue as NSString).intValue {
                        conditionTrue = false
                    }
                case "<":
                    if (compareTo as NSString).intValue >= (testValue as NSString).intValue {
                        conditionTrue = false
                    }
                case ">":
                    if (compareTo as NSString).intValue <= (testValue as NSString).intValue {
                        conditionTrue = false
                    }
                case "<=":
                    if (compareTo as NSString).intValue > (testValue as NSString).intValue {
                        conditionTrue = false
                    }
                case ">=":
                    if (compareTo as NSString).intValue < (testValue as NSString).intValue {
                        conditionTrue = false
                    }
                default:
                    break
                }
            }
            
            if conditionTrue {
                if el.resultToBeEvaluated != nil && el.result.isEmpty {
                    return getValue(attributeKey: el.resultToBeEvaluated!)
                } else {
                    return el.result
                }
            }
        }
        
        if logic.finalElseToBeEvaluated != nil && logic.finalElse.isEmpty {
            return getValue(attributeKey: logic.finalElseToBeEvaluated!)
        } else {
            return logic.finalElse
        }
    }
    
    // see the "point-slope" form of the equation of a straight line
    // y − y1 = m(x − x1)
    // slope: m = (y2-y1)/(x2-x1)
    private func determineLinearValue(_ logic: LinearLogic) -> String {
        switch logic.coords {
        case .emptyArray:
            return LOGIC_ERROR
        case .coordinatesDict(let coords):
            let sortedCoords = sortDictionaryKey(dictData: coords)
            
            let strValue = getValue(attributeKey: logic.attribute)
            if strValue == LOGIC_ERROR {
                return LOGIC_ERROR
            }
            
            var value = Double(strValue)!
            let x = value
            
            var x1: Double?
            var y1: Double?
            let m: Double
            
            for coord in sortedCoords {
                let coordKey = coord.key as! String
                let coordValue = coord.value as! Float
                let x2 = Double(coordKey)!
                let y2 = Double(coordValue)
                
                if x < x2 && x1 == nil {
                    return LOGIC_ERROR
                } else if x == x2 {
                    return String(y2)
                } else if x < x2 {
                    if x2 - x1! == 0.0 {
                        return LOGIC_ERROR
                    }
                    
                    m = (y2 - y1!) / (x2 - x1!)
                    value = m * (x - x1!) + y1!
                    break
                }
                x1 = x2
                y1 = y2
            }
            
            if value < Double(logic.min.replacingOccurrences(of: " ", with: ""))! {
                value = Double(logic.min.replacingOccurrences(of: " ", with: ""))!
            }
            
            if value > Double(logic.max.replacingOccurrences(of: " ", with: ""))! {
                value = Double(logic.max.replacingOccurrences(of: " ", with: ""))!
            }
            
            return String(value)
        }
        
    }
    
    private func determineFunctionValue(_ logic: FunctionLogic) -> String {
        var result = getValue(attributeKey: logic.functionArgument)
        if result == LOGIC_ERROR {
            return LOGIC_ERROR
        }
        
        for val in logic.functionList {
            let functionName = String(val).replacingOccurrences(of: "\"", with: "")
            switch functionName.uppercased() {
            case "DEC2HEX":
                result = String(format:"%02X", result)
            case "HEX2STR":
                result = hex2str(result)
            case "INVERT2":
                result = invert2(result)
            case "LOWERCASE":
                result = result.lowercased()
            case "UPPERCASE":
                result = result.uppercased()
            case "DONOTHING":
                break
            default:
                break
            }
        }
        
        return result
    }
    
    public func dec2hex(_ dec: Decimal) -> String {
        let intValue = NSDecimalNumber(decimal: dec).intValue
        return String(intValue, radix: 16)
    }
    
    public func hex2str(_ hex: String) -> String {
        let n = hex.count
        var sb = String(n / 2)
        var i = 0
        while (i < n) {
            var index = hex.index(hex.startIndex, offsetBy: i)
            let a = hex[index]
            index = hex.index(hex.startIndex, offsetBy: i+1)
            let b = hex[index]
            sb.append(String(hexToInt(a) << 4 | hexToInt(b)))
            i += 2
        }
        return String(sb)
    }
    
    /**
     * Invert a string by taking 2 chars pairs
     * Output is always an even lenghted string
     *
     * example: 10833C319000020
     * =>  1 08 33 C3 19 00 00 20
     * => 20 00 00 19 C3 33 08 01
     * => 20000019C3330801
     * @param string input
     * @return string
     */
    
    private func hexToInt(_ ch: Character) -> Int {
        if "a" <= ch && ch <= "f" {
            return Int(ch.unicodeScalars.first!.value - "a".unicodeScalars.first!.value + 10)
        }
        if "A" <= ch && ch <= "F" {
            return Int(ch.unicodeScalars.first!.value - "A".unicodeScalars.first!.value + 10)
        }
        if "0" <= ch && ch <= "9" {
            return Int(ch.unicodeScalars.first!.value - "0".unicodeScalars.first!.value)
        }
        assertionFailure("error")
        return 0
    }
    
    public func invert2(_ input: String) -> String {
        var output = ""
        let length = input.count
        var i = length
        while (i > 0) {
            let start = i - 2
            if start >= 0 {
                let index = input.index(input.endIndex, offsetBy: -2)
                output += input[..<index]
            } else {
                let index = input.index(input.startIndex, offsetBy: 1)
                output += "0" + input[..<index]
            }
            i -= 2
        }
        
        return output
    }
}
