//
//  ArithmeticLogic.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 05/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct ArithmeticLogic: Codable {
    var type: String
    var baseOperand: String
    var operandList: [OperandList]
}
