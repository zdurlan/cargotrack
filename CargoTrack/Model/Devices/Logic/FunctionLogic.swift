//
//  FunctionLogic.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 05/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct FunctionLogic: Codable {
    var type: String
    var functionArgument: String
    var functionList: [String]
}
