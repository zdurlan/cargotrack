//
//  Logic.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 05/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct Condition: Codable {
    var result: String
    var resultToBeEvaluated: String?
    var elements: [Element]
}

struct Element: Codable {
    var attribute: String
    var logicOperator: String
    var testValue: String
}

struct OperandList: Codable {
    var `operator`: String
    var operand: String
    var fixedValue: String
}

enum Logic {
    case arithmetic(ArithmeticLogic)
    case conditional(ConditionalLogic)
    case linear(LinearLogic)
    case function(FunctionLogic)
    case unsupported
}

extension Logic: Codable {
    private func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let logic = try container.decode(String.self)
        
        let delimiter = ","
        let logicTypeComponents = logic.components(separatedBy: delimiter)

        var logicType = logicTypeComponents[0]
        logicType = logicType.replacingOccurrences(of: "{", with: "")
        logicType = logicType.replacingOccurrences(of: ":", with: "")
        logicType = logicType.replacingOccurrences(of: "type", with: "")
        logicType = logicType.replacingOccurrences(of: "\"", with: "")
        
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        
        switch logicType {
        case "arithmetic":
            if let data = logic.data(using: .utf8) {
                do {
                    let logic = try jsonDecoder.decode(ArithmeticLogic.self, from: data)
                    self = .arithmetic(logic)
                } catch {
                    self = .unsupported
                    print(error.localizedDescription)
                }
            } else {
                self = .unsupported
            }
        case "conditional":
            if let data = logic.data(using: .utf8) {
                do {
                    let logic = try jsonDecoder.decode(ConditionalLogic.self, from: data)
                    self = .conditional(logic)
                } catch {
                    self = .unsupported
                    print(error.localizedDescription)
                }
            } else {
                self = .unsupported
            }
        case "linear":
            if let data = logic.data(using: .utf8) {
                do {
                    let logic = try jsonDecoder.decode(LinearLogic.self, from: data)
                    self = .linear(logic)
                } catch {
                    self = .unsupported
                    print(error.localizedDescription)
                }
            } else {
                self = .unsupported
            }
        case "function":
            if let data = logic.data(using: .utf8) {
                do {
                    let logic = try jsonDecoder.decode(FunctionLogic.self, from: data)
                    self = .function(logic)
                } catch {
                    self = .unsupported
                    print(error.localizedDescription)
                }
            } else {
                self = .unsupported
            }
        default:
            self = .unsupported
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .arithmetic(let logic):
            try container.encode(logic)
        case .conditional(let logic):
            try container.encode(logic)
        case .linear(let logic):
            try container.encode(logic)
        case .function(let logic):
            try container.encode(logic)
        case .unsupported:
            let context = EncodingError.Context(codingPath: [], debugDescription: "Invalid logic.")
            throw EncodingError.invalidValue(self, context)
        }
    }
}
