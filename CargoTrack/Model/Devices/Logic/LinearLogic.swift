//
//  LinearLogic.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 05/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

enum Coordinates {
    case emptyArray([String])
    case coordinatesDict([String: Float])
}

extension Coordinates: Codable {
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode([String].self) {
            self = .emptyArray(x)
            return
        }
        if let x = try? container.decode([String: Float].self) {
            self = .coordinatesDict(x)
            return
        }
        throw DecodingError.typeMismatch(Coordinates.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Coordinates"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .emptyArray(let x):
            try container.encode(x)
        case .coordinatesDict(let x):
            try container.encode(x)
        }
    }
}

struct LinearLogic: Codable {
    var type: String
    var attribute: String
    var min: String
    var max: String
    var coords: Coordinates
}
