//
//  ConditionalLogic.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 05/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct ConditionalLogic: Codable {
    var type: String
    var finalElse: String
    var finalElseToBeEvaluated: String?
    var conditions: [Condition]
}
