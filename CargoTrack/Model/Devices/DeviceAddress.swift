//
//  DeviceAddress.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 14/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct DeviceAddress: Codable {
    let addressFormatted: String
    let addressElements: [String: String?]
}
