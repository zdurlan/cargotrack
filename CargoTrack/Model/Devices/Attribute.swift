//
//  Attribute.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 05/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Attribute: Object, Codable {
    dynamic var status: String?
    dynamic var powerSupply: String?
    dynamic var mileage: String?
    dynamic var driverId: String?
    dynamic var fuelLevel: String?
    dynamic var fuelConsumption: String?
    dynamic var buzzer: String?
    dynamic var immobiliser: String?
    dynamic var camera: String?
    dynamic var ignition: String?
    dynamic var mobileCountryCode: String?
    dynamic var auxiliaryHeating: String?
    dynamic var canbusEngineHours: String?
    dynamic var canbusMileage: String?
}
