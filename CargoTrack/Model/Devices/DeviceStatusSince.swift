//
//  DeviceStatusSince.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 15/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct DeviceStatusSince: Codable {
    let dateStatusSince: String
    let statusFound: Bool
}
