//
//  AttributeDefinitions.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 05/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct AttributeDefinition: Codable {    
    var name: String
    var logic: Logic
}

enum AttributeDefinitions {
    case emptyArray([String])
    case attributeDefinitionsDict([String: AttributeDefinition])
    
    func get() -> [String: AttributeDefinition]? {
        switch self {
        case .emptyArray:
            return nil
        case .attributeDefinitionsDict(let dict):
            return dict
        }
    }
    
    init(dict: [String: AttributeDefinition]) {
        self = .attributeDefinitionsDict(dict)
    }
    
}

extension AttributeDefinitions: Codable {
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode([String].self) {
            self = .emptyArray(x)
            return
        }
        if let x = try? container.decode([String: AttributeDefinition].self) {
            self = .attributeDefinitionsDict(x)
            return
        }
        throw DecodingError.typeMismatch(AttributeDefinitions.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for AttributeDefinitions"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .emptyArray(let x):
            try container.encode(x)
        case .attributeDefinitionsDict(let x):
            try container.encode(x)
        }
    }
}
