//
//  DevicePositionTransmission.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 12/08/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

struct DevicePositionTransmissionResponse: Codable {
    let status: String
    let devPosTrJob: DevicePositionTransmission?
}

struct DevicePositionTransmission: Codable {
    let id: Int
    let deviceId: Int
    let userId: Int
    let isActive: Int
    let periodBegin: String
    let periodEnd: String
    let frequency: Int
    let email: String?
    let phone: String?
    let sendCoords: Int
    let sendAddress: Int
    let sendStatus: Int
    let sendSpeed: Int
}
