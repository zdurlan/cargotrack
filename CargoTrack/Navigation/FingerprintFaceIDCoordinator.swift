//
//  FingerprintFaceIDCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 04/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class FingerprintFaceIDCoordinator: Coordinator {
    private let presenter: UINavigationController
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    override func start() {
        let fingerprintViewController: FingerprintFaceIDViewController = Storyboard.Onboarding.loadViewController()
        if UIDevice().notXIphone {
            fingerprintViewController.authMethod = .fingerprint
        } else {
            fingerprintViewController.authMethod = .faceID
        }
        setDeallocallable(with: fingerprintViewController)
        presenter.pushViewController(fingerprintViewController, animated: true)
    }
}
