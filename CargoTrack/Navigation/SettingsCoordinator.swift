//
//  SettingsCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 03/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class SettingsCoordinator: Coordinator {
    private let presenter: UINavigationController
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    override func start() {
        let settingsViewController: SettingsViewController = Storyboard.Menu.loadViewController()
        setDeallocallable(with: settingsViewController)
        presenter.pushViewController(settingsViewController, animated: true)
    }
}
