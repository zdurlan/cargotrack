//
//  RoadTaxBalanceCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 30/06/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class RoadTaxBalanceCoordinator: Coordinator {
    private let presenter: UINavigationController
    private let taxState: TaxState
    
    init(presenter: UINavigationController, taxState: TaxState) {
        self.presenter = presenter
        self.taxState = taxState
    }
    
    override func start() {
        let roadTaxBalanceVC: RoadTaxBalanceViewController = Storyboard.Menu.loadViewController()
        roadTaxBalanceVC.taxState = taxState
        setDeallocallable(with: roadTaxBalanceVC)
        presenter.pushViewController(roadTaxBalanceVC, animated: true)
    }
}
