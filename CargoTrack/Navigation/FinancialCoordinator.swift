//
//  FinancialCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 24/09/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class FinancialCoordinator: Coordinator {
    private let presenter: UINavigationController
    var isFromFingerprint = false
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }

    override func start() {
        let financialVC: FinancialViewController = Storyboard.Menu.loadViewController()
        financialVC.isFromFingerprint = isFromFingerprint
        setDeallocallable(with: financialVC)
        presenter.pushViewController(financialVC, animated: true)
    }
}
