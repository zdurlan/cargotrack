//
//  NotificationsCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 03/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class NotificationsCoordinator: Coordinator {
    private let presenter: UINavigationController
    private var isFromMenu: Bool
    
    init(presenter: UINavigationController, isFromMenu: Bool) {
        self.presenter = presenter
        self.isFromMenu = isFromMenu
    }
    
    override func start() {
        let notificationsViewController: NotificationsViewController = Storyboard.Menu.loadViewController()
        notificationsViewController.isFromMenu = isFromMenu
        setDeallocallable(with: notificationsViewController)
        presenter.pushViewController(notificationsViewController, animated: true)
    }
}
