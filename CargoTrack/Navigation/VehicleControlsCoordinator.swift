//
//  VehicleControlsCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 07/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class VehicleControlsCoordinator: Coordinator {
    private let presenter: UINavigationController
    private let vehicle: Vehicle
    
    init(presenter: UINavigationController, vehicle: Vehicle) {
        self.presenter = presenter
        self.vehicle = vehicle
    }
    
    override func start() {
        let vehicleControlsController: VehicleControlsViewController = Storyboard.Fleet.loadViewController()
        vehicleControlsController.vehicle = vehicle
        setDeallocallable(with: vehicleControlsController)
        presenter.pushViewController(vehicleControlsController, animated: true)
    }
}
