//
//  PaymentTopUpCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 12/08/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class PaymentTopUpCoordinator: Coordinator {
    private let presenter: UINavigationController
    private let currency: CurrencyType
    
    init(presenter: UINavigationController, currency: CurrencyType) {
        self.presenter = presenter
        self.currency = currency
    }
    
    override func start() {
        let roadTaxPaymentVC: RoadTaxTopUpViewController = Storyboard.Menu.loadViewController()
        roadTaxPaymentVC.currency = currency
        setDeallocallable(with: roadTaxPaymentVC)
        presenter.pushViewController(roadTaxPaymentVC, animated: true)
    }
}
