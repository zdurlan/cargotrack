//
//  ForgotPasswordCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 28/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import UIKit

class ForgotPassViewCoordinator: Coordinator {
    private let presenter: UINavigationController
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    override func start() {
        let forgotPassViewController: ForgotPasswordViewController = Storyboard.Onboarding.loadViewController()
        setDeallocallable(with: forgotPassViewController)
        presenter.pushViewController(forgotPassViewController, animated: true)
    }
}
