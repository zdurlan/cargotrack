//
//  FleetWebviewCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 26/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import UIKit

class FleetWebviewCoordinator: Coordinator {
    private let presenter: UINavigationController
   
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    override func start() {
        WSSClient.sharedInstance.createWSSocketGeneral()
        let fleetWebViewController: MyFleetWebViewController = Storyboard.Fleet.loadViewController()
        setDeallocallable(with: fleetWebViewController)
        presenter.pushViewController(fleetWebViewController, animated: true)
    }
}
