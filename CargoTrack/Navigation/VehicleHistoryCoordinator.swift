//
//  VehicleHistoryCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 07/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class VehicleHistoryCoordinator: Coordinator {
    private let presenter: UINavigationController
    private let vehicle: Vehicle
    
    init(presenter: UINavigationController, vehicle: Vehicle) {
        self.presenter = presenter
        self.vehicle = vehicle
    }
    
    override func start() {
        let vehicleHistoryController: VehicleHistoryViewController = Storyboard.Fleet.loadViewController()
        vehicleHistoryController.vehicle = vehicle
        setDeallocallable(with: vehicleHistoryController)
        presenter.pushViewController(vehicleHistoryController, animated: true)
    }
}
