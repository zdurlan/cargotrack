//
//  AppCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 20/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import KeychainSwift
import UIKit

class AppCoordinator: Coordinator {
    private var reachabilityMonitor: ReachabilityMonitor?
    let window: UIWindow
    let rootViewController: UINavigationController
    var sessionExpired = false
    var timer: Timer!
    
    init(window: UIWindow) {
        self.window = window
        rootViewController = UINavigationController()
        rootViewController.setNavigationBarHidden(true, animated: false)
        
        let vc: OnboardingViewController = Storyboard.Onboarding.loadViewController()
        rootViewController.pushViewController(vc, animated: false)
    }
    
    override func start() {
        startReachabilityMonitor()
        
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(receivedLogout(_:)), name: .didReceiveLogout, object: nil)
        timer = Timer.scheduledTimer(withTimeInterval: 7, repeats: true) { _ in
            if WSSClient.sharedInstance.areWSSNotConnected() {
                WSSClient.sharedInstance.createWSSocketGeneral()
            }
        }
        
        let keychain = KeychainSwift()
        if let sessionToken = keychain.get("accessToken"), !hasNewVersion() {
            if !sessionToken.isEmpty {
                if let userData = UserDefaults.standard.data(forKey: "user"),
                    let user = try? JSONDecoder().decode(User.self, from: userData) {
                    StateController.currentUser = user
                    UserService.shared.accessToken = "Bearer \(user.token)"
                    if (StateController.currentUser?.isSuspended ?? 0) != 0 {
                        let financialCoordinator = FinancialCoordinator(presenter: rootViewController)
                        financialCoordinator.isFromFingerprint = true
                        financialCoordinator.start()
                    } else {
                        if StateController.currentUser?.mobileAppAsWrapper ?? false {
                            let fleetWebViewCoordinator = FleetWebviewCoordinator(presenter: rootViewController)
                            fleetWebViewCoordinator.start()
                        } else if StateController.currentUser?.userRights?.contains(where: { $0.id == 6 }) ?? false {
                            let fleetViewCoordinator = FleetCoordinator(presenter: rootViewController)
                            fleetViewCoordinator.start()
                        } else if StateController.currentUser?.userRights?.contains(where: { $0.id == 26 }) ?? false {
                            let roadTaxCoordinator = RoadTaxCoordinator(presenter: rootViewController)
                            roadTaxCoordinator.isFromFingerprint = true
                            roadTaxCoordinator.start()
                        }
                    }
                }
            }
        }
    }
    
    private func startReachabilityMonitor() {
        if reachabilityMonitor == nil {
            reachabilityMonitor = ReachabilityMonitor(window: window)
        }
        
        reachabilityMonitor!.startMonitoring()
    }
    
    private func stopReachabilityMonitor() {
        if let reachabilityMonitor = reachabilityMonitor {
            reachabilityMonitor.stop()
        }
    }
    
    func applicationMovesToBackground() {
        stopReachabilityMonitor()
    }
    
    func applicationMovesToForeground() {
        if hasNewVersion() && !(rootViewController.topViewController! is UpdateViewController) {
            let updateCoordinator = UpdateCoordinator(presenter: rootViewController)
            updateCoordinator.start()
        }
        startReachabilityMonitor()
    }
    
    private func hasNewVersion() -> Bool {
        guard let info = Bundle.main.infoDictionary,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                return false
        }
        
        guard let data = try? Data(contentsOf: url) else {
            return false
        }
        guard let json = try! JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            return false
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            if let currentVersion = info["CFBundleShortVersionString"] as? String {
                return version > currentVersion
            }
            return false
        }
        return false
    }
    
    @objc private func receivedLogout(_ notification: Notification) {
        DispatchQueue.main.async {
            self.logoutUser()
        }
    }
    
    private func logoutUser() {
        timer.invalidate()
        UserDefaults.standard.set(nil, forKey: "user")
        let keychain = KeychainSwift()
        if !UserDefaults.standard.bool(forKey: "isAuthEnabled") {
            keychain.delete("accessToken")
        }
        //        if UserDefaults.standard.bool(forKey: "rememberMe") {
        //            UserDefaults.standard.set(stateController.currentUser.username, forKey: "username")
        //        }
        UserDefaults.standard.synchronize()
        WSSClient.sharedInstance.webSockets.forEach {
            $0.disconnect()
        }
        WSSClient.sharedInstance.webSockets.removeAll()
        DBManager.sharedInstance.deleteAllVehiclesFromDB()
        StateController.currentUser = nil
        for controller in self.rootViewController.viewControllers as Array {
            if controller.isKind(of: OnboardingViewController.self) {
                self.rootViewController.popToViewController(controller, animated: true)
                break
            }
        }
    }
}
