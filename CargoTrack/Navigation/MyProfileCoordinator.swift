//
//  MyProfileCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 28/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class MyProfileCoordinator: Coordinator {
    private let presenter: UINavigationController
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    override func start() {
        let myProfileViewController: MyProfileViewController = Storyboard.Menu.loadViewController()
        setDeallocallable(with: myProfileViewController)
        presenter.pushViewController(myProfileViewController, animated: true)
    }
}
