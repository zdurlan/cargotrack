//
//  NotificationDetailsCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 18/09/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class NotificationDetailsCoordinator: Coordinator {
    private let presenter: UINavigationController
    private let notification: NotificationPayload
    
    init(presenter: UINavigationController, notification: NotificationPayload) {
        self.presenter = presenter
        self.notification = notification
    }
    
    override func start() {
        let notificationDetailsVC: NotificationDetailsViewController = Storyboard.Menu.loadViewController()
        notificationDetailsVC.notification = notification
        setDeallocallable(with: notificationDetailsVC)
        presenter.pushViewController(notificationDetailsVC, animated: true)
    }
}
