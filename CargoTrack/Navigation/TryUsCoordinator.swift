
//
//  TryUsCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 26/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import UIKit

class TryUsCoordinator: Coordinator {
    private let presenter: UINavigationController
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    override func start() {
        let tryUsViewController: TryUsViewController = Storyboard.Onboarding.loadViewController()
        setDeallocallable(with: tryUsViewController)
        presenter.pushViewController(tryUsViewController, animated: true)
    }
}
