//
//  FuelLevelMapCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 03/05/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class FuelLevelMapCoordinator: Coordinator {
    private let presenter: UINavigationController
    private let alimentare: Alimentare
    
    init(presenter: UINavigationController, alimentare: Alimentare) {
        self.presenter = presenter
        self.alimentare = alimentare
    }
    
    override func start() {
        let fuelLevelMapController: FuelLevelMapViewController = Storyboard.Fleet.loadViewController()
        fuelLevelMapController.alimentare = alimentare
        setDeallocallable(with: fuelLevelMapController)
        presenter.pushViewController(fuelLevelMapController, animated: true)
    }
}
