//
//  FuelConsumptionCoordinator.swift
//  CargoTrack
//
//  Created by Andrei Stefanescu on 10/01/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import Foundation
import UIKit

class FuelConsumptionCoordinator: Coordinator {
    private let presenter: UINavigationController
    private let vehicle: Vehicle
    
    init(presenter: UINavigationController, vehicle: Vehicle) {
        self.presenter = presenter
        self.vehicle = vehicle
    }
    
    override func start() {
        let fuelConsumptionController: FuelConsumptionViewController = Storyboard.Fleet.loadViewController()
        fuelConsumptionController.vehicle = vehicle
        setDeallocallable(with: fuelConsumptionController)
        presenter.pushViewController(fuelConsumptionController, animated: true)
    }
}
