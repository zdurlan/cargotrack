//
//  QLPreviewCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 01/10/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import QuickLook
import UIKit

class QLPreviewCoordinator: Coordinator {
    private let presenter: UINavigationController
    var title: String!
    var tempURL: TemporaryFileURL!
    var qlViewModel: QLViewModel!

    init(navigationController: UINavigationController, title: String, tempURL: TemporaryFileURL) {
        self.presenter = navigationController
        self.title = title
        self.tempURL = tempURL
    }

    override func start() {
        let navigationController = UINavigationController()
        navigationController.modalPresentationStyle = .overFullScreen
        navigationController.isNavigationBarHidden = true

        qlViewModel = QLViewModel(url: tempURL.contentURL, title: title)

        let previewController = CargoQLPreviewController(viewModel: qlViewModel)
        navigationController.setViewControllers([previewController], animated: true)

        presenter.present(navigationController, animated: true, completion: nil)
    }
}

class TemporaryFileURL {
    let contentURL: URL

    init(endpoint: String, fileManager: FileManager) {
        contentURL = fileManager.temporaryDirectory.appendingPathComponent(endpoint)
    }
}

class CargoQLPreviewController: QLPreviewController {
    var viewModel: QLViewModel!

    lazy final private var backItem: UIBarButtonItem = { [unowned self] in
        return UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissPreview))
    }()

    convenience init(viewModel: QLViewModel) {
        self.init()
        self.viewModel = viewModel
        self.dataSource = viewModel
        self.delegate = viewModel
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationItem.leftBarButtonItem = backItem
    }

    @objc func dismissPreview() {
        navigationController?.dismiss(animated: true)
    }
}

protocol QLViewModelDelegate: class {
    func qlViewModelDidDismiss(_ viewModel: QLViewModel)
}

class QLViewModel: NSObject {
    private let url: URL
    let title: String
    weak var navigationDelegate: QLViewModelDelegate?

    init(url: URL, title: String) {
        self.url = url
        self.title = title
    }

    func dismiss() {
        navigationDelegate?.qlViewModelDidDismiss(self)
    }
}

class CargoQLPreviewItem: NSObject, QLPreviewItem {
    var previewItemURL: URL?
    var previewItemTitle: String?

    init(title: String, url: URL) {
        self.previewItemTitle = title
        self.previewItemURL = url
    }
}

extension QLViewModel: QLPreviewControllerDataSource {
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }

    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return CargoQLPreviewItem(title: title, url: url)
    }
}

extension QLViewModel: QLPreviewControllerDelegate {
    func previewControllerDidDismiss(_ controller: QLPreviewController) {
        navigationDelegate?.qlViewModelDidDismiss(self)
    }
}
