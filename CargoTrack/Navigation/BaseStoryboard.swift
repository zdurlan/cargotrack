//
//  StoryboardCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 20/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import UIKit

enum Storyboard: String {
    case Onboarding = "Onboarding"
    case Fleet = "Fleet"
    case Menu = "Menu"
}

extension Storyboard {
    
    var rootViewController: UIViewController? {
        return current.instantiateInitialViewController()
    }
    
    var initialViewController: UIViewController? {
        return current.instantiateInitialViewController()
    }
    
    func loadViewController<T: UIViewController>() -> T {
        if let loadedVC = current.instantiateViewController(withIdentifier: String(describing: T.self)) as? T {
            return loadedVC
        }
        fatalError("View Controller does not have the same Storyboard identifier as its class name")
    }
}

fileprivate extension Storyboard {
    
    private var current: UIStoryboard {
        return UIStoryboard(name: rawValue, bundle: nil)
    }
}
