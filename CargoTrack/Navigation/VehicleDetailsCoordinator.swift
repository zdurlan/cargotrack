//
//  VehicleDetailsCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 06/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import UIKit

class VehicleDetailsCoordinator: Coordinator {
    private let presenter: UINavigationController
    private let vehicle: Vehicle
    
    init(presenter: UINavigationController, vehicle: Vehicle) {
        self.presenter = presenter
        self.vehicle = vehicle
    }
    
    override func start() {
        let vehicleDetailsController: VehicleDetailsViewController = Storyboard.Fleet.loadViewController()
        vehicleDetailsController.vehicle = vehicle
        setDeallocallable(with: vehicleDetailsController)
        presenter.pushViewController(vehicleDetailsController, animated: true)
    }
}
