//
//  RoadTaxCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 28/06/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class RoadTaxCoordinator: Coordinator {
    private let presenter: UINavigationController
    var isFromFingerprint = false
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    override func start() {
        let roadTaxChooseServiceVC: RoadTaxChooseServiceViewController = Storyboard.Menu.loadViewController()
        roadTaxChooseServiceVC.isFromFingerprint = isFromFingerprint
        setDeallocallable(with: roadTaxChooseServiceVC)
        presenter.pushViewController(roadTaxChooseServiceVC, animated: true)
    }
}
