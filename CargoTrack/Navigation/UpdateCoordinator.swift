//
//  UpdateCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 02/05/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class UpdateCoordinator: Coordinator {
    private let presenter: UINavigationController
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    override func start() {
        let updateController: UpdateViewController = Storyboard.Onboarding.loadViewController()
        setDeallocallable(with: updateController)
        presenter.pushViewController(updateController, animated: true)
    }
}

