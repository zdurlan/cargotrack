//
//  PaymentChooseCurrencyCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 12/08/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class PaymentChooseCurrencyCoordinator: Coordinator {
    private let presenter: UINavigationController
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    override func start() {
        let roadTaxPaymentVC: RoadTaxPaymentViewController = Storyboard.Menu.loadViewController()
        setDeallocallable(with: roadTaxPaymentVC)
        presenter.pushViewController(roadTaxPaymentVC, animated: true)
    }
}
