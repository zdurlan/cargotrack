//
//  AllVehiclesMapCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 02/09/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import UIKit

class AllVehiclesMapCoordinator: Coordinator {
    private let presenter: UINavigationController
    private let vehicles: [Vehicle]
    
    init(presenter: UINavigationController, vehicles: [Vehicle]) {
        self.presenter = presenter
        self.vehicles = vehicles
    }
    
    override func start() {
        let vehicleMapController: AllVehiclesMapViewController = Storyboard.Fleet.loadViewController()
        vehicleMapController.vehicles = vehicles
        setDeallocallable(with: vehicleMapController)
        presenter.pushViewController(vehicleMapController, animated: true)
    }
}
