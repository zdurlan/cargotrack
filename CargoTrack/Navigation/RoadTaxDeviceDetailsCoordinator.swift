//
//  RoadTaxDeviceDetailsCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 01/07/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import UIKit

class RoadTaxDeviceDetailsCoordinator: Coordinator {
    private let presenter: UINavigationController
    private let device: RoadTaxDevice
    private let balance: Balance
    
    init(presenter: UINavigationController, device: RoadTaxDevice, balance: Balance) {
        self.presenter = presenter
        self.device = device
        self.balance = balance
    }
    
    override func start() {
        let roadTaxDeviceDetailsVC: RoadTaxDeviceDetailsViewController = Storyboard.Menu.loadViewController()
        roadTaxDeviceDetailsVC.device = device
        roadTaxDeviceDetailsVC.balance = balance
        setDeallocallable(with: roadTaxDeviceDetailsVC)
        presenter.pushViewController(roadTaxDeviceDetailsVC, animated: true)
    }
}
