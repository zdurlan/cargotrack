//
//  FuelLevelCoordinator.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 23/04/2020.
//  Copyright © 2020 Alin Zdurlan. All rights reserved.
//

import Foundation
import UIKit

class FuelLevelCoordinator: Coordinator {
    private let presenter: UINavigationController
    private let vehicle: Vehicle
    
    init(presenter: UINavigationController, vehicle: Vehicle) {
        self.presenter = presenter
        self.vehicle = vehicle
    }
    
    override func start() {
        let fuelLevelController: FuelLevelViewController = Storyboard.Fleet.loadViewController()
        fuelLevelController.vehicle = vehicle
        setDeallocallable(with: fuelLevelController)
        presenter.pushViewController(fuelLevelController, animated: true)
    }
}
