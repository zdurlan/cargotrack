//
//  CoreLocation+Geocoding.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 14/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import CoreLocation
import Foundation

extension CLLocation {
    func geocode(completion: @escaping (_ placemark: [CLPlacemark]?, _ error: Error?) -> Void)  {
        CLGeocoder().reverseGeocodeLocation(self, completionHandler: completion)
    }
}
