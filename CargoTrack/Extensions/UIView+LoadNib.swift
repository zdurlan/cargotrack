//
//  UIView+LoadNib.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 20/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    class func loadFromNib<T: UIView>(owner: Any? = nil) -> T {
        let nibName = String(describing: self)
        let nib = UINib(nibName: nibName, bundle: nil)
        guard let nibview = nib.instantiate(withOwner: owner, options: nil).first as? T else {
            assertionFailure("View \(nibName) not available")
            return T()
        }
        return nibview
    }
}
