//
//  UIViewController+Alert.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 13/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func presentAlertController(title: String, message: String) {
        DispatchQueue.main.async {
            let otherAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let dismiss = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
            otherAlert.addAction(dismiss)
            self.present(otherAlert, animated: true, completion: nil)
        }
    }
    
    func presentAlertController(title: String?, message: String?, actions: [UIAlertAction], style: UIAlertController.Style = .alert) {
        DispatchQueue.main.async {
            let otherAlert = UIAlertController(title: title, message: message, preferredStyle: style)
            actions.forEach {
                otherAlert.addAction($0)
            }
            self.present(otherAlert, animated: true, completion: nil)
        }
    }
}
