//
//  Bundle+Locale.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 22/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

extension Bundle {
    private static var bundle: Bundle!
    
    public static func localizedBundle() -> Bundle! {
        if bundle == nil {
            let appLang = UserDefaults.standard.string(forKey: "app_lang") ?? "en"
            let path = Bundle.main.path(forResource: appLang, ofType: "lproj")
            bundle = Bundle(path: path!)
        }
        
        return bundle
    }
    
    public static func setLanguage(lang: String) {
        UserDefaults.standard.set(lang, forKey: "app_lang")
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        bundle = Bundle(path: path!)
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.localizedBundle(), value: "", comment: "")
    }
    
    func localized(withArguments arguments: CVarArg) -> String {
        let format = NSLocalizedString(self, comment: "")
        return String.localizedStringWithFormat(format, arguments)
    }
    
}
