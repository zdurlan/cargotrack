//
//  NSNumbers+Extensions.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 30/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

extension Double {
    func truncate(places : Int) -> Double
    {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}

extension Int {
    func secondsToHoursMinutesSeconds() -> (Int, Int) {
        return (self / 3600, (self % 3600) / 60)
    }
}
