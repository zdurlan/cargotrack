//
//  AppDelegate.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 20/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Firebase
import GoogleMaps
import IQKeyboardManager
import KeychainSwift
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.shared.isIdleTimerDisabled = true
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        if #available(iOS 13.0, *) {
            window.overrideUserInterfaceStyle = .light
        }
        self.window = window
        
        appCoordinator = AppCoordinator(window: window)
        let keychain = KeychainSwift()
        if let sessionToken = keychain.get("accessToken") {
            if !sessionToken.isEmpty {
                if let userData = UserDefaults.standard.data(forKey: "user"),
                    let user = try? JSONDecoder().decode(User.self, from: userData) {
                    StateController.currentUser = user
                }
            }
        }
        
        appCoordinator?.start()
        
        GMSServices.provideAPIKey("AIzaSyCuY7XPTPSmlAWm-ZyJUB6gq5XpAe4Lndo")
        FirebaseApp.configure()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        appCoordinator?.applicationMovesToBackground()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        appCoordinator?.applicationMovesToForeground()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

