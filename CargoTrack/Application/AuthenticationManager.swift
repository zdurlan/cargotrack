//
//  AuthenticationManager.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 04/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import LocalAuthentication

enum AuthError: Error {
    case noBiometrics
    case failure
}

class AuthenticationManager {
    static func checkAuthMethod(completion: @escaping (Bool, AuthError?) -> Void) {
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
                completion(success, AuthError.failure)
            }
        } else {
            completion(false, AuthError.noBiometrics)
        }
    }
}
