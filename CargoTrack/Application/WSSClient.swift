//
//  WSSClient.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 24/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Starscream

class CargoTrackWebSocket: WebSocket {
    var isConnected: Bool = false
}

class WSSClient {
    var webSockets = [CargoTrackWebSocket]()
    
    static let sharedInstance = WSSClient()
    
    var positions: [[String: Any]] = []
    
    func areWSSNotConnected() -> Bool {
        let unconnectedWebsockets = webSockets.filter { !$0.isConnected }
        unconnectedWebsockets.forEach { $0.connect() }
        return unconnectedWebsockets.count > 0
    }
    
    func sendPingMessage(dsServer: String) {
        var request = URLRequest(url: URL(string: "wss://\(dsServer)/wss")!)
        request.timeoutInterval = 55
        request.setValue("on.cargotrack.ro", forHTTPHeaderField: "Origin")
        let webSocket = WebSocket(request: request)
        webSocket.onEvent = { event in
            switch event {
            case .connected:
                debugPrint("\(dsServer) Connected")
                let value = ["scope": "ping"] as [String : Any]
                guard JSONSerialization.isValidJSONObject(value) else {
                    print("[WEBSOCKET] Value is not a valid JSON object.\n \(value)")
                    return
                }

                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: [])
                    webSocket.write(data: data) {
                        debugPrint("done")
                    }
                } catch let error {
                    debugPrint("[WEBSOCKET] Error serializing JSON:\n\(error)")
                }
            default:
                break
            }
        }
        webSocket.connect()
    }
    
    func createWSSocketGeneral() {
        if UserService.shared.accessToken != nil && StateController.currentUser != nil {
            var request = URLRequest(url: URL(string: "wss://\(StateController.currentUser?.clientDataServer ?? "")/wss")!)
            request.timeoutInterval = 55
            request.setValue("on.cargotrack.ro", forHTTPHeaderField: "Origin")
            let websocket = CargoTrackWebSocket(request: request)
            websocket.onEvent = { event in
                switch event {
                case .connected:
                    websocket.isConnected = true
                    let value = ["scope": "general", "token": UserService.shared.accessToken!, "user_id": StateController.currentUser?.userId ?? 0] as [String : Any]
                    guard JSONSerialization.isValidJSONObject(value) else {
                        print("[WEBSOCKET] Value is not a valid JSON object.\n \(value)")
                        return
                    }

                    do {
                        let data = try JSONSerialization.data(withJSONObject: value, options: [])
                        websocket.write(data: data) {
                            debugPrint("done")
                        }
                    } catch let error {
                        debugPrint("[WEBSOCKET] Error serializing JSON:\n\(error)")
                    }
                case .disconnected:
                    websocket.connect()
                case .text(let text):
                    if let json = try? JSONSerialization.jsonObject(with: text.data(using: .utf8) ?? Data(), options: [.allowFragments]) as? [String: Any] {
                        if let type = json["type"] as? String {
                            if type == "auth" {
                                NotificationCenter.default.post(name: .didReceiveLogout, object: nil)
                            } else if type == "notification" {
                                DispatchQueue.main.async {
                                    guard let textData = text.data(using: .utf8) else {
                                        return
                                    }
                                    if text.contains("immobiliser") || text.contains("buzzer") {
                                        NotificationCenter.default.post(name: .didReceiveDeviceCommand, object: nil, userInfo: ["data": textData])
                                    } else {
                                        NotificationCenter.default.post(name: .didReceiveNotification, object: nil, userInfo: ["data": textData])
                                    }
                                }
                            }
                        }
                    } else {
                        debugPrint("JSON ERROR FROM WSS")
                    }
                default:
                    break
                }
            }
            websocket.connect()
            webSockets.append(websocket)
        }
    }
    
    func createWSSocketFleet(dsServer: String, deviceIds: [Int]) {
        var request = URLRequest(url: URL(string: "wss://\(dsServer)/wss")!)
        request.timeoutInterval = 55
        request.setValue("on.cargotrack.ro", forHTTPHeaderField: "Origin")
        let webSocket = CargoTrackWebSocket(request: request)
        webSocket.onEvent = { event in
            switch event {
            case .connected:
                webSocket.isConnected = true
                debugPrint("\(dsServer) Connected")
                let value = ["scope": "position", "device_ids": deviceIds, "token": UserService.shared.accessToken!] as [String : Any]
                guard JSONSerialization.isValidJSONObject(value) else {
                    print("[WEBSOCKET] Value is not a valid JSON object.\n \(value)")
                    return
                }

                do {
                    let data = try JSONSerialization.data(withJSONObject: value, options: [])
                    webSocket.write(data: data) {
                        debugPrint("done")
                    }
                } catch let error {
                    debugPrint("[WEBSOCKET] Error serializing JSON:\n\(error)")
                }
            case .disconnected:
                debugPrint("\(dsServer) Disconnected")
                if self.webSockets.count > 0 {
                    webSocket.connect()
                    self.sendPingMessage(dsServer: dsServer)
                }
            case .text(let text):
                if let json = try? JSONSerialization.jsonObject(with: text.data(using: .utf8) ?? Data(), options: [.allowFragments]) as? [String: Any] {
                    if self.positions.contains(where: {
                        if let insideId = $0["deviceid"] as? Int {
                            return insideId == json["deviceid"] as! Int
                        }
                        return false
                    }) {
                        var currentPosition = self.positions.first(where: {
                            $0["deviceid"] as! Int == json["deviceid"] as! Int
                        })!
                        let replacingCurrent = (currentPosition["position"] as! [String: Any]).merging(json) { (_, new) in new }
                        currentPosition["position"] = replacingCurrent
                        DBManager.sharedInstance.updateVehicle(json["deviceid"] as! Int, newPosition: currentPosition["position"] as! [String : Any])
                    } else {
                        self.positions.append(["deviceid": json["deviceid"] as! Int, "position": json])
                        DBManager.sharedInstance.updateVehicle(json["deviceid"] as! Int, newPosition: json)
                    }
                } else {
                    debugPrint("JSON ERROR FROM WSS")
                }
            default: break
            }
        }
        webSocket.connect()
        webSockets.append(webSocket)
    }
}
