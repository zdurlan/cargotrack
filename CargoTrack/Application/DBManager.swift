//
//  DBManager.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 24/07/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation
import RealmSwift
import SlackKit

class DBManager {
    static let sharedInstance = DBManager()
    
    static var timerDict: [[String: Int]] = [["deviceid": 0, "timestampOfUpdate": -1]]
    
    private let configuration = Realm.Configuration(
        encryptionKey: nil,
        schemaVersion: 3,
        migrationBlock: { migration, _ in
            migration.enumerateObjects(ofType: "Vehicle", { _, new in
                new?["fuelConsumption"] = FuelConsumptionSensor()
                new?["leakTreshold"] = 10
                new?["refuelingTreshold"] = 10
            })
    })
    
    func getLocationsForEveryVehicle() {
        let realm = try! Realm(configuration: configuration)
        let vehicles = realm.objects(Vehicle.self)
        let usedValues = UsedValues()
        vehicles.forEach { vehicle in
            let vehicleRef = ThreadSafeReference(to: vehicle)
            DispatchQueue(label: "realm").async {
                let locationSemaphore = DispatchSemaphore(value: 0)
                let realm = try! Realm(configuration: self.configuration)
                guard let vehicle = realm.resolve(vehicleRef), let hasUsedValues = vehicle.usedValues else {
                    return // object was deleted
                }
                if hasUsedValues.latitude != nil && hasUsedValues.longitude != nil {
                    APIClient().perform(DeviceService.createReverseGeocodingSingleRequest(vehicleValues: hasUsedValues, dsServer: vehicle.hostServer)) { result in
                        switch result {
                        case .success(let response):
                            if let deviceAddress = try? response.decode(to: DeviceAddress.self) {
                                usedValues.location = deviceAddress.body.addressFormatted
                                usedValues.countryCode = deviceAddress.body.addressElements["country_code"]!
                                locationSemaphore.signal()
                            } else {
                                locationSemaphore.signal()
                                print("error")
                            }
                        case .failure(let error):
                            print(error.localizedDescription)
                            locationSemaphore.signal()
                        }
                    }
                } else {
                    locationSemaphore.signal()
                }
                locationSemaphore.wait()
                try! realm.write {
                    if !vehicle.isInvalidated {
                        vehicle.usedValues!.location = usedValues.location
                        vehicle.usedValues!.countryCode = usedValues.countryCode
                        vehicle.oldValues = vehicle.usedValues
                    }
                }
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .didUpdateVehicles, object: nil)
                }
            }
        }
    }
    
    func getVehiclesFromDB() -> Results<Vehicle> {
        let realm = try! Realm(configuration: configuration)
        return realm.objects(Vehicle.self)
    }
    
    func getVehicle(id: Int) -> Vehicle? {
        let realm = try! Realm(configuration: configuration)
        return realm.object(ofType: Vehicle.self, forPrimaryKey: id)
    }
    
    func updateVehicleFromBackground(_ storedVehicle: Vehicle, newPosition: [String: Any]) {
        if let definitionsData = storedVehicle.attributeDefinitionsData {
            let vehicleRef = ThreadSafeReference(to: storedVehicle)
            let definitionsDict = try! JSONSerialization.jsonObject(with: definitionsData, options: .mutableContainers) as! [String: Any]
            let deviceAttributeLogic = DeviceAttributeLogicDictionary(rawValues: newPosition, definitions: definitionsDict)
            let realm = try! Realm(configuration: configuration)
            guard let vehicle = realm.resolve(vehicleRef) else {
                return // object was deleted
            }
            let vehicleName = vehicle.name
            let vehicleId = vehicle.id
            try! realm.write {
                vehicle.usedValues = vehicle.getData(deviceAttributeLogicDictionary: deviceAttributeLogic)
            }
            let locationSemaphore = DispatchSemaphore(value: 0)
            let usedValues = UsedValues()
            if vehicle.usedValues?.latitude != nil && vehicle.usedValues?.longitude != nil {
                APIClient().perform(DeviceService.createReverseGeocodingSingleRequest(vehicleValues: vehicle.usedValues!, dsServer: vehicle.hostServer)) { result in
                    switch result {
                    case .success(let response):
                        if let deviceAddress = try? response.decode(to: DeviceAddress.self) {
                            usedValues.location = deviceAddress.body.addressFormatted
                            usedValues.countryCode = deviceAddress.body.addressElements["country_code"]!
                            locationSemaphore.signal()
                        } else {
                            locationSemaphore.signal()
                            APIClient().perform(SlackService.sendMessage("Vehicle: \(vehicleName) on user: \(StateController.currentUser?.name ?? "") has no device address")) { _ in }
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                        locationSemaphore.signal()
                    }
                }
            } else {
                locationSemaphore.signal()
            }
            locationSemaphore.wait()
            try! realm.write {
                if !vehicle.isInvalidated {
                    vehicle.usedValues!.location = usedValues.location
                    vehicle.usedValues!.countryCode = usedValues.countryCode
                    vehicle.oldValues = vehicle.usedValues
                }
            }
            let fuelConsumptionSemaphore = DispatchSemaphore(value: 0)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateToSendString = dateFormatter.string(from: Date())
            if vehicle.isInvalidated {
                return
            }
            if let fuelConsumptionValue = vehicle.fuelConsumption?.sensor.value,
                let mileage = vehicle.fuelConsumption?.mileage.value {
                var newPositionsFuel: [[String: Any]] = [[:]]
                APIClient().perform(DeviceService.createDevicePositionsRequest(deviceId: [vehicle.secondaryId], dsServer: vehicle.hostServer)) { result in
                    switch result {
                    case .success(let response):
                        guard let json = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: [.allowFragments]) as? [[String: Any]], json.count > 0 else {
                            debugPrint("error decoding JSON")
                            fuelConsumptionSemaphore.signal()
                            return
                        }
                        newPositionsFuel = json
                        fuelConsumptionSemaphore.signal()
                    case .failure(let error):
                        print(error.localizedDescription)
                        fuelConsumptionSemaphore.signal()
                    }
                }
                fuelConsumptionSemaphore.wait()
                if newPositionsFuel.count > 0,
                    let newPositionForVehicle = newPositionsFuel.first(where: {
                        if let deviceId = $0["deviceid"] as? Int {
                            return deviceId == vehicle.secondaryId
                        }
                        APIClient().perform(SlackService.sendMessage("Vehicle: \(vehicleName) on user: \(StateController.currentUser?.name ?? "")  has no new positions")) { _ in }
                        return false
                    }) {
                    let newDeviceAttributeLogic = DeviceAttributeLogicDictionary(rawValues: newPositionForVehicle, definitions: definitionsDict)
                    let newMileage = newDeviceAttributeLogic.getValue(attributeKey: vehicle.attributes!.mileage!)
                    let newFuelConsumption = newDeviceAttributeLogic.getValue(attributeKey: vehicle.attributes!.fuelConsumption!)
                    if newMileage != "LOGIC_ERROR" && newFuelConsumption != "LOGIC_ERROR" {
                        let mileageKm = Double(newMileage)! - Double(mileage)
                        let consumption = Double(newFuelConsumption)! - Double(fuelConsumptionValue)
                        if mileageKm > 0 {
                            let lastConsumption = (100 * consumption) / mileageKm
                            if !vehicle.isInvalidated {
                                try! realm.write {
                                    vehicle.usedValues?.fuelConsumption = String(lastConsumption)
                                    vehicle.oldValues = vehicle.usedValues
                                }
                            }
                            debugPrint("Updated vehicle")
                        } else {
                            try! realm.write {
                                if !vehicle.isInvalidated {
                                    vehicle.usedValues?.fuelConsumption = "0"
                                    vehicle.oldValues = vehicle.usedValues
                                    vehicle.deviceTime = dateToSendString
                                    APIClient().perform(SlackService.sendMessage("Vehicle: \(vehicleName) on user: \(StateController.currentUser?.name ?? "") set fuel consumption 0")) { _ in }
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            let vehicleDict: [String: Int] = ["deviceid": vehicleId]
                            NotificationCenter.default.post(name: .didUpdateVehicles, object: nil, userInfo: vehicleDict)
                        }
                    }
                }
            } else {
                try! realm.write {
                    if !vehicle.isInvalidated {
                        vehicle.usedValues?.fuelConsumption = "0"
                        vehicle.oldValues = vehicle.usedValues
                        APIClient().perform(SlackService.sendMessage("Vehicle: \(vehicleName) on user: \(StateController.currentUser?.name ?? "") set fuel consumption 0")) { _ in }
                    }
                }
                DispatchQueue.main.async {
                    let vehicleDict: [String: Int] = ["deviceid": vehicleId]
                    NotificationCenter.default.post(name: .didUpdateVehicles, object: nil, userInfo: vehicleDict)
                }
            }
            //}
        }
    }
    
    func updateVehicle(_ vehicleId: Int, newPosition: [String: Any]) {
        let realm = try! Realm(configuration: configuration)
        if let storedVehicle = realm.objects(Vehicle.self).first(where: {
            $0.secondaryId == vehicleId
        }) {
            if let definitionsData = storedVehicle.attributeDefinitionsData {
                let vehicleRef = ThreadSafeReference(to: storedVehicle)
                if !storedVehicle.isInvalidated {
                    DispatchQueue(label: "realm").async {
                        let definitionsDict = try! JSONSerialization.jsonObject(with: definitionsData, options: .mutableContainers) as! [String: Any]
                        let deviceAttributeLogic = DeviceAttributeLogicDictionary(rawValues: newPosition, definitions: definitionsDict)
                        let realm = try! Realm(configuration: self.configuration)
                        guard let vehicle = realm.resolve(vehicleRef) else {
                            return // object was deleted
                        }
                        if vehicle.isInvalidated {
                            return
                        }
                        let vehicleName = vehicle.name
                        try! realm.write {
                            if !vehicle.isInvalidated {
                                vehicle.usedValues = vehicle.getData(deviceAttributeLogicDictionary: deviceAttributeLogic)
                            }
                        }
                        let locationSemaphore = DispatchSemaphore(value: 0)
                        let usedValues = UsedValues()
                        if vehicle.usedValues?.latitude != nil && vehicle.usedValues?.longitude != nil {
                            APIClient().perform(DeviceService.createReverseGeocodingSingleRequest(vehicleValues: vehicle.usedValues!, dsServer: vehicle.hostServer)) { result in
                                switch result {
                                case .success(let response):
                                    if let deviceAddress = try? response.decode(to: DeviceAddress.self) {
                                        usedValues.location = deviceAddress.body.addressFormatted
                                        usedValues.countryCode = deviceAddress.body.addressElements["country_code"]!
                                        locationSemaphore.signal()
                                    } else {
                                        locationSemaphore.signal()
                                        APIClient().perform(SlackService.sendMessage("Vehicle: \(vehicleName) on user: \(StateController.currentUser?.name ?? "") has no device address")) { _ in }
                                    }
                                case .failure(let error):
                                    print(error.localizedDescription)
                                    locationSemaphore.signal()
                                }
                            }
                        } else {
                            locationSemaphore.signal()
                        }
                        locationSemaphore.wait()
                        try! realm.write {
                            if !vehicle.isInvalidated {
                                vehicle.usedValues!.location = usedValues.location
                                vehicle.usedValues!.countryCode = usedValues.countryCode
                                vehicle.oldValues = vehicle.usedValues
                            }
                        }
                        let fuelConsumptionSemaphore = DispatchSemaphore(value: 0)
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        if vehicle.isInvalidated {
                            return
                        }
                        if let fuelConsumptionValue = vehicle.fuelConsumption?.sensor.value,
                            let mileage = vehicle.fuelConsumption?.mileage.value {
                            var newPositionsFuel: [[String: Any]] = [[:]]
                            APIClient().perform(DeviceService.createDevicePositionsRequest(deviceId: [vehicle.secondaryId], dsServer: vehicle.hostServer)) { result in
                                switch result {
                                case .success(let response):
                                    guard let json = try? JSONSerialization.jsonObject(with: response.body ?? Data(), options: [.allowFragments]) as? [[String: Any]], json.count > 0 else {
                                        debugPrint("error decoding JSON")
                                        fuelConsumptionSemaphore.signal()
                                        return
                                    }
                                    newPositionsFuel = json
                                    fuelConsumptionSemaphore.signal()
                                case .failure(let error):
                                    print(error.localizedDescription)
                                    fuelConsumptionSemaphore.signal()
                                }
                            }
                            fuelConsumptionSemaphore.wait()
                            if newPositionsFuel.count > 0,
                                let newPositionForVehicle = newPositionsFuel.first(where: {
                                    if let deviceId = $0["deviceid"] as? Int {
                                        return deviceId == vehicle.secondaryId
                                    }
                                    APIClient().perform(SlackService.sendMessage("Vehicle: \(vehicleName) on user: \(StateController.currentUser?.name ?? "")  has no new positions")) { _ in }
                                    return false
                                }) {
                                let newDeviceAttributeLogic = DeviceAttributeLogicDictionary(rawValues: newPositionForVehicle, definitions: definitionsDict)
                                let newMileage = newDeviceAttributeLogic.getValue(attributeKey: vehicle.attributes!.mileage!)
                                let newFuelConsumption = newDeviceAttributeLogic.getValue(attributeKey: vehicle.attributes!.fuelConsumption!)
                                if newMileage != "LOGIC_ERROR" && newFuelConsumption != "LOGIC_ERROR" {
                                    let mileageKm = Double(newMileage)! - Double(mileage)
                                    let consumption = Double(newFuelConsumption)! - Double(fuelConsumptionValue)
                                    if mileageKm > 0 {
                                        let lastConsumption = (100 * consumption) / mileageKm
                                        if !vehicle.isInvalidated {
                                            try! realm.write {
                                                vehicle.usedValues?.fuelConsumption = String(lastConsumption)
                                                vehicle.oldValues = vehicle.usedValues
                                            }
                                        }
                                        debugPrint("Updated vehicle")
                                    } else {
                                        try! realm.write {
                                            if !vehicle.isInvalidated {
                                                vehicle.usedValues?.fuelConsumption = "0"
                                                vehicle.oldValues = vehicle.usedValues
                                                APIClient().perform(SlackService.sendMessage("Vehicle: \(vehicleName) on user: \(StateController.currentUser?.name ?? "") set fuel consumption 0")) { _ in }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            try! realm.write {
                                if !vehicle.isInvalidated {
                                    vehicle.usedValues?.fuelConsumption = "0"
                                    vehicle.oldValues = vehicle.usedValues
                                    APIClient().perform(SlackService.sendMessage("Vehicle: \(vehicleName) on user: \(StateController.currentUser?.name ?? "") set fuel consumption 0")) { _ in }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func addVehiclesToDB(_ vehicles: [Vehicle]) {
        let realm = try! Realm(configuration: configuration)
        try! realm.write {
            realm.deleteAll()
            realm.add(vehicles)
            print("Added vehicle list")
        }
    }
    
    func deleteAllVehiclesFromDB() {
        let realm = try! Realm(configuration: configuration)
        try! realm.write {
            realm.deleteAll()
        }
    }
}

extension Notification.Name {
    static let didUpdateVehicles = Notification.Name("didUpdateVehicles")
    static let didReceiveNotification = Notification.Name("didReceiveNotification")
    static let didReceiveDeviceCommand = Notification.Name("didReceiveDeviceCommand")
    static let didReceiveLogout = Notification.Name("didReceiveLogout")
}
