//
//  StateController.swift
//  CargoTrack
//
//  Created by Alin Zdurlan on 26/06/2019.
//  Copyright © 2019 Alin Zdurlan. All rights reserved.
//

import Foundation

enum UnitOfMeasurement: String {
    case imperial
    case metric
}

struct StateController {
    static var currentUser: User?
    
    static let slackBotAPIKey = "xoxb-506267241781-713739577445-yx4ZoaAUtzZzFqxaKXThCzhR"
    
    static var fromBackground = false

    static func getUnitOfMeasurement() -> UnitOfMeasurement {
        return UnitOfMeasurement(rawValue: UserDefaults.standard.string(forKey: "unitOfMeasurement") ?? "metric")!
    }
    
    static func getDateFormat() -> String {
        return UserDefaults.standard.string(forKey: "dateFormat") ?? "dd-MM-yyyy HH:mm:ss"
    }
    
    static func getStopDuration() -> Int {
        return UserDefaults.standard.integer(forKey: "stopDuration")
    }
    
    static func getShowCoordinatesOnFleetScreenValue() -> Bool {
        return UserDefaults.standard.bool(forKey: "showCoordinatesOnFleet")
    }
}
